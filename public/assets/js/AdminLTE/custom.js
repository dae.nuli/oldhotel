$(function() {
    // subcategory();

    $(document).on('keyup','.autocomma', function(event) {
        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
            return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });

	$(document).on('click','.delete',function(){
		return confirm('Are you sure want to delete ?');
	});

    $(document).on('click','.done-order',function(){
        return confirm('Are you sure want to deliver this order ?');
    });

    $(document).on('click','.restore',function(){
        return confirm('Are you sure want to restore ?');
    });

    $(document).on('click','.actived',function(){
        return confirm('Are you sure want to active ?');
    });

    $(document).on('click','.noactived',function(){
        return confirm('Are you sure want to non active ?');
    });

    $(document).on('click','.published',function(){
        return confirm('Are you sure want to publish ?');
    });

    $(document).on('click','.drafted',function(){
        return confirm('Are you sure want to draft ?');
    });
    $(document).on('click','.clean',function(){
        return confirm('Are you sure want to clean this room ?');
    });
	$('.filter').on('click',function (e){
        e.preventDefault();
	        if($(this).data('method')==='filter'){
		        $('#compose-modal').modal('show');
	        }
    });

    // $('.info-client').on('click',function (e){
    //     var idClient = $(this).data('idclient');
    //     $('#open-modal').load('/hotel/index.php/admin/orders/modal-client-info/'+idClient);
    // });

    // $('.message-detail').on('click',function (e){
    //     var idMessage = $(this).data('idmessage');
    //     $('#open-modal').load('/hotel/index.php/admin/messages/detail/'+idMessage);
    // });

    // $('.add-orders').on('click',function (e){
    //     $('#open-modal').load('/hotel/index.php/admin/orders/modal-add-orders');
    // });

    $(document).on('click keyup','.modal-icon',function (e){
        waiting();
        $('#open-modal').load('/hotel/index.php/admin/facility/icon');
    });

    $('.add-room').on('click',function (e){
        waiting();
        // e.preventDefault();
        var transaction = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-add-room/'+transaction);
    });

    $('.add-cafe').on('click',function (e){
        waiting();
        var transaction = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-add-cafe/'+transaction);
    });

    $('.add-adds').on('click',function (e){
        waiting();
        // e.preventDefault();
        var transaction = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-add-adds/'+transaction);
    });

    $('.edit-payment').on('click',function (e){
        waiting();
        var transaction = $(this).data('transaction');
        var id_payment  = $(this).data('idpayment');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-edit-payment/'+transaction+'/'+id_payment);
    });

    $('.edit-cafe-payment').on('click',function (e){
        waiting();
        var transaction = $(this).data('transaction');
        var id_payment  = $(this).data('idpayment');
        $('#open-modal').load('/hotel/index.php/admin/cafeTransaction/modal-edit-payment/'+transaction+'/'+id_payment);
    });

    $('.add-payment').on('click',function (e){
        // e.preventDefault();
        waiting();
        var id = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-add-payment/'+id);
    });

    $('.add-cafe-payment').on('click',function (e){
        // e.preventDefault();
        waiting();
        var id = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/cafeTransaction/modal-add-payment/'+id);
    });

    $('.detail-customers').on('click',function (e){
        waiting();
        // e.preventDefault();
        var customers = $(this).data('customers');
        $('#open-modal').load('/hotel/index.php/admin/customers/modal-detail-customers/'+customers);
    });

    $('.detail-user').on('click',function (e){
        // e.preventDefault();
        var id_user = $(this).data('iduser');
        $('#open-modal').load('/hotel/index.php/admin/users/modal-detail-user/'+id_user);
    });

    // $('.deleted-staff').on('click',function (e){
    //     $('#open-modal').load('/hotel/index.php/admin/users/modal-deleted-user');
    // });

    $('.edit-customers').on('click',function (e){
        var id = $(this).data('customers');
        var tr = $(this).data('tr');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-edit-customers/'+id+'/'+tr);
    });

    $('.edit-transaction').on('click',function (e){
        var id = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-edit-transaction/'+id);
    });


    $('.edit-cafe-transaction').on('click',function (e){
        var id = $(this).data('transaction');
        $('#open-modal').load('/hotel/index.php/admin/cafeTransaction/modal-edit-transaction/'+id);
    });
    // $(document).on('change','.category_product',function(){
    // 	$('.subcategory').attr('data-selected','');
    //     subcategory();
    // });
    // function subcategory(){
    //     var category_product = $('.category_product').val();
    //     console.log(category_product);
    //     $.ajax({
    //         type : 'POST',
    //         url  : '/hotel/index.php/admin/products/subcategory',
    //         data : {id_category : category_product},
    //         beforeSend : function() {
    //             $('.subcategory').html("<option value=''>Loading...</option>");
    //         },
    //         success : function(data){
    //             $('.subcategory').html(data);
    //         }
    //     }).done(function(){
    //         $('.subcategory').val($('.subcategory').attr('data-selected'));
    //     });
    // }

    $('.choose-room').on('click',function (e){
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-choose-room');
    });

    $('.choose-tambahan').on('click',function (e){
        $('#open-modal').load('/hotel/index.php/admin/checkIn/modal-choose-tambahan');
    });

    $(document).on('click','.waiting',function(){
        $('.waiting').addClass('disabled');
    });

    $(document).on('click','.tutup',function(){
        $('.waiting').removeClass('disabled');
    });
    
    $(document).on('click','.close-waiting,.close-button',function(){
        $('.please-waiting').removeClass('disabled');
    });
    $(document).on('click','.please-waiting',function(){
        waiting();
        // $('.please-waiting').addClass('disabled');
    });

});

function waiting(){
    $.blockUI({ 
        message: '<h1 style="font-weight:300">Please Wait</h1>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#fff', 
            '-webkit-border-radius': '0', 
            '-moz-border-radius': '0',  
            color: '#000',
            'text-transform': 'uppercase'
        },
        baseZ:9999
    });
}