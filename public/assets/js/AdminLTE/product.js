$(function() {
    subcategory();

    $(document).on('change','.category_product',function(){
    	$('.subcategory').attr('data-selected','');
        subcategory();
    });
    function subcategory(){
        var category_product = $('.category_product').val();
        console.log(category_product);
        $.ajax({
            type : 'POST',
            url  : '/admin/products/subcategory',
            data : {id_category : category_product},
            beforeSend : function() {
                $('.subcategory').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.subcategory').html(data);
            }
        }).done(function(){
            $('.subcategory').val($('.subcategory').attr('data-selected'));
        });
    }

    
});