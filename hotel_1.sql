-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2018 at 10:37 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.1.15-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `web_keywords` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `gplus` varchar(100) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `home_description` text NOT NULL,
  `blog_description` text NOT NULL,
  `how_to_buy_description` text NOT NULL,
  `order_description` text NOT NULL,
  `contact_description` text NOT NULL,
  `product_description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `email`, `web_keywords`, `address`, `facebook`, `twitter`, `instagram`, `gplus`, `bbm`, `phone`, `home_description`, `blog_description`, `how_to_buy_description`, `order_description`, `contact_description`, `product_description`, `created_at`, `updated_at`) VALUES
(1, 'Hotel Quas', 'hotel@gmail.com', '', 'lembaga pembangunan langkawi', 'hotel', 'hotel', 'hotel', '', '2494612', '2342342', 'home is a', 'blog is a', 'how to buy is a menu', 'order is menu', 'menu is contanct', 'produk adalah', '0000-00-00 00:00:00', '2018-05-18 00:18:31');

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_controller` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `id_group`, `id_controller`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 8, '2015-03-24 09:29:04', '2015-03-24 09:22:59', '2015-03-24 09:29:04'),
(2, 2, 8, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(3, 2, 9, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(4, 2, 17, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(5, 2, 4, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(6, 2, 5, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(7, 2, 6, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(8, 2, 7, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(9, 2, 8, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(10, 2, 9, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(11, 2, 17, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(12, 2, 4, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(13, 2, 5, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(14, 2, 6, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(15, 2, 7, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(16, 2, 8, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(17, 2, 9, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(18, 2, 14, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(19, 2, 17, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(20, 2, 4, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(21, 2, 5, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(22, 2, 6, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(23, 2, 7, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(24, 2, 8, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(25, 2, 9, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(26, 2, 17, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(27, 2, 4, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(28, 2, 5, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(29, 2, 6, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(30, 2, 7, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(31, 2, 8, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(32, 2, 9, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(33, 2, 13, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(34, 2, 14, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(35, 2, 15, '2015-03-24 09:41:22', '2015-03-24 09:41:09', '2015-03-24 09:41:22'),
(36, 2, 17, '2015-03-24 09:41:22', '2015-03-24 09:41:09', '2015-03-24 09:41:22'),
(37, 2, 8, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(38, 2, 9, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(39, 2, 13, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(40, 2, 14, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(41, 2, 15, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(42, 2, 17, '2015-04-14 08:25:40', '2015-03-24 09:41:23', '2015-04-14 08:25:40'),
(43, 2, 8, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(44, 2, 9, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(45, 2, 13, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(46, 2, 14, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(47, 2, 15, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(48, 2, 17, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(49, 2, 22, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(50, 2, 23, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(51, 2, 24, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(52, 2, 25, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(53, 2, 8, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(54, 2, 9, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(55, 2, 10, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(56, 2, 13, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(57, 2, 14, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(58, 2, 15, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(59, 2, 17, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(60, 2, 21, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(61, 2, 22, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(62, 2, 23, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(63, 2, 24, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(64, 2, 25, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(65, 2, 8, '2015-11-16 07:45:36', '2015-04-18 02:14:27', '2015-11-16 07:45:36'),
(66, 2, 9, '2018-02-12 22:39:34', '2015-04-18 02:14:27', '2018-02-12 22:39:34'),
(67, 2, 10, '2015-11-16 07:46:02', '2015-04-18 02:14:27', '2015-11-16 07:46:02'),
(68, 2, 13, '2015-11-16 07:45:40', '2015-04-18 02:14:27', '2015-11-16 07:45:40'),
(69, 2, 14, '2015-11-16 07:45:40', '2015-04-18 02:14:27', '2015-11-16 07:45:40'),
(70, 2, 15, '2015-11-16 07:45:40', '2015-04-18 02:14:27', '2015-11-16 07:45:40'),
(71, 2, 17, '2018-02-12 22:39:34', '2015-04-18 02:14:27', '2018-02-12 22:39:34'),
(72, 2, 21, '2018-02-12 22:39:34', '2015-04-18 02:14:27', '2018-02-12 22:39:34'),
(73, 2, 22, '2015-11-16 07:45:32', '2015-04-18 02:14:27', '2015-11-16 07:45:32'),
(74, 2, 23, '2015-11-16 07:45:55', '2015-04-18 02:14:27', '2015-11-16 07:45:55'),
(75, 2, 24, '2015-11-16 07:45:48', '2015-04-18 02:14:27', '2015-11-16 07:45:48'),
(76, 2, 25, '2015-11-16 07:45:59', '2015-04-18 02:14:27', '2015-11-16 07:45:59'),
(77, 2, 26, '2015-11-16 07:45:51', '2015-04-18 02:14:27', '2015-11-16 07:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `cafe_item`
--

CREATE TABLE `cafe_item` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cafe_item`
--

INSERT INTO `cafe_item` (`id`, `name`, `price`, `stock`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Teh (es/panas)', 2000, 10, 1, 4, NULL, NULL, '2016-08-07 09:32:00', '2018-05-27 03:07:12'),
(2, 'Jeruk (es/panas)', 2500, 5, 1, 4, NULL, NULL, '2016-08-07 09:32:00', '2018-05-27 03:07:08'),
(3, 'Soto Makassar', 5500, 2, 1, 1, NULL, NULL, '2016-08-07 09:32:00', '2018-05-27 03:06:03'),
(4, 'test', 22222, 22, 1, NULL, NULL, '2016-08-07 09:35:24', '2016-08-07 09:35:21', '2016-08-07 09:35:24'),
(5, 'ngeci', 100000, 2, 4, NULL, NULL, '2018-02-12 13:12:38', '2018-02-07 19:52:07', '2018-02-12 13:12:38'),
(6, 'xxxx', 2222222, 2, 4, NULL, NULL, '2018-05-26 07:54:56', '2018-05-26 07:54:44', '2018-05-26 07:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `cafe_payment`
--

CREATE TABLE `cafe_payment` (
  `id` int(11) NOT NULL,
  `id_cafe_transaction` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_method` int(11) DEFAULT NULL,
  `note` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cafe_transaction`
--

CREATE TABLE `cafe_transaction` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `transaction_number` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `table_number` varchar(10) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `note` text,
  `order_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '0. progress, 1. delivered',
  `payment_status` enum('1','0') NOT NULL DEFAULT '0',
  `type` enum('1','0') DEFAULT '0' COMMENT '0. from cafe, 1. from booking',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cafe_transaction_detail`
--

CREATE TABLE `cafe_transaction_detail` (
  `id` int(11) NOT NULL,
  `id_cafe_transaction` int(11) NOT NULL,
  `id_cafe_item` int(11) NOT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `note` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id`, `id_parent`, `name`, `url`, `description`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Blog', NULL, 'Blog Parent', 'fa fa-file-o', '2015-11-16 07:45:21', '2015-03-24 08:02:15', '2015-11-16 07:45:21'),
(2, 1, 'Category', 'news_category', 'Blog Category', '', '2015-11-16 07:45:21', '2015-03-24 08:03:03', '2015-11-16 07:45:21'),
(3, 1, 'List', NULL, 'Blog List', '', '2015-11-16 07:45:21', '2015-03-24 08:05:39', '2015-11-16 07:45:21'),
(4, NULL, 'Products', NULL, 'Products', 'fa fa-list-alt', '2015-11-16 07:45:24', '2015-03-24 08:08:53', '2015-11-16 07:45:24'),
(5, 4, 'Category', 'product_category', 'Product Category', '', '2015-11-16 07:45:24', '2015-03-24 08:11:07', '2015-11-16 07:45:24'),
(6, 4, 'Sub Category', 'product_subcategory', 'Product Sub Category', '', '2015-11-16 07:45:24', '2015-03-24 08:12:13', '2015-11-16 07:45:24'),
(7, 4, 'List', 'products', 'Product List', '', '2015-11-16 07:45:24', '2015-03-24 08:12:36', '2015-11-16 07:45:24'),
(8, NULL, 'Orders', 'orders', 'Orders List', 'fa fa-shopping-cart', '2015-11-16 07:45:36', '2015-03-24 08:13:06', '2015-11-16 07:45:36'),
(9, NULL, 'Customers', 'customers', 'customers List', 'fa fa-users', NULL, '2015-03-24 08:13:32', '2015-11-16 07:46:27'),
(10, NULL, 'Best Seller', 'best', 'Best Seller List', 'fa fa-star', '2015-11-16 07:46:02', '2015-03-24 08:14:07', '2015-11-16 07:46:02'),
(11, NULL, 'Testimony', 'testimony', 'Testimony List', 'fa fa-comments', '2015-11-16 07:45:34', '2015-03-24 08:14:49', '2015-11-16 07:45:34'),
(12, NULL, 'Gallery', 'gallery', 'Gallery List', 'fa fa-picture-o', '2015-11-16 07:45:45', '2015-03-24 08:15:20', '2015-11-16 07:45:45'),
(13, NULL, 'Management Staff', NULL, 'Management Staff Menu', 'fa fa-user', '2015-11-16 07:45:40', '2015-03-24 09:30:57', '2015-11-16 07:45:40'),
(14, 13, 'List', 'users', 'Staff List', '', '2015-11-16 07:45:40', '2015-03-24 09:31:27', '2015-11-16 07:45:40'),
(15, 13, 'Deleted Staff', 'deleted-staff', 'Deleted Staff List', '', '2015-11-16 07:45:40', '2015-03-24 09:31:53', '2015-11-16 07:45:40'),
(17, NULL, 'About', 'about', 'About Menu', 'fa fa-info-circle', NULL, '2015-03-24 08:18:46', '2015-03-24 08:53:07'),
(21, NULL, 'Reports', 'report', 'Reports Menu', 'fa fa-bar-chart-o', NULL, '2015-03-24 09:33:59', '2015-04-14 08:26:05'),
(22, NULL, 'Bank', 'bank', 'Bank is menu for input bank account', 'fa fa-usd', '2015-11-16 07:45:32', '2015-04-14 08:04:07', '2015-11-16 07:45:32'),
(23, NULL, 'How To Buy', 'how', 'is menu for description some step to buy product', 'fa fa-question-circle', '2015-11-16 07:45:55', '2015-04-14 08:05:37', '2015-11-16 07:45:55'),
(24, NULL, 'Banner', 'banner', 'is menu to set banner content', 'fa fa-desktop', '2015-11-16 07:45:48', '2015-04-14 08:06:17', '2015-11-16 07:45:48'),
(25, NULL, 'Statistics', 'statistics', 'is menu to view visitor activity', 'fa fa-signal', '2015-11-16 07:45:59', '2015-04-14 08:08:07', '2015-11-16 07:45:59'),
(26, NULL, 'Messages', 'messages', 'Is all about contact ', 'fa fa-envelope', '2015-11-16 07:45:51', '2015-04-18 02:14:15', '2015-11-16 07:45:51'),
(27, NULL, 'Transactions', NULL, 'transaction menu', 'fa fa-shopping-cart', NULL, '2015-11-16 07:58:53', '2015-11-16 07:58:53'),
(28, 27, 'Check In', 'checkIn', 'Check In Menu', '', NULL, '2015-11-16 07:59:29', '2015-11-16 07:59:29'),
(29, 27, 'Check Out', 'checkOut', 'Check Out Menu', '', NULL, '2015-11-16 08:00:23', '2015-11-16 08:00:23'),
(30, NULL, 'Room', 'room', 'Room Menu', 'fa fa-home', NULL, '2015-11-16 08:01:30', '2015-11-16 08:01:30'),
(31, NULL, 'Room Class', 'roomClass', 'Room Class Menu', 'fa fa-briefcase', NULL, '2015-11-16 08:02:00', '2015-11-16 08:02:00'),
(32, NULL, 'Facilities', 'facility', 'Facilities Menu', 'fa fa-gift', NULL, '2015-11-16 08:02:46', '2015-11-16 08:02:46'),
(33, NULL, 'Additional', 'additional', 'Additional Menu', 'fa fa-tags', NULL, '2015-11-16 08:03:48', '2015-11-16 08:03:48');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_card_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `title`, `full_name`, `email`, `phone`, `id_card_number`, `card_type`, `address`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Mr', 'Nuli Giarsyani', 'giarsyani.nuli@gmail.com', '23323', '2323', 'KTP', 'xxxx', 4, NULL, NULL, NULL, '2018-06-01 01:59:59', '2018-06-01 01:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `name`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'makanan gratis', 'fa fa-cutlery', NULL, NULL, NULL),
(2, 'teh manis', 'fa fa-coffee', NULL, NULL, NULL),
(3, 'antar jemput', 'fa fa-plane', NULL, NULL, NULL),
(5, 'double kasur', 'fa fa-truck', NULL, NULL, NULL),
(6, 'extra bed cover', 'fa fa-archive', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `active` enum('1','0') DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `handphone` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_09_12_131203_create_table_rooms', 1),
('2015_09_12_133846_create_table_facility', 1),
('2015_09_12_133955_create_table_customers', 2),
('2015_09_12_134014_create_table_transactions', 2),
('2015_09_12_134025_create_table_transactions_detail', 2),
('2015_09_12_140223_create_table_transactions_payment', 2),
('2015_09_23_083201_create_table_tambahan', 3);

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`) VALUES
(1, 'ATM'),
(2, 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE `religion` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`id`, `name`) VALUES
(1, 'Islam'),
(2, 'Hindhu');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1: Ada, 2: Dipakai, 3:Booked',
  `kondisi_kamar` enum('1','2') COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1. Bersih, 2. Kotor',
  `facility` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `id_parent`, `name`, `price`, `status`, `kondisi_kamar`, `facility`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Ekonomi', '300000', '1', '1', '1,2', 1, 1, NULL, NULL, NULL, NULL),
(2, 1, 'AA01', '40000000', '1', '1', '1,2', 1, 1, NULL, '2015-11-16 01:51:03', NULL, NULL),
(3, 1, 'AA02', '300000', '1', '2', '1,2', 1, 4, NULL, NULL, NULL, '2018-05-27 03:04:02'),
(4, 1, 'AA03', '300000', '1', '2', '1,2', 1, 0, NULL, NULL, NULL, '2018-06-01 02:00:09'),
(5, 1, 'AA04', '300000', '1', '1', '1,2', 1, 0, NULL, NULL, NULL, '2018-05-27 02:55:23'),
(6, 1, 'AA05', '300000', '1', '1', '1,2', 1, 0, NULL, NULL, NULL, NULL),
(7, 1, 'AA06', '300000', '1', '1', '1,2', 1, 0, NULL, NULL, NULL, NULL),
(8, NULL, 'Executive', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(9, 8, 'EE01', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(10, 8, 'EE02', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(11, 8, 'EE03', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(12, 8, 'EE04', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(13, 8, 'EE05', '400000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(14, NULL, 'Bussines', '350000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(15, 14, 'BB01', '350000', '1', '1', '1,2,3', 1, 0, NULL, NULL, NULL, NULL),
(16, NULL, 'dae', '2000000', NULL, NULL, '1', 1, 0, NULL, NULL, NULL, NULL),
(17, 1, 'AAA04', '400000', '1', '1', '1,2', 1, 0, NULL, NULL, NULL, NULL),
(18, NULL, 'res', '565656', NULL, NULL, '2', 1, 1, NULL, '2015-11-16 02:01:50', NULL, NULL),
(19, 1, 'xeee', '300000', '1', '1', '1,2', 4, 4, NULL, NULL, NULL, NULL),
(20, 1, 'xerrrr', '300000', '1', '1', '1', 4, NULL, NULL, '2018-05-27 02:18:51', NULL, NULL),
(21, 1, 'xxcxcxcxcx', '300000', '1', '1', '1', 4, NULL, NULL, '2018-05-27 02:18:47', NULL, NULL),
(22, 1, 'qwqwq', '300000', '1', '1', '1', 4, NULL, NULL, '2018-05-27 02:18:42', NULL, NULL),
(23, 1, 'ek', '300000', '1', '1', '1,2,1,2,3', 4, NULL, NULL, '2018-05-27 02:22:03', NULL, NULL),
(24, 8, 'exe', '400000', '1', '1', '1,2,1,2,3', 4, NULL, NULL, '2018-05-27 02:21:58', NULL, NULL),
(25, 1, 'xxxx', '300000', '1', '1', '1,2,6', 4, 4, NULL, NULL, NULL, NULL),
(26, NULL, 'zxzxz', '232323', NULL, NULL, '1,2', 4, NULL, NULL, NULL, '2018-05-27 03:06:54', '2018-05-27 03:06:54');

-- --------------------------------------------------------

--
-- Table structure for table `tambahan`
--

CREATE TABLE `tambahan` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tambahan`
--

INSERT INTO `tambahan` (`id`, `name`, `price`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'kasur', '200000', 1, 0, NULL, NULL, NULL, NULL),
(2, 'bantal', '20000', 4, NULL, NULL, NULL, NULL, NULL),
(3, 'xcx', '3434343', 4, NULL, NULL, NULL, NULL, NULL),
(4, 'xxxxx', '23232', 4, 4, NULL, NULL, NULL, '2018-05-27 03:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_customers` int(10) UNSIGNED NOT NULL,
  `checkin_date` datetime NOT NULL,
  `checkout_date` datetime NOT NULL,
  `start_payment` datetime DEFAULT NULL,
  `end_payment` datetime DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `total` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_after_discount` int(11) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guest_status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '1:aktif, 2:out',
  `payment_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0:belum lunas,1:lunas',
  `book_via` enum('1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '1:website, 2:front office',
  `type` enum('1','2') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1:booking, 2:tidak booking',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `checkout_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `id_customers`, `checkin_date`, `checkout_date`, `start_payment`, `end_payment`, `discount`, `total`, `total_after_discount`, `note`, `guest_status`, `payment_status`, `book_via`, `type`, `created_by`, `updated_by`, `deleted_by`, `checkout_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '664836', 1, '2018-06-01 08:00:00', '2018-06-05 08:00:00', NULL, NULL, NULL, '1200000', NULL, NULL, '2', '0', '2', NULL, 4, NULL, NULL, NULL, NULL, '2018-06-01 01:59:59', '2018-06-01 02:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_detail`
--

CREATE TABLE `transactions_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_transactions` int(10) UNSIGNED NOT NULL,
  `id_item` int(10) UNSIGNED DEFAULT NULL,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(10) UNSIGNED DEFAULT NULL,
  `item_price` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('1','0','2') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '2. cafe, 1: room, 0: additional',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions_detail`
--

INSERT INTO `transactions_detail` (`id`, `id_transactions`, `id_item`, `item`, `class`, `qty`, `item_price`, `note`, `type`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'AA03', 'Ekonomi', 1, '300000', '', '1', 4, NULL, NULL, NULL, '2018-06-01 02:00:00', '2018-06-01 02:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_payment`
--

CREATE TABLE `transactions_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_transactions` int(10) UNSIGNED NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions_payment`
--

INSERT INTO `transactions_payment` (`id`, `id_transactions`, `amount`, `payment_date`, `payment_method`, `note`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1300000', '2018-06-01 09:11:00', 2, 'xx', 4, NULL, NULL, '2018-06-01 02:17:18', '2018-06-01 02:11:43', '2018-06-01 02:17:18'),
(2, 1, '200000', '2018-06-01 09:21:00', 2, 'vvvxxxx', 4, 4, NULL, '2018-06-01 02:21:43', '2018-06-01 02:21:26', '2018-06-01 02:21:43'),
(3, 1, '1100000', '2018-06-01 09:21:00', 2, 'ccc', 4, 4, NULL, NULL, '2018-06-01 02:22:01', '2018-06-01 02:30:50'),
(4, 1, '400000', '2018-06-01 09:22:00', 2, 'ccc', 4, 4, NULL, '2018-06-01 02:29:28', '2018-06-01 02:22:57', '2018-06-01 02:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `bbm`, `facebook`, `twitter`, `instagram`, `is_active`, `level`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 'admin', 'admin@example.com', '574181de95e0bd6035ede92464d090f60a6aba90', 'dicoba', '123', '', '', '', '', '1', 1, NULL, '2016-08-23 03:16:42', '2018-02-12 12:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '0000-00-00 00:00:00', '2015-03-24 03:24:36'),
(2, 'Front Officer', '0000-00-00 00:00:00', '2015-09-08 20:38:12'),
(3, 'Owner', '2018-02-12 12:57:30', '2018-02-12 12:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) NOT NULL,
  `id_article` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('1','2','3','4') NOT NULL COMMENT '1. products, 2. news, 3. Gallery, 4. general',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`),
  ADD KEY `id_controller` (`id_controller`);

--
-- Indexes for table `cafe_item`
--
ALTER TABLE `cafe_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cafe_payment`
--
ALTER TABLE `cafe_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cafe_transaction`
--
ALTER TABLE `cafe_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cafe_transaction_detail`
--
ALTER TABLE `cafe_transaction_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religion`
--
ALTER TABLE `religion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tambahan`
--
ALTER TABLE `tambahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions_payment`
--
ALTER TABLE `transactions_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `cafe_item`
--
ALTER TABLE `cafe_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cafe_payment`
--
ALTER TABLE `cafe_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cafe_transaction`
--
ALTER TABLE `cafe_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cafe_transaction_detail`
--
ALTER TABLE `cafe_transaction_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `religion`
--
ALTER TABLE `religion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tambahan`
--
ALTER TABLE `tambahan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_payment`
--
ALTER TABLE `transactions_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
