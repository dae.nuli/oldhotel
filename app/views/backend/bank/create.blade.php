@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/bank', 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" value="{{Input::old('name')}}" class="form-control" id="name" placeholder="Bank Name">
                {{$errors->first('name','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="account">Account</label>
                <input type="text" name="account" value="{{Input::old('account')}}" class="form-control" id="account" placeholder="Account Number">
                {{$errors->first('account','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <a href="javascript:openCustomRoxy()"><img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;"></a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>  
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/bank')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop