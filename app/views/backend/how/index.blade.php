@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        tinymce.init({
            selector: "textarea",
            plugins: [
                ["advlist autolink link image lists charmap hr anchor pagebreak spellchecker"],
                ["searchreplace wordcount insertdatetime media nonbreaking"],
                ["save table contextmenu directionality emoticons template paste"]
            ], 
            schema: "html5", 
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
            style_formats:[
                {title:'Left Margin',selector:'img',styles:{'margin-left':'5px'}},
                {title:'Right Margin',selector:'img',styles:{'margin-right':'5px'}},
                {title:'Top Margin',selector:'img',styles:{'margin-right':'5px'}},
                {title:'Bottom Margin',selector:'img',styles:{'margin-right':'5px'}}
            ],
            relative_urls: false,
            file_browser_callback: RoxyFileBrowser
         });

        function RoxyFileBrowser(field_name, url, type, win) {
          var roxyFileman = '/fileman/index.html?integration=tinymce4';
          if (roxyFileman.indexOf("?") < 0) {     
            roxyFileman += "?type=" + type;   
          }
          else {
            roxyFileman += "&type=" + type;
          }
          roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
          if(tinyMCE.activeEditor.settings.language){
            roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
          }
          tinyMCE.activeEditor.windowManager.open({
             file: roxyFileman,
             title: 'Roxy Fileman',
             width: 850, 
             height: 650,
             resizable: "yes",
             plugins: "media",
             inline: "yes",
             close_previous: "no"  
          }, { window: win, input: field_name });
          return false; 
        }

    });
    </script>

@stop

@section('body-content')
@if(Session::has('how'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('how')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/how', 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">
            <div class="form-group">
                <label for="body">Content</label>
                <textarea id="body" name="content" class="form-control textarea" style="height:200px" rows="3" placeholder="Content ...">{{$how->content}}</textarea>
                {{$errors->first('content','<p class="text-red">:message</p>')}}
            </div> 
        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop