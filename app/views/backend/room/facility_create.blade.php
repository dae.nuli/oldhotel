@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/facility','id'=>'request','method'=>'POST'))}}
        <div class="box-body">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" name="name" value="{{Input::old('name')}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="icon">Icon</label>
                        <input type="text" name="icon" value="{{Input::old('icon')}}" class="form-control modal-icon" id="icon" data-validation="required" data-validation-error-msg="The icon field is required." autocomplete="off">
                        {{$errors->first('icon','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/facility')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
<div id="open-modal"></div>
@stop