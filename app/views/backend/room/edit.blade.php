@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    $(document).on('change','.class',function(){
        var idClass       = $(this).val();
        var selectedClass = $(this);
        if(idClass!=''){
            $.post('{{URL::to("admin/room/find-class")}}', {ClassId: idClass}, function(data, textStatus, xhr) {
                selectedClass.parents('.col-xs-4').next().next().find('.price').val(accounting.formatNumber(data.price, 0, ".", ""));
                if(data.facility!=''){
                    selectedClass.parents('.row').next().find('input[type=checkbox]').prop('checked', false);
                    var facil = data.facility.split(',');
                    $.each(facil,function(index, row) {
                        selectedClass.parents('.row').next().find('input[type=checkbox][value='+row+']').prop('checked', true);
                    });
                }
            });
        }else{
            selectedClass.parents('.col-xs-4').next().next().find('.price').val('');
            selectedClass.parents('.row').next().find('input[type=checkbox]').prop('checked', false);
        }
    });
    $('.price').priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/room/edit/'.$room->id, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="class">Class</label>
                        <select name="class" class="form-control class" id="class">
                            <option value="">- Select Class -</option>
                            @foreach($class as $row)
                                @if($room->id_parent==$row->id)
                                    <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        {{$errors->first('class','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{$room->name}}" class="form-control" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Price</label>
                        <input type="text" name="price" value="{{$room->price}}" class="form-control price" data-validation="required" data-validation-error-msg="The price field is required." autocomplete="off">
                        {{$errors->first('price','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-4">
                    <div class="form-group">
                        <label for="facility">Facility</label>
                        <div class="form-group facility">
                            <?php $label = explode(',', $room->facility); ?>
                            @foreach($facility as $i => $row)
                            <label>
                                <input name="facility[]" type="checkbox" value="{{$row->id}}" {{(array_search($row->id,$label) === false?'':'checked')}}><span>{{$row->name}}</span>
                            </label>
                            @endforeach
                        </div>
                        {{$errors->first('facility','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/room')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop