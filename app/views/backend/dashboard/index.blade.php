@extends('backend.layouts.content')

@section('body-content')
	<div class="row">

            <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            {{$checkIn}}
                        </h3>
                        <p>
                            Check In
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{URL::to('admin/checkIn')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3>
                            {{$checkOut}}
                        </h3>
                        <p>
                            Check Out
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-share"></i>
                    </div>
                    <a href="{{URL::to('admin/checkOut')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            
                        {{-- <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        {{$visitor}}
                                    </h3>
                                    <p>
                                        Unique Visitors
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="{{URL::to('admin/statistics')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div> --}}<!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        {{$customers}}
                                    </h3>
                                    <p>
                                        Customers
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{URL::to('admin/customers')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>

                        
                        
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{$dirtyRoom}}</h3>
                                    <p>
                                        Dirty Room
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-home"></i>
                                </div>
                                <a href="{{URL::to('admin/room/dirty')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>
                                        {{$emptyRoom}}
                                    </h3>
                                    <p>
                                        Empty Room
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-home"></i>
                                </div>
                                <a href="{{URL::to('admin/room')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>
                                        {{$filledRoom}}
                                    </h3>
                                    <p>
                                        Filled Room
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-home"></i>
                                </div>
                                <a href="{{URL::to('admin/room')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        {{$leavingSoon}}
                                    </h3>
                                    <p>
                                        Guest Leaving Soon
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="{{URL::to('admin/checkIn')}}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        
	</div>

    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Transactions Chart</h3>
                    <h3 class="box-title pull-right">
                        {{Form::open(array('url' => 'admin/home', 'method' => 'post'))}}
                        <select class="form-control" name="sales" onchange="this.form.submit()">
                            @foreach($yearRange as $row)
                                <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                            @endforeach
                        </select>
                        {{Form::close()}}
                    </h3>
                </div>
                <div class="box-body chart-responsive">
                    <canvas id="myChart" style="width:100%; height:300px;" height="400"></canvas>
                    {{-- <div class="chart" id="revenue-chart" style="height: 300px;"></div> --}}
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
@stop

@section('end-script')
    @parent
    <script src="{{asset('assets/js/Chart.js')}}"></script>
    <script type="text/javascript">
    $(function () {
        var data = {
            labels: [{{$month}}],
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [{{$chart}}]
                }
            ]
        };
        // Get context with jQuery - using jQuery's .get() method.
        var ctx = $("#myChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var myNewChart = new Chart(ctx).Line(data);
    })
    </script>
@stop