@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src='{{asset("assets/js/jquery.museum.js")}}'></script>
    <script>
        $(document).ready(function() {
            var options = {
                namespace: "msm",       // for custom CSS class namespacing
                padding: 25,            // in case you would like a specific amount of padding on the lightbox
                disable_url_hash: true, // disable using hashes in case your website already uses URL hashes for states
            };
            $.museum($('.images img'),options);
        });
    </script>
@stop

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/banner/create')}}" class="btn btn-primary">Create</a>
</div>

{{Form::open(array('url'=>'admin/banner', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search banner title">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('banner'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('banner')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Title</th>
                <th>Picture</th>
                <th>Status</th>
                <th>Modified at</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($banner as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->title}}</td>
                <td>
                    <?php
                    $paths = public_path($row->picture);
                    ?>
                    @if(!empty($row->picture) && is_file($paths))
                        <img width="100" src="{{asset($row->picture)}}" alt="img"/>
                    @else
                        <img width="100" src="{{asset('assets/store/no_image.png')}}" alt="img"/>
                    @endif 
                </td>
                <td>
                    @if($row->status)
                        <span class="label label-success">Active</span>
                    @else
                        <span class="label label-warning">Not Active</span>
                    @endif
                </td>
                <td>{{date('d F Y, H:i:s',strtotime($row->updated_at))}}</td>
                <td>
                    @if($row->status)
                        <a href="{{URL::to('admin/banner/noactive/'.$row->id)}}" class="btn btn-warning btn-xs noactived"><i class="fa fa-fw fa-times-circle"></i></a>
                    @else
                        <a href="{{URL::to('admin/banner/active/'.$row->id)}}" class="btn btn-success btn-xs actived"><i class="fa fa-fw  fa-check-circle"></i></a>
                    @endif

                    <a href="{{URL::to('admin/banner/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/banner/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$banner->links()}}
    </div>
</div>
@stop