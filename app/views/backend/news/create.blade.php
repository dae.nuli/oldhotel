@extends('backend.layouts.content')

@section('end-script')
    @parent
 
    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        tinymce.init({
            selector: "textarea",
            plugins: [
                ["advlist autolink link image lists charmap hr anchor pagebreak spellchecker"],
                ["searchreplace wordcount insertdatetime media nonbreaking"],
                ["save table contextmenu directionality emoticons template paste"]
            ], 
            schema: "html5", 
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
            style_formats:[
                {title:'Left Margin',selector:'img',styles:{'margin-left':'5px'}},
                {title:'Right Margin',selector:'img',styles:{'margin-right':'5px'}},
                {title:'Top Margin',selector:'img',styles:{'margin-right':'5px'}},
                {title:'Bottom Margin',selector:'img',styles:{'margin-right':'5px'}},
                {title:'Float Left',selector:'img',styles:{'float':'left'}},
                {title:'Float Right',selector:'img',styles:{'float':'right'}}
            ],
            relative_urls: false,
            file_browser_callback: RoxyFileBrowser
         });

        function RoxyFileBrowser(field_name, url, type, win) {
          var roxyFileman = '/fileman/index.html?integration=tinymce4';
          if (roxyFileman.indexOf("?") < 0) {     
            roxyFileman += "?type=" + type;   
          }
          else {
            roxyFileman += "&type=" + type;
          }
          roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
          if(tinyMCE.activeEditor.settings.language){
            roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
          }
          tinyMCE.activeEditor.windowManager.open({
             file: roxyFileman,
             title: 'Roxy Fileman',
             width: 850, 
             height: 650,
             resizable: "yes",
             plugins: "media",
             inline: "yes",
             close_previous: "no"  
          }, { window: win, input: field_name });
          return false; 
        }

    });
    </script>

@stop

@section('body-content')
<div class="box"> 
    <div class="box-header">
        <!-- <h3 class="box-title">{{$title}}</h3> -->
    </div><!-- /.box-header -->
    {{Form::open(array('url'=>'admin/news', 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">  
            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="category">
                    <option value="">-Select Category-</option>
                    @foreach($category as $row)
                        @if($row->id == Input::old('category'))
                            <option value="{{Input::old('category')}}" selected>{{$row->name}}</option>
                        @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                        @endif
                    @endforeach
                </select>
                {{$errors->first('category','<p class="text-red">:message</p>')}}
            </div>  
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" value="{{Input::old('title')}}" class="form-control" id="title" placeholder="News Title">
                {{$errors->first('title','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="body">Content</label>
                <textarea id="body" name="content" class="form-control textarea" style="height:200px" rows="3" placeholder="Content ...">{{Input::old('content')}}</textarea>
                {{$errors->first('content','<p class="text-red">:message</p>')}}
            </div>  
            <div class="form-group">
                <label for="file">Picture</label>
                <input type="file" name="picture" id="file">
                <p class="help-block">Example block-level help text here.</p>
                {{$errors->first('picture','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group"> 
                <select class="form-control" id="note" name="note"> 
                    <option value="0">Draft</option> 
                    <option value="1" selected>Publish</option> 
                </select> 
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/news')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop