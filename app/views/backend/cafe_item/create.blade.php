@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a style="margin:10px 10px 0 10px;color:white" class="btn btn-sm btn-primary add-item">Insert New Row</a>
</div>
@stop

@section('end-script')
    @parent
    <script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    $('.price').priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
    function formats(){
        $(this).priceFormat({
            prefix:'',
            thousandsSeparator: '.',
            centsLimit: 0
        });
    }
    var max_fields      = 15; //maximum input boxes allowed
    var wrapper         = $(".add-dinamic"); //Fields wrapper
    var add_button      = $(".add-item"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="box">'+
                '<div class="box-body"><a class="btn btn-danger pull-right btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> Remove</a>'+
                    '<div class="row">'+
                        '<div class="col-xs-4">'+
                            '<div class="form-group">'+
                                '<label>Name</label>'+
                                '<input type="text" name="name[]" class="form-control" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<div class="form-group">'+
                                '<label>Price</label>'+
                                '<input type="text" name="price[]" class="form-control price" data-validation="required" data-validation-error-msg="The price field is required." autocomplete="off">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-xs-3">'+
                            '<div class="form-group">'+
                                '<label>Stock</label>'+
                                '<input type="text" name="stock[]" class="form-control" data-validation="required" data-validation-error-msg="The stock field is required." autocomplete="off">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>').on('keyup','.price',formats);
        }
    });
    
    $(document).on("click",".delete-item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.add-dinamic div').remove(); x--;
    })
    </script>

@stop

@section('body-content')
{{Form::open(array('url'=>$action, 'id'=>'request', 'method'=>'POST'))}}
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name[]" class="form-control" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                    {{$errors->first('name','<p class="text-red">:message</p>')}}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Price</label>
                    <input type="text" name="price[]" class="form-control price" data-validation="required" data-validation-error-msg="The price field is required." autocomplete="off">
                    {{$errors->first('price','<p class="text-red">:message</p>')}}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label>Stock</label>
                    <input type="text" name="stock[]" class="form-control" data-validation="required" data-validation-error-msg="The stock field is required." autocomplete="off">
                    {{$errors->first('stock','<p class="text-red">:message</p>')}}
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div>
<div class="add-dinamic"></div>
<div class="box-footer">
    <a href="{{URL::to($url)}}" class="btn btn-default">{{trans('button.bc')}}</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
{{Form::close()}}
@stop