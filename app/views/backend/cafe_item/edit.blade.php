@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    $('.price').priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>$action, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{$index->name}}" class="form-control" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Price</label>
                        <input type="text" name="price" value="{{$index->price}}" class="form-control price" data-validation="required" data-validation-error-msg="The price field is required." autocomplete="off">
                        {{$errors->first('price','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Stock</label>
                        <input type="text" name="stock" value="{{$index->stock}}" class="form-control" data-validation="required" data-validation-error-msg="The stock field is required." autocomplete="off">
                        {{$errors->first('stock','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to($url)}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop