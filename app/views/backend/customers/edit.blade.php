@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/customers/update/'.$customers->id, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="row">
                <div class="col-xs-2">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <select name="title" class="form-control" id="title" data-validation="required" data-validation-error-msg="The title field is required.">
                            <option value=" ">- Select Title -</option>
                            <option value="Mr" {{($customers->title=='Mr')?'selected':''}}>Mr</option>
                            <option value="Mrs" {{($customers->title=='Mrs')?'selected':''}}>Mrs</option>
                        </select>
                        {{$errors->first('title','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="name">Customer Name</label>
                        <input type="text" name="name" autocomplete="off" value="{{$customers->full_name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The customer name field is required.">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="card_type">Card Type</label>
                        <select name="card_type" class="form-control" id="card_type" data-validation="required" data-validation-error-msg="The card type field is required.">
                            <option value=" ">- Select Card Type -</option>
                            <option value="KTP" {{($customers->card_type=='KTP')?'selected':''}}>KTP</option>
                            <option value="SIM" {{($customers->card_type=='SIM')?'selected':''}}>SIM</option>
                            <option value="PASPOR" {{($customers->card_type=='PASPOR')?'selected':''}}>PASPOR</option>
                        </select>
                        {{$errors->first('card_type','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="id_card">ID Card</label>
                        <input type="text" name="id_card" autocomplete="off" value="{{$customers->id_card_number}}" class="form-control" id="id_card" data-validation="number" data-validation-error-msg="The id card field is required and must be a number.">
                        {{$errors->first('id_card','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" autocomplete="off" value="{{$customers->address}}" class="form-control" id="address" data-validation="required" data-validation-error-msg="The address field is required.">
                        {{$errors->first('address','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="phone">Phone Number</label>
                        <input type="text" name="phone" autocomplete="off" value="{{$customers->phone}}" class="form-control" id="phone" data-validation="number" data-validation-error-msg="The phone field is required and must be a number.">
                        {{$errors->first('phone','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" autocomplete="off" value="{{$customers->email}}" class="form-control" id="email" data-validation="email" data-validation-error-msg="The email must be a valid email address." data-validation-optional="true">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/customers')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop