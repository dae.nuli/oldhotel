<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Customers Detail</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" disabled="" value="{{$customers->title}}. {{$customers->full_name}}">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" disabled="" value="{{$customers->email}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label>Card Type</label>
                                <input type="text" class="form-control" disabled="" value="{{$customers->card_type}}">
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label>Card Number</label>
                                <input type="text" class="form-control" disabled="" value="{{$customers->id_card_number}}">
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" disabled="" value="{{$customers->phone}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="note">Address</label>
                                <textarea class="form-control" disabled="" rows="3">{{$customers->address}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>