<!-- sidebar menu: : style can be found in sidebar.less -->
<?php
    $uri3  = Request::segment(3);
    $uri   = Request::segment(2);
    $uri1  = Request::segment(1);
    $admin = Session::get('admin');
?>
                    <ul class="sidebar-menu">
                        <li @if($uri1=='admin' && $uri==' ') class="active" @endif>
                            <a href="{{URL::to('admin/home')}}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        @if($admin['level']==1)
                        <li class="treeview @if($uri=='checkOut'||$uri=='checkIn'||$uri=='cafeTransaction') active @endif">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Transactions</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='checkIn') class="active" @endif><a href="{{URL::to('admin/checkIn')}}"><i class="fa fa-angle-double-right"></i> Check In</a></li>
                                <li @if($uri=='checkOut') class="active" @endif><a href="{{URL::to('admin/checkOut')}}"><i class="fa fa-angle-double-right"></i> Check Out</a></li>
                                <li @if($uri=='cafeTransaction') class="active" @endif><a href="{{URL::to('admin/cafeTransaction')}}"><i class="fa fa-angle-double-right"></i> Cafe</a></li>
                            </ul>
                        </li>
                        {{-- <li class="treeview @if($uri=='roomClass'||$uri=='room'||$uri=='facility') active @endif">
                            <a href="#">
                                <i class="fa fa-file-o"></i>
                                <span>Room</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='roomClass') class="active" @endif><a href="{{URL::to('admin/roomClass')}}"><i class="fa fa-angle-double-right"></i> Class</a></li>
                                <li @if($uri=='room') class="active" @endif><a href="{{URL::to('admin/room')}}"><i class="fa fa-angle-double-right"></i> List</a></li>
                                <li @if($uri=='facility') class="active" @endif><a href="{{URL::to('admin/facility')}}"><i class="fa fa-angle-double-right"></i> Facility</a></li>
                            </ul>
                        </li> --}}
{{--                         <li class="treeview @if($uri=='room'||$uri=='roomClass') active @endif">
                            <a href="#">
                                <i class="fa fa-home"></i>
                                <span>Room</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='room') class="active" @endif><a href="{{URL::to('admin/room')}}"><i class="fa fa-angle-double-right"></i> Class</a></li>
                                <li @if($uri=='roomClass') class="active" @endif><a href="{{URL::to('admin/roomClass')}}"><i class="fa fa-angle-double-right"></i> Room List</a></li>
                            </ul>
                        </li> --}}

{{--                         <li @if($uri=='room') class="active" @endif>
                            <a href="{{URL::to('admin/room')}}">
                                <i class="fa fa-home"></i> <span>Room</span>
                            </a>
                        </li>
                        <li @if($uri=='roomClass') class="active" @endif>
                            <a href="{{URL::to('admin/roomClass')}}">
                                <i class="fa fa-briefcase"></i> <span>Room Class</span>
                            </a>
                        </li> --}}
{{--                         <li @if($uri=='facility') class="active" @endif>
                            <a href="{{URL::to('admin/facility')}}">
                                <i class="fa fa-gift"></i> <span>Facilities</span>
                            </a>
                        </li> --}}
{{--                         <li @if($uri=='extracharge') class="active" @endif>
                            <a href="{{URL::to('admin/extracharge')}}">
                                <i class="fa fa-file-o"></i> <span>Extra Charge</span>
                            </a>
                        </li> --}}
                        <li class="treeview @if($uri=='customers'||$uri=='users') active @endif">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>User</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='customers') class="active" @endif><a href="{{URL::to('admin/customers')}}"><i class="fa fa-angle-double-right"></i> Customers</a></li>
                                <li @if($uri=='users') class="active" @endif><a href="{{URL::to('admin/users')}}"><i class="fa fa-angle-double-right"></i> Staff</a></li>
                            </ul>
                        </li>
                        <li class="treeview @if($uri=='extracharge'||$uri=='room'||$uri=='roomClass'||$uri=='facility'||$uri=='cafeItem') active @endif">
                            <a href="#">
                                <i class="fa fa-home"></i>
                                <span>Master</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='extracharge') class="active" @endif><a href="{{URL::to('admin/extracharge')}}"><i class="fa fa-angle-double-right"></i> Extra Charge</a></li>
                                <li @if($uri=='room') class="active" @endif><a href="{{URL::to('admin/room')}}"><i class="fa fa-angle-double-right"></i> Room Class</a></li>
                                <li @if($uri=='roomClass') class="active" @endif><a href="{{URL::to('admin/roomClass')}}"><i class="fa fa-angle-double-right"></i> Room List</a></li>
                                <li @if($uri=='facility') class="active" @endif><a href="{{URL::to('admin/facility')}}"><i class="fa fa-angle-double-right"></i> Facilities</a></li>
                                <li @if($uri=='cafeItem') class="active" @endif><a href="{{URL::to('admin/cafeItem')}}"><i class="fa fa-angle-double-right"></i> Cafe Item</a></li>
                            </ul>
                        </li>
                        <!-- <li class="treeview @if($uri=='products'||$uri=='product_category'||$uri=='product_subcategory') active @endif">
                            <a href="#">
                                <i class="fa fa-list-alt"></i>
                                <span>Products</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='product_category') class="active" @endif><a href="{{URL::to('admin/product_category')}}"><i class="fa fa-angle-double-right"></i> Category</a></li>
                                <li @if($uri=='product_subcategory') class="active" @endif><a href="{{URL::to('admin/product_subcategory')}}"><i class="fa fa-angle-double-right"></i> Sub Category</a></li>
                                <li @if($uri=='products') class="active" @endif><a href="{{URL::to('admin/products')}}"><i class="fa fa-angle-double-right"></i> List</a></li>
                            </ul>
                        </li>
                         
                        <li @if($uri=='orders') class="active" @endif>
                            <a href="{{URL::to('admin/orders')}}">
                                <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                            </a>
                        </li>
                        
                        <li @if($uri=='best') class="active" @endif>
                            <a href="{{URL::to('admin/best')}}">
                                <i class="fa fa-star"></i> <span>Best Seller</span>
                            </a>
                        </li>
                        <li @if($uri=='testimony') class="active" @endif>
                            <a href="{{URL::to('admin/testimony')}}">
                                <i class="fa fa-comments"></i> <span>Testimony</span>
                            </a>
                        </li>
                        <li @if($uri=='gallery') class="active" @endif>
                            <a href="{{URL::to('admin/gallery')}}">
                                <i class="fa fa-picture-o"></i> <span>Gallery</span>
                            </a>
                        </li> 
                        <li class="treeview @if($uri=='users') active @endif">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Management Staff</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='users') class="active" @endif>
                                    <a href="{{URL::to('admin/users')}}">
                                        <i class="fa fa-angle-double-right"></i> List
                                    </a>
                                </li>
                                <li @if($uri=='deleted-staff') class="active" @endif>
                                    <a href="{{URL::to('admin/deleted-staff')}}">
                                        <i class="fa fa-angle-double-right"></i> Deleted Staff
                                    </a>
                                </li>
                            </ul> 
                        </li>

                        
                        
                        <li @if($uri=='bank') class="active" @endif>
                            <a href="{{URL::to('admin/bank')}}">
                                <i class="fa fa-usd"></i> <span>Bank</span>
                            </a>
                        </li>
                        <li @if($uri=='how') class="active" @endif>
                            <a href="{{URL::to('admin/how')}}">
                                <i class="fa fa-question-circle"></i> <span>How To Buy</span>
                            </a>
                        </li>
                        <li @if($uri=='banner') class="active" @endif>
                            <a href="{{URL::to('admin/banner')}}">
                                <i class="fa fa-desktop"></i> <span>Banner</span>
                            </a>
                        </li>
                        <li @if($uri=='statistics') class="active" @endif>
                            <a href="{{URL::to('admin/statistics')}}">
                                <i class="fa fa-signal"></i> <span>Statistics</span>
                            </a>
                        </li>
                        <li @if($uri=='messages') class="active" @endif>
                            <a href="{{URL::to('admin/messages')}}">
                                <i class="fa fa-envelope"></i> <span>Messages</span>
                            </a>
                        </li> -->
                        <li class="treeview @if($uri=='report'||$uri=='cafeReport') active @endif">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Report</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='report') class="active" @endif><a href="{{URL::to('admin/report')}}"><i class="fa fa-angle-double-right"></i> Room</a></li>
                                <li @if($uri=='cafeReport') class="active" @endif><a href="{{URL::to('admin/cafeReport')}}"><i class="fa fa-angle-double-right"></i> Cafe</a></li>
                            </ul>
                        </li>
                        {{-- <li @if($uri=='report') class="active" @endif>
                            <a href="{{URL::to('admin/report')}}">
                                <i class="fa fa-bar-chart-o"></i> <span>Report</span>
                            </a>
                        </li> --}}
{{--                         <li @if($uri=='customers') class="active" @endif>
                            <a href="{{URL::to('admin/customers')}}">
                                <i class="fa fa-users"></i> <span>Customers</span>
                            </a>
                        </li>
                        <li @if($uri=='users') class="active" @endif>
                            <a href="{{URL::to('admin/users')}}">
                                <i class="fa fa-user"></i> <span>Staff</span>
                            </a>
                        </li>  --}}
                        <li @if($uri=='about') class="active" @endif>
                            <a href="{{URL::to('admin/about')}}">
                                <i class="fa fa-info-circle"></i> <span>About</span>
                            </a>
                        </li> 
                        <li class="treeview @if($uri=='group' || $uri=='permission') active @endif">
                            <a href="#">
                                <i class="fa fa-list-ul"></i>
                                <span>Group Permission</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='group') class="active" @endif>
                                    <a href="{{URL::to('admin/group')}}">
                                        <i class="fa fa-angle-double-right"></i> Group
                                    </a>
                                </li>
                                <li @if($uri=='permission') class="active" @endif>
                                    <a href="{{URL::to('admin/permission')}}">
                                        <i class="fa fa-angle-double-right"></i> Permission
                                    </a>
                                </li>
                            </ul> 
                        </li>
                        @else
                        {{Permission::ShowMenu($admin['level'],$uri,$uri1)}}
                        @endif
                    </ul>