<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Customers</h4>
            </div>
            {{Form::open(array('url'=>'admin/checkIn/update-customers/'.$user->id.'/'.$tr, 'id'=>'request', 'method'=>'POST'))}}
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label>Title</label>
                                <select name="title" class="form-control" id="title" data-validation="required" data-validation-error-msg="The title field is required.">
                                    <option value=" ">- Select Title -</option>
                                    <option value="Mr" {{($user->title=='Mr')?'selected':''}}>Mr</option>
                                    <option value="Mrs" {{($user->title=='Mrs')?'selected':''}}>Mrs</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Customer Name</label>
                                <input type="text" name="name" autocomplete="off" value="{{$user->full_name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The customer name field is required.">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="card_type">Card Type</label>
                                <select name="card_type" class="form-control" id="card_type" data-validation="required" data-validation-error-msg="The card type field is required.">
                                    <option value=" ">- Select Card Type -</option>
                                    <option value="KTP" {{($user->card_type=='KTP')?'selected':''}}>KTP</option>
                                    <option value="SIM" {{($user->card_type=='SIM')?'selected':''}}>SIM</option>
                                    <option value="PASPOR" {{($user->card_type=='PASPOR')?'selected':''}}>PASPOR</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="id_card">ID Card</label>
                                <input type="text" name="id_card" autocomplete="off" value="{{$user->id_card_number}}" class="form-control" id="id_card" data-validation="number" data-validation-error-msg="The id card field is required and must be a number.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" name="address" autocomplete="off" value="{{$user->address}}" class="form-control" id="address" data-validation="required" data-validation-error-msg="The address field is required.">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="text" name="phone" autocomplete="off" value="{{$user->phone}}" class="form-control" id="phone" data-validation="number" data-validation-error-msg="The phone field is required and must be a number.">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" autocomplete="off" value="{{$user->email}}" class="form-control" id="email" data-validation="email" data-validation-error-msg="The email must be a valid email address." data-validation-optional="true">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>