@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/checkIn/goout/'.$transaction->id)}}" class="btn btn-danger go-out"><i class="fa fa-fw fa-sign-out"></i> Check Out</a>
</div>
<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary add-payment" data-transaction="{{$transaction->id}}"><i class="fa fa-fw fa-credit-card"></i> Add Payment</a>
</div>
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/checkIn/print/'.$transaction->id)}}" target="_blank" class="btn btn-primary"><i class="fa fa-fw fa-print"></i> Print</a>
</div>
@stop

@section('end-script')
    @parent

    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>

    <link href="{{asset('assets/css/bootstrap-editable.css')}}" rel="stylesheet">
    <script src="{{asset('assets/js/bootstrap-editable.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script type="text/javascript">
    jQuery('.datetimepicker2').datetimepicker({
        closeOnDateSelect:true,
        format:'Y-m-d',
        timepicker:false,
        scrollInput:false
    });
    $(document).ready(function() {
         
    });
    </script>

@stop
 

@section('body-content-child')
@if(Session::has('booking'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('booking')}}.
    </div>
@endif
@if(Session::has('warning'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('warning')}}.
    </div>
@endif
{{-- {{Form::open(array('url'=>'admin/booking/detail/'.$transaction->id, 'method'=>'POST', 'files'=>true))}} --}}
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Customer</h3>
                    <div class="pull-right box-tools">
                        <a style="margin:10px 10px 0 10px;color:white" href="#" class="btn btn-primary btn-xs edit-customers please-waiting" data-tr="{{$transaction->id}}" data-customers="{{$customers->id}}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <p>{{$customers->title}}. {{$customers->full_name}}</p>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <p>{{($customers->email)?$customers->email:'-'}}</p>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <p>{{$customers->phone}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ID Type</label>
                                <p>{{$customers->card_type}}</p>
                            </div>
                            <div class="form-group">
                                <label>ID Number</label>
                                <p>{{$customers->id_card_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <p>{{$customers->address}}</p>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Address</label>
                                <textarea class="form-control" rows="3" disabled="">{{$customers->address}}</textarea>
                            </div>
                        </div>
                    </div> --}}
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Transaction</h3>
                    <div class="pull-right box-tools">
                        <a style="margin:10px 10px 0 10px;color:white" href="#" class="btn btn-primary btn-xs edit-transaction please-waiting" data-transaction="{{$transaction->id}}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Transaction ID</label>
                                <p>{{$transaction->code}}</p>
                                {{-- <p>{{$orders->code_order}} {{Helper::CheckPayment($orders->id)}}</p> --}}
                            </div>
                            <div class="form-group">
                                <label>Transaction Date</label>
                                <p>{{date('d F Y',strtotime($transaction->created_at))}}</p>
                            </div>
                            <div class="form-group">
                                <label>Status Payment</label>
                                <p>{{($transaction->payment_status==1)?'<span class="label label-success">PAID</span>':'<span class="label label-danger">UNPAID</span>'}}</p>
                            </div>
                            
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Check in date</label>
                                <p>{{date('d F Y H:i',strtotime($transaction->checkin_date))}}</p>
                                {{-- <p>{{$orders->code_order}} {{Helper::CheckPayment($orders->id)}}</p> --}}
                            </div>
                            <div class="form-group">
                                <label>Check out date</label>
                                <p>{{date('d F Y H:i',strtotime($transaction->checkout_date))}} <b>({{Helper::countDay($transaction->checkin_date,$transaction->checkout_date)}} days)</b></p>
                            </div>
                            <div class="form-group">
                                {{-- <div class="row"> --}}
                                    {{-- <div class="col-md-12"> --}}
                                        <label>Total</label>
                                        <p>Rp <span class="total">{{($transaction->total)?number_format($transaction->total,0,",","."):'-'}}</span></p>
                                    {{-- </div> --}}
{{--                                     <div class="col-md-3">
                                        <label>Discount</label>
                                        <p>{{$transaction->discount}} %</p>
                                    </div>
                                    <div class="col-md-5">
                                        <label>After discount</label>
                                        <p>Rp <span class="total">{{number_format($transaction->total_after_discount,0,",",".")}}</span></p>
                                    </div> --}}
                                {{-- </div> --}}
                            </div>

                            @if(isset($cafeTransaction))
                            <div class="form-group">
                                <label for="exampleInputPassword1">Table Number</label>
                                <p>{{$cafeTransaction->table_number}}</p>
                            </div>
                            @endif
                            {{-- <div class="form-group">
                                <label>Max Payment Date</label>
                                <p>{{date('d F Y H:i',strtotime($transaction->end_payment))}}</p>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

       {{--  <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="form-group">
                        <label>Note</label>
                        <textarea class="form-control" rows="3" name="note" placeholder="Note of operator"></textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div> --}}
    </div>
</div>
<div class="col-xs-6">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title pull-left">Rooms</h3>
            <div class="pull-right" style="margin-left:5px">
                <a style="margin:10px 10px 0 10px;color:white" data-transaction="{{$transaction->id}}" class="btn btn-sm btn-primary add-room">Add Room</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Room Number</th>
                        <th>Class</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($detailRoom)>0)
                        @foreach($detailRoom as $row)
                        <tr>
                            <td>{{$row->item}}</td>
                            <td>{{$row->class}}</td>
                            <td>Rp {{number_format($row->item_price,0,",",".")}}</span></td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{URL::to('admin/checkIn/delete-room/'.$row->id)}}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>   
                        @endforeach
                    @else
                        <tr><td colspan="6"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<div class="col-xs-6">
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title pull-left">Extra Charge</h3>
            <div class="pull-right" style="margin-left:5px">
                <a style="margin:10px 10px 0 10px;color:white" data-transaction="{{$transaction->id}}" class="btn btn-sm btn-primary add-adds">Add Item</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($detailAdds)>0)
                        @foreach($detailAdds as $row)
                        <tr>
                            <td>{{$row->item}}</td>
                            <td>Rp {{number_format($row->item_price,0,",",".")}}</span></td>
                            <td>{{$row->qty}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{URL::to('admin/checkIn/delete-item/'.$row->id)}}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>   
                        @endforeach
                    @else
                        <tr><td colspan="6"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- <div class="col-xs-12">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Cafe</h3>
            <div class="pull-right" style="margin-left:5px">
                <a style="margin:10px 10px 0 10px;color:white" data-transaction="{{$transaction->id}}" class="btn btn-sm btn-primary add-cafe">Order Cafe</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Note</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($detailCafe))
                    @foreach($detailCafe as $rowCafe)
                        <tr>
                            <td>{{$rowCafe->item_name}}</td>
                            <td>Rp {{number_format($rowCafe->item_price,0,",",".")}}</span></td>
                            <td>{{$rowCafe->qty}}</td>
                            <td>{{($rowCafe->note)?$rowCafe->note:'-'}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{URL::to('admin/checkIn/delete-cafe-item/'.$rowCafe->id.'/'.$transaction->id)}}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td colspan="5"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div> --}}
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Payment</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Payment Method</th>
                        <th>Amount</th>
                        <th>Note</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($payments)>0)
                        @foreach($payments as $row)
                        <tr>
                            <td>{{date("d F Y H:i",strtotime($row->payment_date))}}</td>
                            <td>{{$row->method->name}}</td>
                            <td>Rp {{number_format($row->amount,0,",",".")}}</td>
                            <td>{{$row->note}}</td>
                            <td><div class="btn-group"><a class="btn btn-sm btn-default edit-payment" data-transaction="{{$transaction->id}}" data-idpayment="{{$row->id}}"><i class="fa fa-edit"></i></a><a href="{{URL::to('admin/checkIn/delete-payment/'.$row->id.'/'.$row->id_transactions)}}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a></div></td>
                        </tr> 
                        @endforeach
                    @else
                        <tr><td colspan="5"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <ul>
                <li>Payment Total : <b>Rp {{number_format($totalPayment,0,",",".")}}</b></li>
                <li>Cashback : 
                @if($totalPayment)
                    <?php $tot = $totalPayment-$transaction->total; ?>
                    @if($tot<0)
                        <b>Rp 0</b>
                    @else
                        <b>Rp {{number_format($tot,0,",",".")}}</b>
                    @endif
                @else
                    <b>Rp 0</b>
                @endif
                </li>
            </ul>
        </div>
    </div>
</div>
 
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to('admin/checkIn')}}" class="btn btn-default">{{trans('button.bc')}}</a>
        {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
    </div>
</div>
{{-- {{Form::close()}} --}}
<div id="open-modal"></div>
@stop