<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<!-- DATA TABLES -->
<link href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- DATA TABES SCRIPT -->
<script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "aoColumnDefs": [ { "aTargets": [0,2,4], "bSortable": false } ],
            "aaSorting": [[ 3, "asc" ]]
        });
    });
</script>
<script type="text/javascript">
  $('[data-toggle="tooltip"]').tooltip();
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-room">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Room</h4>
            </div>
            {{Form::open(array('url'=>'admin/checkIn/add-room/'.$transaction->id, 'id'=>'request', 'method'=>'POST'))}}
            <div class="modal-body table-room">
                <div id="room-errors"></div>
                <div class="box-bodytable-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width: 20%">Class</th>
                            <th>Facility</th>
                            <th>Price</th>
                            <th style="width: 10%">Choose</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($rooms as $row)
                            <tr>
                                <td>{{$row->name}}</td>
                                <td>{{$row->classRoom->name}}</td>
                                <td><div data-toggle="tooltip" data-html="true" data-placement="top" title="{{Helper::findFacilityName($row->id)}}">{{Helper::findFacilityIcon($row->id)}}</td>
                                <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                <td><input type="checkbox" class="choose-room-id" name="room[]" value="{{$row->id}}" data-price="{{$row->price}}" data-validation="checkbox_group" data-validation-qty="1-2" data-validation-error-msg="You have to choose the room or maximum 2 room." data-validation-error-msg-container="#room-errors"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
