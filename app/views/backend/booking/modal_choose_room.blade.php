<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
    var room = $('.room-value').val();
    if(room!=''){
        var rooms = room.split(',');
        $.each(rooms,function(index, row) {
            $('input[type=checkbox][value='+row+']').prop('checked', true);
        });
    }
});
</script>
<!-- DATA TABLES -->
<link href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- DATA TABES SCRIPT -->
<script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "aoColumnDefs": [ { "aTargets": [0,2,4], "bSortable": false } ],
            "aaSorting": [[ 3, "asc" ]]
        });
    });
</script>
<script type="text/javascript">
  $('[data-toggle="tooltip"]').tooltip();
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-room">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button keep-room" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose The Room</h4>
            </div>
            <div class="modal-body table-room">
                <div class="box-bodytable-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width: 20%">Class</th>
                            <th>Facility</th>
                            <th>Price</th>
                            <th style="width: 10%">Choose</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($rooms as $row)
                            <tr>
                                <td>{{$row->name}}</td>
                                <td>{{$row->classRoom->name}}</td>
                                <td><div data-toggle="tooltip" data-html="true" data-placement="top" title="{{Helper::findFacilityName($row->id)}}">{{Helper::findFacilityIcon($row->id)}}</td>
                                <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                <td><input type="checkbox" class="choose-room-id" value="{{$row->id}}" data-price="{{$row->price}}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting keep-room" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
