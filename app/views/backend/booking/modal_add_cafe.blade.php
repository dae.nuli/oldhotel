<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});

$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});

$('.price').priceFormat({
    prefix:'',
    thousandsSeparator: '',
    centsLimit: 0
});


$(document).on('click','.choose-item',function(){
    var insertedQTY = $(this).parents('td').prev().prev().find('.input-qty').val();
    var note        = $(this).parents('td').prev().find('.note').val();
    if(insertedQTY!=""){
        var name   = $(this).data('name');
        var price  = $(this).data('price');
        var id     = $(this).data('id');
        var amount = (price*insertedQTY);
        $('<span><input type="hidden" name="id[]" value="'+id+'">'+
        '<input type="hidden" name="qty[]" value="'+insertedQTY+'">'+
        '<input type="hidden" name="note[]" value="'+note+'"></span>').appendTo('.insert-item');
        $(this).parents('tbody').find('tr[data-id="'+id+'"]').hide('slow');
        $(this).attr('disabled', 'disabled');
    }
});

$(document).on('keyup','.input-qty',function(){
    var qty   = $(this).val();
    var stock = $(this).data('stock');
    if(qty>0){
        if(qty>stock){
            $(this).parents('td').next().next().find('a').removeClass('choose-item').addClass('chose');
            alert('stock is less than '+stock);
            $(this).val('');
        }else{
            $(this).parents('td').next().next().find('a').removeClass('chose').addClass('choose-item');
        }
    }else{
        $(this).parents('td').next().find('a').addClass('chose');
    }
});

</script>
<!-- DATA TABLES -->
<link href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- DATA TABES SCRIPT -->
<script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": false,
            "bInfo": false,
            "bAutoWidth": false

            // "bPaginate": false,
            // "bLengthChange": false,
            // "bFilter": false,
            // "bSort": true,
            // "bInfo": false,
            // "bAutoWidth": false,
            // "aoColumnDefs": [ { "aTargets": [0,2,4], "bSortable": false } ],
            // "aaSorting": [[ 3, "asc" ]]
        });
    });
</script>
<style type="text/css">
.radio input[type="radio"]{
    margin-left: 0;
    margin-right: 10px;
}
.chose{
    pointer-events: none;
    cursor: default;
    opacity: 0.6;
}
</style>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-cafe">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Order Cafe</h4>
            </div>
            {{Form::open(array('url'=>$action, 'id'=>'request', 'method'=>'POST'))}}
            <div class="insert-item" style="display:none"></div>
            <div class="modal-body table-room">
                {{-- <div class="container"> --}}
                {{-- <div class="row"> --}}
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Table Number</label>
                            <input type="text" name="number" autocomplete="off" class="form-control" data-validation="required" data-validation-error-msg="<small>The table number field is required.</small>">
                        </div>
                    </div>
                {{-- </div> --}}
                {{-- </div> --}}
                <div id="room-errors"></div>
                <div class="box-bodytable-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Item Name</th>
                            <th>Stocks</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Note</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody class="data-item">
                            @foreach($cafe as $row)
                            @if(isset($cafeTransaction->id))
                            <?php $check = Helper::CheckOrderedItem($cafeTransaction->id,$row->id); ?>
                                @if(!$check)
                                    <tr data-id="{{$row->id}}">
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->stock}}</td>
                                        <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                        <td><input type="text" size="5" maxlength="2" autocomplete="off" class="input-qty price" data-stock="{{$row->stock}}"></td>
                                        <td><input type="text" class="note" autocomplete="off"></td>
                                        <td><a class="btn btn-primary btn-xs chose" data-id="{{$row->id}}" data-name="{{$row->name}}" data-price="{{$row->price}}">Choose</a></td>
                                    </tr>
                                @endif
                            @else
                            <tr data-id="{{$row->id}}">
                                <td>{{$row->name}}</td>
                                <td>{{$row->stock}}</td>
                                <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                <td><input type="text" size="5" maxlength="2" autocomplete="off" class="input-qty price" data-stock="{{$row->stock}}"></td>
                                <td><input type="text" class="note" autocomplete="off"></td>
                                <td><a class="btn btn-primary btn-xs chose" data-id="{{$row->id}}" data-name="{{$row->name}}" data-price="{{$row->price}}">Choose</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
