<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
<script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    
    
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
jQuery('.datetimepicker1, .datetimepicker2').datetimepicker({
    closeOnDateSelect:true,
    minDate:'0',
    format:'Y-m-d H:00',
    scrollInput:false
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Transaction</h4>
            </div>
            {{Form::open(array('url'=>'admin/checkIn/update-transaction/'.$transaction->id, 'id'=>'request', 'method'=>'POST'))}}
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="checkin">Checkin Date</label>
                                <input type="text" name="checkin" autocomplete="off" value="{{date('Y-m-d H:i',strtotime($transaction->checkin_date))}}" class="form-control datetimepicker1" id="checkin" data-validation="required" data-validation-error-msg="The checkin field is required.">
                                {{$errors->first('checkin','<p class="text-red">:message</p>')}}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="checkout">Checkout Date</label>
                                <input type="text" name="checkout" autocomplete="off" value="{{date('Y-m-d H:i',strtotime($transaction->checkout_date))}}" class="form-control datetimepicker2" id="checkout" data-validation="required" data-validation-error-msg="The checkout field is required.">
                                {{$errors->first('checkout','<p class="text-red">:message</p>')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>