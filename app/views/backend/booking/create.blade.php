@extends('backend.layouts.content')

@section('end-script')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    var ruangan  = [ ];
    var tambahan = 0;
    var qtynya   = 0;


    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    jQuery('.datetimepicker1, .datetimepicker2, .payment').datetimepicker({
        closeOnDateSelect:true,
        minDate:'0',
        format:'Y-m-d H:00',
        scrollInput:false
    });
    (function($){
        $.fn.valInc = function(vadd)
        {
            var val = $(this).val() + '' + vadd;
            $(this).val(val);
        }
    })(jQuery);
    // $(document).on('click','.choose-room-id',function(){
    //     var rel    = $('.room-value').val();
    //     var idRoom = $(this).val();
    //     if($(this).is(':checked')){   
    //         if(rel==''){
    //             $('.room-value').val(','+idRoom);
    //         }else{
    //             $('.room-value').valInc(','+idRoom);
    //         }
    //     }else{
    //         $('.room-value').val(function(index,value){
    //             return value.replace(','+idRoom,'');
    //         });
    //     }
    // });

    // $(document).on('click','.choose-room-id',function(){
    //     var rel    = $('.room-value').val();
    //     var idRoom = $(this).val();
    //     if($(this).is(':checked')){   
    //         if(rel==''){
    //             $('.room-value').val(','+idRoom);
    //         }else{
    //             $('.room-value').valInc(','+idRoom);
    //         }
    //     }else{
    //         $('.room-value').val(function(index,value){
    //             return value.replace(','+idRoom,'');
    //         });
    //     }
    // });

    // $(document).on('click','.choose-adds-id',function(){
    //     var rel    = $('.additional-value').val();
    //     var qty    = $(this).closest('td').prev().find('.choose-adds-qty').val();
    //     var idAdds = $(this).val();
    //     if($(this).is(':checked')){   
    //         $(this).attr('data-qty', qty);
    //         if(rel==''){
    //             $('.additional-value').val(','+idAdds);
    //         }else{
    //             $('.additional-value').valInc(','+idAdds);
    //         }
    //     }else{
    //         $(this).removeAttr('data-qty');
    //         $('.additional-value').val(function(index,value){
    //             return value.replace(','+idAdds,'');
    //         });
    //     }
    // });
    $(document).on('click','.choose-adds-id',function(){
        var rel    = $('.additional-value').val();
        var qty    = $(this).closest('td').prev().find('.choose-adds-qty').val();
        var idAdds = $(this).val();
        if($(this).is(':checked')){   
            $(this).attr('data-qty', qty);
        }else{
            $(this).removeAttr('data-qty');
        }
    });
    
    $(document).on('click','.keep-room',function(){
        var rooms = [ ];
        // var ruangan = [ ];
        $('.choose-room-id:checked').each(function(index, el) {
            rooms.push($(this).val());
            // ruangan.push($(this).data('price'));
        });

        $('.room-value').val(rooms);

    });

    $(document).on('click','.keep-qty',function(){
        var store = [ ];
        var val   = [ ];
        var harga   = [ ];
        $('.choose-adds-id:checked').each(function(index, el) {
            store.push($(this).data('qty'));
            val.push($(this).val());
            // harga.push($(this).data('price')*$(this).data('qty'));
        });
        $('.additional-value-qty').val(store);
        $('.additional-value').val(val);
            console.log(harga);
            console.log(ruangan);
    });

    $(document).on('change','.choose-adds-qty',function(){
        // console.log('sds');
        var qt = $(this).val();
        if($(this).closest('td').next().find('.choose-adds-id').is(':checked')){
            // console.log('masuk');
            $(this).closest('td').next().find('.choose-adds-id').attr('data-qty', qt);
        }
    });
    </script>
@stop
 
@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/checkIn', 'method'=>'POST', 'id'=>'request'))}}
        <div class="box-body">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <select name="title" class="form-control" id="title" data-validation="required" data-validation-error-msg="The title field is required.">
                                <option value=" ">- Select Title -</option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                            </select>
                            {{$errors->first('title','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="name">Customer Name</label>
                            <input type="text" name="name" autocomplete="off" value="{{Input::old('name')}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The customer name field is required.">
                            {{$errors->first('name','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="card_type">Card Type</label>
                            <select name="card_type" class="form-control" id="card_type" data-validation="required" data-validation-error-msg="The card type field is required.">
                                <option value=" ">- Select Card Type -</option>
                                <option value="KTP">KTP</option>
                                <option value="SIM">SIM</option>
                                <option value="PASPOR">PASPOR</option>
                            </select>
                            {{-- <input type="text" name="card_type" autocomplete="off" value="{{Input::old('card_type')}}" class="form-control" id="card_type" data-validation="required" data-validation-error-msg="The card type field is required."> --}}
                            {{$errors->first('card_type','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="id_card">ID Card</label>
                            <input type="text" name="id_card" autocomplete="off" value="{{Input::old('id_card')}}" class="form-control" id="id_card" data-validation="number" data-validation-error-msg="The id card field is required and must be a number.">
                            {{$errors->first('id_card','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" name="address" autocomplete="off" value="{{Input::old('address')}}" class="form-control" id="address" data-validation="required" data-validation-error-msg="The address field is required.">
                            {{$errors->first('address','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" name="phone" autocomplete="off" value="{{Input::old('phone')}}" class="form-control" id="phone" data-validation="number" data-validation-error-msg="The phone field is required and must be a number.">
                            {{$errors->first('phone','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" autocomplete="off" value="{{Input::old('email')}}" class="form-control" id="email" data-validation="email" data-validation-error-msg="The email must be a valid email address." data-validation-optional="true">
                            {{$errors->first('email','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="room">Room</label>
                        <input type="hidden" name="room" class="room-value total-room" value="{{Input::old('room')}}">
                        <a class="form-control btn btn-default choose-room please-waiting">Choose Room</a>
                        {{$errors->first('room','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-3">
                        <label for="additional">Extra Charge</label>
                        <input type="hidden" name="additional" class="additional-value total-adds" value="{{Input::old('additional')}}">
                        <input type="hidden" name="additional_qty" class="additional-value-qty total-qty" value="{{Input::old('additional_qty')}}">
                        <a class="form-control btn btn-default choose-tambahan please-waiting">Choose Extra Charge</a>
                        {{$errors->first('additional','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="checkin">Checkin Date</label>
                            <input type="text" name="checkin" autocomplete="off" value="{{Input::old('checkin')}}" class="form-control datetimepicker1" id="checkin" data-validation="required" data-validation-error-msg="The checkin field is required.">
                            {{$errors->first('checkin','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="checkout">Checkout Date</label>
                            <input type="text" name="checkout" autocomplete="off" value="{{Input::old('checkout')}}" class="form-control datetimepicker2" id="checkout" data-validation="required" data-validation-error-msg="The checkout field is required.">
                            {{$errors->first('checkout','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
{{--                     <div class="col-xs-2">
                        <div class="form-group">
                            <label for="discount">Discount</label>
                            <input type="text" name="discount" autocomplete="off" value="{{(Input::old('discount'))?Input::old('discount'):'0'}}" class="form-control" id="discount" data-validation="number" data-validation-error-msg="The discount field must be number." data-validation-allowing="range[1;100]" data-validation-optional="true">
                            {{$errors->first('discount','<p class="text-red">:message</p>')}}
                        </div>
                    </div> --}}
                </div>
                {{-- <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" class="form-control total" value="" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Payment</label>
                            <a class="form-control btn btn-default add-payment please-waiting">Choose Payment Method</a>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="max_payment">Max Payment Date</label>
                            <input type="text" name="max_payment" autocomplete="off" value="{{Input::old('max_payment')}}" class="form-control payment" id="max_payment" data-validation="required" data-validation-error-msg="The checkout field is required.">
                            {{$errors->first('max_payment','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div> --}}
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/checkIn')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
<div id="open-modal"></div>
@stop