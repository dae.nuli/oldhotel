<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
    var add     = $('.additional-value').val();
    var add_qty = $('.additional-value-qty').val();
    if(add!=''){
        var adds = add.split(',');
        $.each(adds,function(index, row) {
            $('input[type=checkbox][value='+row+']').prop('checked', true);
        });
    }

    if(add_qty!=""){
        var adds_qty = add_qty.split(',');
        var adds     = add.split(',');
        $.each(adds,function(indexs, rows) {
            $('input[type=checkbox][value='+rows+']:checked').closest('td').prev().find('.choose-adds-qty').val(adds_qty[indexs]);
            $('input[type=checkbox][value='+rows+']:checked').attr('data-qty', adds_qty[indexs]);
        });        
    }
});
</script>
<!-- DATA TABLES -->
<link href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- DATA TABES SCRIPT -->
<script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "aoColumnDefs": [ { "aTargets": [0,2,3], "bSortable": false } ],
            "aaSorting": [[ 1, "asc" ]]
        });
    });
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button keep-qty" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose Extra Charge</h4>
            </div>
            <div class="modal-body table-room">
                <div class="box-body table-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th style="width: 10%">Choose</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($tambahan as $row)
                            <tr>
                                <td>{{$row->name}}</td>
                                <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                <td>
                                    <select class="form-control choose-adds-qty">
                                        @for($i=1;$i<6;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td><input type="checkbox" class="choose-adds-id" value="{{$row->id}}" data-price="{{$row->price}}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting keep-qty" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>