<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
$(document).on('click','.choosees',function(){
    // console.log('sds');
    var qt = $(this).val();
    if($(this).is(':checked')){
        $(this).closest('td').prev().find('select').removeAttr('disabled');
    }else{
        $(this).closest('td').prev().find('select').attr('disabled','');
    }
});
</script>
<!-- DATA TABLES -->
<link href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- DATA TABES SCRIPT -->
<script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "aoColumnDefs": [ { "aTargets": [0,2,3], "bSortable": false } ],
            "aaSorting": [[ 1, "asc" ]]
        });
    });
</script>
<script type="text/javascript">
  $('[data-toggle="tooltip"]').tooltip();
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-room">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose Extra Charge</h4>
            </div>
            {{Form::open(array('url'=>'admin/checkIn/add-adds/'.$transaction->id, 'id'=>'request', 'method'=>'POST'))}}
            <div class="modal-body table-room">
                <div id="room-errors"></div>
                <div class="box-bodytable-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th style="width: 10%">Choose</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($tambahan as $key => $row)
                            <tr>
                                <td>{{$row->name}}</td>
                                <td>Rp {{number_format($row->price,0,",",".")}}</td>
                                <td>
                                    <select class="form-control" name="additional_qty[]" disabled="">
                                        @for($i=1;$i<6;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td><input type="checkbox" name="additional[]" value="{{$row->id}}" class="choosees" data-validation="checkbox_group" data-validation-qty="1-2" data-validation-error-msg="You have to choose the item or maximum 2 item." data-validation-error-msg-container="#room-errors"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
