@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });

    var price = $('.price').val();
    $('.price').val(accounting.formatNumber(price));

    $(document).on('input','.price',function(){
        var price = $(this).val();
        $('.price').val(accounting.formatNumber(price));
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/extracharge/edit/'.$extra->id,'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" name="name" value="{{$extra->name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" value="{{$extra->price}}" class="form-control price" id="price" data-validation="required" data-validation-error-msg="The price field is required." autocomplete="off">
                        {{$errors->first('price','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/extracharge')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop