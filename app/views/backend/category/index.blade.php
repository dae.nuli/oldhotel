@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src='{{asset("assets/js/jquery.museum.js")}}'></script>
    <script>
        $(document).ready(function() {
            var options = {
                namespace: "msm",       // for custom CSS class namespacing
                padding: 25,            // in case you would like a specific amount of padding on the lightbox
                disable_url_hash: true, // disable using hashes in case your website already uses URL hashes for states
            };
            $.museum($('.images img'),options);
        });
    </script>
@stop

@section('header-content')
<div class="pull-right">
    <a href="{{URL::to('admin/product_category/create')}}" class="btn btn-primary">Create</a>
</div>
@stop

@section('body-content')
@if(Session::has('product_category'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('product_category')}}.
    </div>
@endif

@if(Session::has('product_category_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('product_category_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Name</th>
                <th>Picture</th>
                <th>Position</th>
                <th>Total Sub Category</th>
                <th>Created by</th>
                <th>Modified at</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($category as $row)
            <?php $total = Helper::total_subcategory($row->id); ?>
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->name}}</td>
                <td>
                    <?php
                    $paths = public_path($row->picture);
                    ?>
                    @if(!empty($row->picture) && is_file($paths))
                        <img width="100" src="{{asset($row->picture)}}" alt="img"/>
                    @else
                        <img width="100" src="{{asset('assets/store/no_image.png')}}" alt="img"/>
                    @endif 
                </td>
                <td>{{$row->position}}</td>
                <td>{{$total}}</td>
                <td>{{$row->author->name}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->updated_at))}}</td>
                <td>
                    @if($row->is_active)
                        <a href="{{URL::to('admin/product_category/noactive/'.$row->id)}}" class="btn btn-warning btn-xs noactived"><i class="fa fa-fw fa-times-circle"></i></a>
                    @else
                        <a href="{{URL::to('admin/product_category/active/'.$row->id)}}" class="btn btn-success btn-xs actived"><i class="fa fa-fw  fa-check-circle"></i></a>
                    @endif
                    <a href="{{URL::to('admin/product_category/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/product_category/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                    <!-- <a href="">Hapus</a> -->
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$category->links()}}
    </div>
</div>
@stop