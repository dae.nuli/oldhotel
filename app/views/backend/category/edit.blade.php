@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        // function openCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog({modal:true, width:875,height:600});
        // }
        // function closeCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog('close');
        // }
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/product_category/edit/'.$category->id, 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <label for="nama">Name</label>
                <input type="text" name="name" value="{{$category->name}}" class="form-control" id="name" placeholder="Name">
                {{$errors->first('name','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="position">Position</label>
                <input type="text" name="position" value="{{$category->position}}" class="form-control" id="position" placeholder="Name">
                {{$errors->first('position','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;" value="{{$category->picture}}"><br/>
                <a href="javascript:openCustomRoxy()">
                    <?php $paths = public_path($category->picture); ?>
                    @if(!empty($category->picture) && is_file($paths))
                        <img src="{{asset($category->picture)}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @else
                        <img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @endif
                </a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div> 
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/product_category')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop