@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <script type="text/javascript">

    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    $('.price').priceFormat({
        prefix:'',
        thousandsSeparator: '',
        centsLimit: 0
    });
      $(function () {
        $('#example1').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": false,
          "bAutoWidth": true
        });
      });

    $(document).on('click','.choose-item',function(){
        var insertedQTY = $(this).parents('td').prev().find('.input-qty').val();
        if(insertedQTY!=""){
            var name   = $(this).data('name');
            var price  = $(this).data('price');
            var id     = $(this).data('id');
            var amount = (price*insertedQTY);
            $('<tr data-id="'+id+'">'+
                '<td><input type="hidden" name="id[]" value="'+id+'">'+name+'</td>'+
                '<td>Rp '+accounting.formatNumber(price, 0, ".", "")+'</td>'+
                '<td><input type="hidden" name="qty[]" value="'+insertedQTY+'">'+insertedQTY+'</td>'+
                '<td><b>Rp '+accounting.formatNumber(amount, 0, ".", "")+'</b></td>'+
                '<td><a class="btn btn-danger btn-xs delete-item" data-id="'+id+'"><i class="fa fa-fw fa-trash-o"></i> Remove</a></td>'+
                '</tr>').appendTo('.insert-item');
            // $('.insert-item').find('tr[data-id="'+id+'"]').fadeIn(1000);
            $(this).parents('tbody').find('tr[data-id="'+id+'"]').hide('slow');
            $(this).attr('disabled', 'disabled');
            // $(this).addClass('chose').removeClass('choose-item');
        }
    });

    $(document).on('keyup','.input-qty',function(){
        var qty   = $(this).val();
        var stock = $(this).data('stock');
        if(qty>0){
            if(qty>stock){
                $(this).parents('td').next().find('a').removeClass('choose-item').addClass('chose');
                alert('stock is less than '+stock);
                $(this).val('');
            }else{
                $(this).parents('td').next().find('a').removeClass('chose').addClass('choose-item');
            }
        }else{
            $(this).parents('td').next().find('a').addClass('chose');
        }
    });

    $(document).on('click','.delete-item', function(){
        var id = $(this).data('id');
        $(this).parents('tbody').find('tr[data-id="'+id+'"]').fadeOut('slow', function() {
            $(this).remove();
        });
        $('.data-item').find('tr[data-id="'+id+'"]').show('slow');
        $('.data-item').find('tr[data-id="'+id+'"] a.choose-item').removeAttr('disabled');
    });
    </script>
@stop

@section('head-script')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}"/ >
    <style type="text/css">
    .radio input[type="radio"]{
        margin-left: 0;
        margin-right: 10px;
    }
    .chose{
        pointer-events: none;
        cursor: default;
        opacity: 0.6;
    }
    </style>
@stop

@section('body-content-child')
<section class="col-lg-6">
<div class="row">
{{Form::open(array('url'=>$action, 'method'=>'POST', 'id'=>'request'))}}
<div class="col-lg-12">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="name" autocomplete="off" value="{{Input::old('name')}}" class="form-control" data-validation="required" data-validation-error-msg="<small>The name field is required.</small>">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>Table Number</label>
                        <input type="text" name="number" autocomplete="off" value="{{Input::old('number')}}" class="form-control" data-validation="required" data-validation-error-msg="<small>The table number field is required.</small>">
                        {{$errors->first('number','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Note</label>
                        <textarea name="note" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label>Order Status</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="0" checked>
                                PROCESS
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="1">
                                DELIVERED
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div>
</div>

<div class="col-lg-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Ordered Item</h3>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Item Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="insert-item">
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            <a href="{{URL::to($url)}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</div>
{{Form::close()}}

</div>
</section>
<section class="col-lg-6">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Cafe Item</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Item Name</th>
                    <th>Stocks</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="data-item">
                    @foreach($item as $row)
                    <tr data-id="{{$row->id}}">
                        <td>{{$row->name}}</td>
                        <td>{{$row->stock}}</td>
                        <td>Rp {{number_format($row->price,0,",",".")}}</td>
                        <td><input type="text" size="5" maxlength="2" autocomplete="off" class="input-qty price" data-stock="{{$row->stock}}"></td>
                        <td><a class="btn btn-primary btn-xs chose" data-id="{{$row->id}}" data-name="{{$row->name}}" data-price="{{$row->price}}">Choose</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</section>
<div id="open-modal"></div>
@stop