<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
<script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
<script type="text/javascript">
jQuery('.datetimepicker2').datetimepicker({
    closeOnDateSelect:true,
    format:'Y-m-d H:i',
    timepicker:true,
    scrollInput:false
});

$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});

$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});

    // var amount = $('.amount').val();
    // $('.amount').val(accounting.formatNumber(amount));
    $('.priceformat').priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
    // $(document).on('input','.amount',function(){
    //     var amount = $(this).val();
    //     $('.amount').val(accounting.formatNumber(amount));
    // });
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Payment</h4>
            </div>
            <form action="{{URL::to($url.'/edit-payment/'.$transaction->id.'/'.$payment->id)}}" id="request" method="POST">
                <div class="modal-body">
                    <div class="box-body">  
                        <div class="form-group">
                            <label for="method">Payment Method</label>
                            <select class="form-control" id="method" name="method" data-validation="required" data-validation-error-msg="The payment method field is required.">
                                    <option value="">- Select Method -</option>
                                @foreach($method as $row)
                                    @if($row->id == $payment->payment_method)
                                        <option value="{{$row->id}}" selected="selected">{{$row->name}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input type="text" name="amount" autocomplete="off" class="form-control priceformat" value="{{$payment->amount}}" id="amount" data-validation="required" data-validation-error-msg="The amount field is required.">
                        </div>
                        <div class="form-group">
                            <label for="date">Payment Date</label>
                            <input type="text" name="date" autocomplete="off" class="form-control datetimepicker2" value="{{date('Y-m-d H:i',strtotime($payment->payment_date))}}" id="date" data-validation="required" data-validation-error-msg="The payment date field is required.">
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea class="form-control" name="note" id="note" rows="3">{{$payment->note}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>