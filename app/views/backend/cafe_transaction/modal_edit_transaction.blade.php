<script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
   form : '#request',
   onSuccess : function() {
      waiting();
   }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Transaction</h4>
            </div>
            {{Form::open(array('url'=>$url.'/update-transaction/'.$transaction->id, 'id'=>'request', 'method'=>'POST'))}}
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Customer Name</label>
                                <input type="text" name="name" autocomplete="off" value="{{$transaction->customer_name}}" class="form-control" data-validation="required" data-validation-error-msg="The name field is required.">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Table Number</label>
                                <input type="text" name="table_number" autocomplete="off" value="{{$transaction->table_number}}" class="form-control" data-validation="required" data-validation-error-msg="The table number field is required.">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>