@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src='{{asset("assets/js/jquery.museum.js")}}'></script>
    <script>
        $(document).ready(function() {
            var options = {
                namespace: "msm",       // for custom CSS class namespacing
                padding: 25,            // in case you would like a specific amount of padding on the lightbox
                disable_url_hash: true, // disable using hashes in case your website already uses URL hashes for states
            };
            $.museum($('.images img'),options);
        });
    </script>
@stop

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to($url.'/create')}}" class="btn btn-primary waiting">Create</a>
</div>
{{Form::open(array('url'=>$url, 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search" autocomplete="off">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('success')}}.
    </div>
@endif

@if(Session::has('alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('alert')}}.
    </div>
@endif

<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Customer Name</th>
                <th>Transaction ID</th>
                <th>Table Number</th>
                <th>Total</th>
                <th>Note</th>
                <th>Order Status</th>
                <th>Payment Status</th>
                <th></th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($index as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->customer_name}}</td>
                <td><a href="{{URL::to($url.'/detail/'.$row->id)}}">{{$row->transaction_number}}</a></td>
                <td><b>{{$row->table_number}}</b></td>
                <td>Rp {{number_format($row->total,0,",",".")}}</td>
                <td>{{Str::words($row->note,3,' readmore')}}</td>
                <td>{{($row->order_status==1)?'<small class="label label-success">DELIVERED</small>':'<small class="label label-warning">PROCESS</small>'}}</td>
                <td>{{($row->payment_status==1)?'<span class="label label-success">PAID</span>':'<span class="label label-danger">UNPAID</span>'}}</td>
                <td>
                    <a href="{{URL::to($url.'/done/'.$row->id)}}" class="btn btn-info btn-xs done-order"><i class="fa fa-fw fa-check"></i> Done</a>
                    <a href="{{URL::to($url.'/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$index->links()}}
    </div>
</div>
@stop