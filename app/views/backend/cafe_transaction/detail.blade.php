@extends('backend.layouts.content')

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary add-cafe-payment" data-transaction="{{$transaction->id}}"><i class="fa fa-fw fa-credit-card"></i> Add Payment</a>
</div>
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to($url.'/print/'.$transaction->id)}}" target="_blank" class="btn btn-primary"><i class="fa fa-fw fa-print"></i> Print</a>
</div>
@stop

@section('end-script')
    @parent
    <script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.price_format.2.0.min.js')}}"></script>
    <script type="text/javascript">
    $('.price').priceFormat({
        prefix:'',
        thousandsSeparator: '',
        centsLimit: 0
    });
      $(function () {
        $('#example1').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": false,
          "bAutoWidth": true
        });
      });

    // $(document).on('click','.choose-item',function(){
    //     var insertedQTY = $(this).parents('td').prev().find('.input-qty').val();
    //     if(insertedQTY!=""){
    //         var name   = $(this).data('name');
    //         var price  = $(this).data('price');
    //         var id     = $(this).data('id');
    //         var amount = (price*insertedQTY);
    //         $('<tr data-id="'+id+'">'+
    //             '<td><input type="hidden" name="id[]" value="'+id+'">'+name+'</td>'+
    //             '<td>Rp '+accounting.formatNumber(price, 0, ".", "")+'</td>'+
    //             '<td><input type="hidden" name="qty[]" value="'+insertedQTY+'">'+insertedQTY+'</td>'+
    //             '<td><b>Rp '+accounting.formatNumber(amount, 0, ".", "")+'</b></td>'+
    //             '<td><a class="btn btn-danger btn-xs delete-item" data-id="'+id+'"><i class="fa fa-fw fa-trash-o"></i> Remove</a></td>'+
    //             '</tr>').appendTo('.insert-item');
    //         // $('.insert-item').find('tr[data-id="'+id+'"]').fadeIn(1000);
    //         $(this).parents('tbody').find('tr[data-id="'+id+'"]').hide('slow');
    //         // $(this).addClass('chose').removeClass('choose-item');
    //     }
    // });

    $(document).on('keyup','.input-qty',function(){
        var qty   = $(this).val();
        var stock = $(this).data('stock');
        if(qty>0){
            if(qty>stock){
                $(this).parents('td').next().find('button').addClass('chose');
                alert('stock is less than '+stock);
                $(this).val(null);
                $(this).parents('td').next().find('.inserted-qty').val(null);
            }else{
                $(this).parents('td').next().find('button').removeClass('chose');
                $(this).parents('td').next().find('.inserted-qty').val(qty);
            }
        }else{
            $(this).parents('td').next().find('.inserted-qty').val(null);
            $(this).parents('td').next().find('button').addClass('chose');
        }
    });
    </script>

@stop

@section('head-script')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/datatables/dataTables.bootstrap.css')}}"/ >
    <style type="text/css">
    .radio input[type="radio"]{
        margin-left: 0;
        margin-right: 10px;
    }
    .chose{
        pointer-events: none;
        cursor: default;
        opacity: 0.6;
    }
    </style>
@stop

@section('body-content-child')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('success')}}.
    </div>
@endif
@if(Session::has('alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('alert')}}.
    </div>
@endif
{{-- {{Form::open(array('url'=>'admin/booking/detail/'.$transaction->id, 'method'=>'POST', 'files'=>true))}} --}}
<div class="col-xs-6">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Transaction Data</h3>
                    <div class="pull-right box-tools">
                        <a style="margin:10px 10px 0 10px;color:white" href="#" class="btn btn-primary btn-xs edit-cafe-transaction please-waiting" data-transaction="{{$transaction->id}}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Customer Name</label>
                                <p>{{$transaction->customer_name}}</p>
                            </div>
                            <div class="form-group">
                                <label>Transaction Date</label>
                                <p>{{date('d F Y',strtotime($transaction->created_at))}}</p>
                            </div>
                            <div class="form-group">
                                <label>Status Payment</label>
                                <p>{{($transaction->payment_status==1)?'<span class="label label-success">PAID</span>':'<span class="label label-danger">UNPAID</span>'}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Transaction Number</label>
                                <p>{{$transaction->transaction_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Table Number</label>
                                <p>{{$transaction->table_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Total</label>
                                <p>Rp <b>{{number_format($transaction->total,0,",",".")}}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">Item</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($item as $row)
                            <tr>
                                <td>{{$row->item_name}}</td>
                                <td>Rp {{number_format($row->item_price,0,",",".")}}</td>
                                <td>{{$row->qty}}</td>
                                <td>Rp {{number_format($row->qty*$row->item_price,0,",",".")}}</td>
                                <td><a href="{{URL::to($url.'/delete-item/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Remove</a></td>
                            </tr>
                            <?php $total[] = ($row->qty*$row->item_price); ?>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Rp <b>{{isset($total)?number_format(array_sum($total),0,",","."):'0'}}</b></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="col-xs-6">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Cafe Item</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Item Name</th>
                    <th>Stocks</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="data-item">
                    @foreach($item_cafe as $row)
                    <?php $check = Helper::CheckOrderedItem($transaction->id,$row->id); ?>
                    @if(!$check)
                    <tr data-id="{{$row->id}}">
                        <td>{{$row->name}}</td>
                        <td>{{$row->stock}}</td>
                        <td>Rp {{number_format($row->price,0,",",".")}}</td>
                        <td><input type="text" size="5" maxlength="2" autocomplete="off" class="input-qty price" data-stock="{{$row->stock}}"></td>
                        <td>
                            {{Form::open(array('url'=>$url.'/insert-new-item/'.$transaction->id, 'method'=>'POST'))}}<input type="hidden" name="id" value="{{$row->id}}"><input type="hidden" name="qty" class="inserted-qty"><button type="submit" class="btn btn-primary btn-xs chose">Choose</button>
                            {{Form::close()}}
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Payment</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Payment Method</th>
                        <th>Amount</th>
                        <th>Note</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($payments)>0)
                        @foreach($payments as $row)
                        <tr>
                            <td>{{date("d F Y H:i",strtotime($row->payment_date))}}</td>
                            <td>{{$row->method->name}}</td>
                            <td>Rp {{number_format($row->amount,0,",",".")}}</td>
                            <td>{{$row->note}}</td>
                            <td><div class="btn-group"><a class="btn btn-sm btn-default edit-cafe-payment" data-transaction="{{$transaction->id}}" data-idpayment="{{$row->id}}"><i class="fa fa-edit"></i></a><a href="{{URL::to($url.'/delete-payment/'.$row->id.'/'.$row->id_cafe_transaction)}}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a></div></td>
                        </tr> 
                        @endforeach
                    @else
                        <tr><td colspan="5"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <ul>
                <li>Payment Total : <b>Rp {{number_format($totalPayment,0,",",".")}}</b></li>
                <li>Cashback : 
                @if($totalPayment)
                    <?php $tot = $totalPayment-$transaction->total; ?>
                    @if($tot<0)
                        <b>Rp 0</b>
                    @else
                        <b>Rp {{number_format($tot,0,",",".")}}</b>
                    @endif
                @else
                    <b>Rp 0</b>
                @endif
                </li>
            </ul>
        </div>
    </div>
</div>
 
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to($url)}}" class="btn btn-default">{{trans('button.bc')}}</a>
    </div>
</div>
{{-- {{Form::close()}} --}}
<div id="open-modal"></div>
@stop