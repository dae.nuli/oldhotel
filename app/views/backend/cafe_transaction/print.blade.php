<?php $about = Helper::about(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{$about->name}} | Invoice-{{$transaction->id}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('assets/css/new/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('assets/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body onload="window.print();">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
              <i class="fa fa-globe"></i> {{$about->name}}.
              <small class="pull-right">Date: {{date('d F Y')}}</small>
            </h2>
          </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
            From
            <address>
              <strong>{{$about->name}}</strong><br>
              Address: {{$about->address}}<br/>
              Phone: {{$about->phone}}<br/>
              Email: {{($about->email)?$about->email:'-'}}<br/>
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            To
            <address>
              <strong>{{$transaction->customer_name}}.</strong><br>
              Table Number: {{$transaction->table_number}}<br>
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            <b>Invoice #{{$transaction->id}}</b><br/>
            <b>Transaction Number:</b> {{$transaction->transaction_number}}<br/>
            <b>Order Date:</b> {{date('d F Y, H:i',strtotime($transaction->created_at))}}<br/>
            <b>Front Officer:</b> {{$staff->name}}<br>

          </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Qty</th>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Note</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($detail as $row)
                <tr>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->item_name}}</td>
                    <td>Rp {{number_format($row->item_price,0,",",".")}}</td>
                    <td>{{($row->note)?$row->note:'-'}}</td>
                    <td>Rp {{number_format($row->item_price*$row->qty,0,",",".")}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-6">
            {{-- <p class="lead">Payment Methods:</p>
            <img src="../../dist/img/credit/visa.png" alt="Visa"/>
            <img src="../../dist/img/credit/mastercard.png" alt="Mastercard"/>
            <img src="../../dist/img/credit/american-express.png" alt="American Express"/>
            <img src="../../dist/img/credit/paypal2.png" alt="Paypal"/>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
            </p> --}}
          </div><!-- /.col -->
          <div class="col-xs-6">
            <p class="lead"></p>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">Total:</th>
                  <th>Rp {{number_format($transaction->total,0,",",".")}}</th>
                </tr>
                <tr>
                  <th>Payment:</th>
                  <td>Rp {{number_format($totalPayment,0,",",".")}}</td>
                </tr>
                <tr>
                  <th>Give back:</th>
                  <td>Rp {{($totalPayment)?number_format($totalPayment-$transaction->total,0,",","."):0}}</td>
                </tr>
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
  </body>
</html>