@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        // tinymce.init({
        //     selector: "textarea",
        //     plugins: [
        //         ["advlist autolink link image lists charmap hr anchor pagebreak spellchecker"],
        //         ["searchreplace wordcount insertdatetime media nonbreaking"],
        //         ["save table contextmenu directionality emoticons template paste"]
        //     ], 
        //     schema: "html5", 
        //     toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
        //     style_formats:[
        //         {title:'Left Margin',selector:'img',styles:{'margin-left':'5px'}},
        //         {title:'Right Margin',selector:'img',styles:{'margin-right':'5px'}},
        //         {title:'Top Margin',selector:'img',styles:{'margin-right':'5px'}},
        //         {title:'Bottom Margin',selector:'img',styles:{'margin-right':'5px'}}
        //     ],
        //     relative_urls: false,
        //     // file_browser_callback: RoxyFileBrowser
        //  });

        // function RoxyFileBrowser(field_name, url, type, win) {
        //   var roxyFileman = '/fileman/index.html?integration=tinymce4';
        //   if (roxyFileman.indexOf("?") < 0) {     
        //     roxyFileman += "?type=" + type;   
        //   }
        //   else {
        //     roxyFileman += "&type=" + type;
        //   }
        //   roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
        //   if(tinyMCE.activeEditor.settings.language){
        //     roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
        //   }
        //   tinyMCE.activeEditor.windowManager.open({
        //      file: roxyFileman,
        //      title: 'Roxy Fileman',
        //      width: 850, 
        //      height: 650,
        //      resizable: "yes",
        //      plugins: "media",
        //      inline: "yes",
        //      close_previous: "no"  
        //   }, { window: win, input: field_name });
        //   return false; 
        // }

    });
    </script>

@stop

@section('body-content')
@if(Session::has('about'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('about')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/about/update', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{$about->name}}" class="form-control" id="name" placeholder="Name">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{$about->email}}" class="form-control" id="email" placeholder="Email Address">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="facebook">Facebook Account</label>
                        <input type="text" name="facebook" value="{{$about->facebook}}" class="form-control" id="facebook" placeholder="arinigrafika">
                        {{$errors->first('facebook','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="twitter">Twitter Account</label>
                        <input type="text" name="twitter" value="{{$about->twitter}}" class="form-control" id="twitter" placeholder="arinigrafika">
                        {{$errors->first('twitter','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="instagram">Instagram Account</label>
                        <input type="text" name="instagram" value="{{$about->instagram}}" class="form-control" id="instagram" placeholder="arinigrafika">
                        {{$errors->first('instagram','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="bbm">Bbm</label>
                        <input type="text" name="bbm" value="{{$about->bbm}}" class="form-control" id="bbm" placeholder="Pin Bbm">
                        {{$errors->first('bbm','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="phone">Phone Number</label>
                        <input type="text" name="phone_number" value="{{$about->phone}}" class="form-control" id="phone" placeholder="Phone Number">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="address">Address</label>
                        <input type="text" name="address" value="{{$about->address}}" class="form-control" id="address" placeholder="Address">
                        {{$errors->first('address','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <!-- <div class="form-group">
                <label for="body">Web Keywords</label>
                <textarea id="body" name="web_keywords" class="form-control textarea" style="height:100px" rows="3" placeholder="Web Keywords">{{$about->web_keywords}}</textarea>
                {{$errors->first('web_keywords','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="home">Home Description</label>
                        <input type="text" name="home_description" value="{{$about->home_description}}" class="form-control" id="home" placeholder="Home Description">
                        {{$errors->first('home_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="blog_description">Blog Description</label>
                        <input type="text" name="blog_description" value="{{$about->blog_description}}" class="form-control" id="blog_description" placeholder="Blog Description">
                        {{$errors->first('blog_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="how_to_buy_description">How To Buy Description</label>
                        <input type="text" name="how_to_buy_description" value="{{$about->how_to_buy_description}}" class="form-control" id="how_to_buy_description" placeholder="How To Buy Description">
                        {{$errors->first('how_to_buy_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="order_description">Order Description</label>
                        <input type="text" name="order_description" value="{{$about->order_description}}" class="form-control" id="order_description" placeholder="Order Description">
                        {{$errors->first('order_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="contact_description">Contact Description</label>
                        <input type="text" name="contact_description" value="{{$about->contact_description}}" class="form-control" id="contact_description" placeholder="Contact Description">
                        {{$errors->first('contact_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="product_description">Product Description</label>
                        <input type="text" name="product_description" value="{{$about->product_description}}" class="form-control" id="product_description" placeholder="Product Description">
                        {{$errors->first('product_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div> 

            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;" value="{{$about->picture}}">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <a href="javascript:openCustomRoxy()">
                    <?php $paths = public_path($about->picture); ?>
                    @if(!empty($about->picture) && is_file($paths))
                        <img src="{{asset($about->picture)}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @else
                        <img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @endif
                </a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>  
        </div>  -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop