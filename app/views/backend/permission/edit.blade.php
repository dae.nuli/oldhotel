@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/permission/update/'.$permission->id, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">
            <div class="form-group">
                <label for="parent">Parent</label>
                <select class="form-control parent_selected" id="parent" name="parent">
                    <option value="">-Select Parent-</option>
                    @foreach($parent as $row)
                        @if($row->id == $permission->id_parent)
                            <option value="{{$permission->id_parent}}" selected>{{$row->name}}</option>
                        @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group submenu">
                <label for="name"><a class="btn btn-primary btn-xs">Sub Menu <i class="fa fa-fw fa-sitemap"></i></a></label>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" value="{{$permission->name}}" class="form-control" id="name">
                {{$errors->first('name','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="path">Path</label>
                <input type="text" name="path" value="{{$permission->url}}" class="form-control" id="path">
                {{$errors->first('path','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="icon">Icon Class</label>
                <input type="text" name="icon" value="{{$permission->icon}}" class="form-control icon modal-icon" id="icon">
                {{$errors->first('icon','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="body">Description</label>
                <textarea id="body" name="description" class="form-control" style="height:100px">{{$permission->description}}</textarea>
                {{$errors->first('description','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <a href="{{URL::to('admin/permission')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
<div id="open-modal"></div>
@stop