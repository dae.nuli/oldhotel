@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    </script>
@stop

@section('body-content')
@if(Session::has('users'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('users')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'/admin/profile', 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">  
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="nama">Name</label>
                            <input type="text" name="name" value="{{$profile->name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                            {{$errors->first('name','<p class="text-red">:message</p>')}}
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" disabled="" value="{{$profile->email}}" class="form-control" id="email">
                            {{$errors->first('email','<p class="text-red">:message</p>')}}
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" name="phone_number" value="{{$profile->phone}}" class="form-control" id="phone_number" data-validation="number" data-validation-error-msg="The phone number field is required and must be a number." autocomplete="off">
                            {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">                
                        <div class="form-group">
                            <label for="bbm">BBM</label>
                            <input type="text" name="bbm" value="{{$profile->bbm}}" class="form-control" id="bbm" autocomplete="off">
                            {{$errors->first('bbm','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="facebook">Facebook Account</label>
                            <input type="text" name="facebook" value="{{$profile->facebook}}" class="form-control" id="facebook" autocomplete="off">
                            {{$errors->first('facebook','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="twitter">Twitter Account</label>
                            <input type="text" name="twitter" value="{{$profile->twitter}}" class="form-control" id="twitter" autocomplete="off">
                            {{$errors->first('twitter','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4"> 
                        <div class="form-group">
                            <label for="instagram">Instagram Account</label>
                            <input type="text" name="instagram" value="{{$profile->instagram}}" class="form-control" id="instagram" autocomplete="off">
                            {{$errors->first('instagram','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" data-validation="length" data-validation-error-msg="The password must be at least 8 characters." data-validation-length="min8" autocomplete="off" data-validation-optional="true">
                            <small class="help-block">Complete this form if you wanto change the password.</small>
                            {{$errors->first('password','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="retype_password">Retype Password</label>
                            <input type="password" name="retype_password" class="form-control" id="retype_password" data-validation="length" data-validation-error-msg="The password must be at least 8 characters." autocomplete="off" data-validation-length="min8" data-validation-optional="true">
                            {{$errors->first('retype_password','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12"> 
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea id="address" name="address" class="form-control" style="height:100px" rows="3" placeholder="Address ...">{{$profile->address}}</textarea>
                            {{$errors->first('address','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            {{-- <a href="{{URL::to('admin/users')}}" class="btn btn-default">{{trans('button.bc')}}</a> --}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop