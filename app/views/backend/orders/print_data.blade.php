<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <link rel="stylesheet" href="{{asset('assets/css/pdf_print.css')}}" media="all" />
  </head>
  <body>
<?php $arini = FrontEnd::about(); ?>
    <header class="clearfix">
      <div id="logo">
        <img src="{{asset('front/arini.png')}}">
      </div>
      <div id="company">
        <h2 class="name">{{$arini->name}}</h2>
        <div>{{$arini->address}}</div>
        <div>{{$arini->phone}}</div>
        <div><a href="mailto:{{$arini->email}}">{{$arini->email}}</a></div>
      </div>
    </header><br><br><br><br><br><br>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name">{{$client->name}}</h2>
          <div class="address">{{$client->address}}</div>
          <div class="phone">{{$client->phone}}</div>
          <div class="email"><a href="mailto:{{$client->email}}">{{$client->email}}</a></div>
        </div>
        <div id="invoice">
          <h1>INVOICE : {{$orders->code_order}}</h1>
          <div class="date">Date : {{date('d F Y')}}</div>
        </div>
      </div><br><br><br><br><br><br><br>
      <label><h1 class="legend"><center>DATA MEMPELAI</center></h1></label><br/>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th colspan="2"><label><h1>PIHAK WANITA</h1></label></th>
            <th colspan="2" class="left-border"><label><h1>PIHAK PRIA</h1></label></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Nama</td><td>: {{$mempelai->woman_name}}</td>
            <td class="left-border">Nama</td><td>: {{$mempelai->man_name}}</td>
          </tr>
          <tr>
            <td>Nama Ayah</td><td>: {{$mempelai->woman_father}}</td>
            <td class="left-border">Nama Ayah</td><td>: {{$mempelai->man_father}}</td>
          </tr>
          <tr>
            <td>Nama Ibu</td><td>: {{$mempelai->woman_mother}}</td>
            <td class="left-border">Nama Ibu</td><td>: {{$mempelai->man_mother}}</td>
          </tr>
          <tr>
            <td>Alamat</td><td>: {{$mempelai->woman_address}}</td>
            <td class="left-border">Alamat</td><td>: {{$mempelai->man_address}}</td>
          </tr>
        </tbody>
      </table>

      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th colspan="2"><label><h1>AKAD NIKAH</h1></label></th>
            <th colspan="2" class="left-border"><label><h1>RESEPSI</h1></label></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Hari, tanggal</td><td>: {{date('d F Y',strtotime($mempelai->akad_date))}}</td>
            <td class="left-border">Hari, tanggal</td><td>: {{date('d F Y',strtotime($mempelai->resepsi_date))}}</td>
            
          </tr>
          <tr>
            <td>Jam</td><td>: {{date('H:i',strtotime($mempelai->akad_time))}}</td>
            <td class="left-border">Jam</td><td>: {{date('H:i',strtotime($mempelai->resepsi_time))}}</td>
          </tr>
          <tr>
            <td>Lokasi</td><td>: {{$mempelai->akad_location}}</td>
            <td class="left-border">Lokasi</td><td>: {{$mempelai->resepsi_location}}</td>
          </tr>
        </tbody>
      </table>

      <label><h1>NAMA PANGGILAN</h1></label>
      <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td>Nama Panggilan Wanita</td><td>: {{$mempelai->nickname_woman}}</td>
          </tr>
          <tr>
            <td>Nama Panggilan Pria</td><td>: {{$mempelai->nickname_man}}</td>
          </tr>
          <tr>
            <td>Agama</td><td>: {{$mempelai->religion->name}}</td>
          </tr>
          <tr>
            <td>Turut Mengundang</td><td>: {{$mempelai->important_invitation}}</td>
          </tr>
        </tbody>
      </table>
     

    </main>
  </body>
</html>