@extends('backend.layouts.content')

@section('end-script')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
<script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
<script type="text/javascript">
jQuery('.datetimepicker2').datetimepicker({
    closeOnDateSelect:true,
    format:'Y-m-d',
    timepicker:false
});
</script>
@stop
 

@section('body-content-child')
{{Form::open(array('url'=>'admin/orders/detail/'.$orders->id, 'method'=>'POST', 'files'=>true))}}
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Client</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <p>{{$client->name}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <p>{{$client->email}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone</label>
                        <p>{{$client->phone}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Address</label>
                        <textarea class="form-control" rows="3" disabled="">{{$client->address}}</textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    {{-- <h3 class="box-title">Client</h3> --}}
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Order ID</label>
                        <p>{{$orders->code_order}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Order Date</label>
                        <p>{{date('d F Y, H:i:s',strtotime($orders->created_at))}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Total</label>
                        <p>Rp {{number_format($orders->total,0,",",".")}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header">
                    <h5 class="box-title" style="padding-bottom:0">Creating</h5>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Start Date</label>
                        <input type="text" class="form-control datetimepicker2" name="start_date" placeholder="{{date('Y-m-d')}}">
                        {{$errors->first('start_date','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Finish Date</label>
                        <input type="text" class="form-control datetimepicker2" name="finish_date" placeholder="{{date('Y-m-d')}}">
                        {{$errors->first('finish_date','<p class="text-red">:message</p>')}}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Note</label>
                        <textarea class="form-control" rows="3" name="note" placeholder="Note of operator"></textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Items</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Note</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail as $row)
                    <?php $price = Helper::ProductPrice($row->code_product); ?>
                    <?php $subtotal = ($row->qty*$price); ?>
                    <tr>
                        <td>{{$row->qty}}</td>
                        <td>{{$row->code_product}}</td>
                        <td>Rp {{number_format($price,0,",",".")}}</td>
                        <td>{{$row->note}}</td>
                        <td>Rp {{number_format($subtotal,0,",",".")}}</td>
                    </tr> 
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<div class="col-xs-12">
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Data Mempelai</h3>
        </div>
    </div><!-- /.box -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Pihak Wanita</h4>
                    <div class="form-group">
                        <label for="a">Nama Mempelai Wanita</label>
                        <p>Sukiyem</p>
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ayah</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ibu</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label for="b">Alamat</label>
                        <p>Koroku</p>    
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Pihak Pria</h4>
                    <div class="form-group">
                        <label for="a">Nama Mempelai Pria</label>
                        <p>Sukiyem</p>
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ayah</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ibu</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label for="b">Alamat</label>
                        <p>Koroku</p>    
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Akad Nikah</h4>
                    <div class="form-group">
                        <label>Hari, tanggal</label>
                        <p>Sukiyem</p>
                    </div>
                    <div class="form-group">
                        <label>Jam</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label>Lokasi</label>
                        <p>Koroku</p>    
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Resepsi</h4>
                    <div class="form-group">
                        <label>Hari, tanggal</label>
                        <p>Sukiyem</p>
                    </div>
                    <div class="form-group">
                        <label>Jam</label>
                        <p>Koroku</p>    
                    </div>
                    <div class="form-group">
                        <label>Lokasi</label>
                        <p>Koroku</p>    
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="col-xs-6">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-body">
            <div class="form-group">
                <label>Nama Panggilan Wanita</label>
                <p>Sukiyem</p>
            </div>
            <div class="form-group">
                <label>Nama Panggilan Wanita</label>
                <p>Koroku</p>    
            </div>
            <div class="form-group">
                <label>Lokasi</label>
                <p>Koroku</p>    
            </div>
            <div class="form-group">
                <label>Agama</label>
                <p>Koroku</p>    
            </div>
            <div class="form-group">
                <label>Turut Mengundang</label>
                <p>Koroku</p>    
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to('admin/orders')}}" class="btn btn-default">{{trans('button.bc')}}</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{{Form::close()}}
@stop