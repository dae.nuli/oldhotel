<?php $about = Helper::about(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{$about->name}} | Invoice-{{$transaction->id}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('assets/css/new/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('assets/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body onload="window.print();">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
              <i class="fa fa-globe"></i> {{$about->name}}.
              <small class="pull-right">Date: {{date('d F Y')}}</small>
            </h2>
          </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">

            <address>
              <strong>{{$about->name}}</strong><br>
              Address: {{$about->address}}<br/>
              Phone: {{$about->phone}}<br/>
              Email: {{($about->email)?$about->email:'-'}}<br/>
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            Customer
            <address>
              <strong>{{$customers->full_name}}.</strong><br>
              Address: {{$customers->address}}<br>
              Phone: {{$customers->phone}}<br/>
              Email: {{($customers->email)?$customers->email:'-'}}
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            <b>Invoice #{{$transaction->id}}</b><br/>
            <b>Transaction ID:</b> {{$transaction->code}}<br/>
            <b>Check in date:</b> {{date('d F Y, H:i',strtotime($transaction->checkin_date))}}<br/>
            <b>Check out date:</b> {{date('d F Y, H:i',strtotime($transaction->checkout_date))}}<br/>
            <b>Front Officer:</b> {{$staff->name}}<br>

          </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Qty</th>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Note</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <?php $day = Helper::countDay($transaction->checkin_date,$transaction->checkout_date); ?>
                @foreach($detail as $row)
                <tr>
                    <td>
                        @if($row->type==1)
                        {{$day}} days
                        @else
                        {{$row->qty}}
                        @endif
                    </td>
                    <td>
                        @if($row->type==1)
                        {{$row->item}} (room)
                        @else
                        {{$row->item}}
                        @endif
                    </td>
                    <td>{{number_format($row->item_price,0,",",".")}}</td>
                    <td>{{($row->note)?$row->note:'-'}}</td>
                    <td>
                        @if($row->type==1)
                        {{number_format($row->item_price*$day,0,",",".")}}
                        @else
                        {{number_format($row->item_price*$row->qty,0,",",".")}}
                        @endif
                    </td>
                </tr>
                @endforeach
                {{--
                @foreach($detailCafe as $cafe)
                <tr>
                  <td>{{$cafe->qty}}</td>
                  <td>{{$cafe->item_name}}</td>
                  <td>{{number_format($cafe->item_price,0,",",".")}}</td>
                  <td>{{$cafe->note}}</td>
                  <td>{{number_format($cafe->item_price*$cafe->qty,0,",",".")}}</td>
                </tr>
                @endforeach
                --}}
              </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-6">
            {{-- <p class="lead">Payment Methods:</p>
            <img src="../../dist/img/credit/visa.png" alt="Visa"/>
            <img src="../../dist/img/credit/mastercard.png" alt="Mastercard"/>
            <img src="../../dist/img/credit/american-express.png" alt="American Express"/>
            <img src="../../dist/img/credit/paypal2.png" alt="Paypal"/>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
            </p> --}}
          </div><!-- /.col -->
          <div class="col-xs-6">
            <p class="lead"></p>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">Total:</th>
                  <th>Rp {{number_format($transaction->total,0,",",".")}}</th>
                </tr>
                <tr>
                  <th>Payment:</th>
                  <td>Rp {{number_format($totalPayment,0,",",".")}}</td>
                </tr>
                <tr>
                  <th>Give back:</th>
                  <td>Rp {{($totalPayment)?number_format($totalPayment-$transaction->total,0,",","."):0}}</td>
                </tr>
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
  </body>
</html>