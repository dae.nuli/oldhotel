@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src='{{asset("assets/js/jquery.museum.js")}}'></script>
    <script>
        $(document).ready(function() {
            var options = {
                namespace: "msm",       // for custom CSS class namespacing
                padding: 25,            // in case you would like a specific amount of padding on the lightbox
                disable_url_hash: true, // disable using hashes in case your website already uses URL hashes for states
            };
            $.museum($('.images img'),options);
        });
    </script>
@stop

@section('header-content')
{{-- <div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/checkIn/create')}}" class="btn btn-primary waiting">Create</a>
</div> --}}
{{Form::open(array('url'=>'admin/checkOut', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search" autocomplete="off">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('booking'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('booking')}}.
    </div>
@endif

@if(Session::has('products_cannot_change_subcategory'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('products_cannot_change_subcategory')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Transaction ID</th>
                <th>Name</th>
                {{-- <th>Phone</th> --}}
                <th>Total</th>
                <th>Checkin</th>
                <th>Checkout</th>
                <th>Guest Status</th>
                {{-- <th>Book Via</th> --}}
                <th>Payment Status</th>
                {{-- <th></th> --}}
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($booking as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td><a href="{{URL::to('/admin/checkOut/detail/'.$row->id)}}">{{$row->code}}</a></td>
                <td>{{$row->customers->title}}. {{$row->customers->full_name}}</td>
                {{-- <td>{{$row->customers->phone}}</td> --}}
                <td>Rp {{number_format($row->total,0,",",".")}}</td>
                <td>{{date('d F Y, H:i',strtotime($row->checkin_date))}}</td>
                <td>{{date('d F Y, H:i',strtotime($row->checkout_date))}}</td>
                <td>{{($row->guest_status==1)?'<small class="label label-success">Checked In</small>':'<small class="label label-warning">Checked Out</small>'}}</td>
                {{-- <td>{{($row->book_via==1)?'Website':'Front Office'}}</td> --}}
                <td>{{($row->payment_status==1)?'<span class="label label-success">PAID</span>':'<span class="label label-danger">UNPAID</span>'}}</td>
                <td>
                    {{-- <a href="{{URL::to('admin/checkIn/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a> --}}
                    {{-- <a href="{{URL::to('admin/checkIn/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a> --}}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$booking->links()}}
    </div>
</div>
@stop