@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/AdminLTE/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
       form : '#request',
       onSuccess : function() {
          waiting();
       }
    });
    </script>
@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/users/update/'.$users->id, 'id'=>'request', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" name="name" value="{{$users->name}}" class="form-control" id="name" data-validation="required" data-validation-error-msg="The name field is required." autocomplete="off">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{$users->email}}" class="form-control" id="email" data-validation="required email" data-validation-error-msg="The email field is required and must be a valid email address." autocomplete="off">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="phone_number">Phone Number</label>
                        <input type="text" name="phone_number" value="{{$users->phone}}" class="form-control" id="phone_number" data-validation="required" data-validation-error-msg="The phone number field is required." autocomplete="off">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3">                
                    <div class="form-group">
                        <label for="bbm">BBM</label>
                        <input type="text" name="bbm" value="{{$users->bbm}}" class="form-control" id="bbm" autocomplete="off">
                        {{$errors->first('bbm','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="facebook">Facebook Account</label>
                        <input type="text" name="facebook" value="{{$users->facebook}}" class="form-control" id="facebook" autocomplete="off">
                        {{$errors->first('facebook','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="twitter">Twitter Account</label>
                        <input type="text" name="twitter" value="{{$users->twitter}}" class="form-control" id="twitter" autocomplete="off">
                        {{$errors->first('twitter','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="instagram">Instagram Account</label>
                        <input type="text" name="instagram" value="{{$users->instagram}}" class="form-control" id="instagram" autocomplete="off">
                        {{$errors->first('instagram','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="group">Group</label>
                        <select class="form-control" id="group" name="group" data-validation="required" data-validation-error-msg="The group field is required.">
                            <option value="">-Select Group-</option>
                            @foreach($group as $row)
                                @if($row->id == $users->level)
                                    <option value="{{$row->id}}" selected>{{$row->group_name}}</option>
                                @else
                                    <option value="{{$row->id}}">{{$row->group_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        {{$errors->first('group','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" autocomplete="off" data-validation="length" data-validation-error-msg="The password must be at least 8 characters." data-validation-length="min8" data-validation-optional="true">
                        {{$errors->first('password','<p class="text-red">:message</p>')}}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="retype_password">Retype Password</label>
                        <input type="password" name="retype_password" class="form-control" id="retype_password" autocomplete="off" data-validation="length" data-validation-error-msg="The password must be at least 8 characters." data-validation-length="min8" data-validation-optional="true">
                        {{$errors->first('retype_password','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <label for="status">Status</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1" {{($users->is_active==1)?'selected':''}}>Active</option>
                        <option value="0" {{($users->is_active==0)?'selected':''}}>Not Active</option>
                    </select>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input id="address" name="address" class="form-control" value="{{$users->address}}" autocomplete="off">
                        {{$errors->first('address','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/users')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop