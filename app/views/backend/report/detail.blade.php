@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/report/print/'.$transaction->id)}}" target="_blank" class="btn btn-primary"><i class="fa fa-fw fa-print"></i> Print</a>
</div>
@stop

@section('end-script')
    @parent

    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>

    <link href="{{asset('assets/css/bootstrap-editable.css')}}" rel="stylesheet">
    <script src="{{asset('assets/js/bootstrap-editable.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script type="text/javascript">
    jQuery('.datetimepicker2').datetimepicker({
        closeOnDateSelect:true,
        format:'Y-m-d',
        timepicker:false,
        scrollInput:false
    });
    $(document).ready(function() {
         
    });
    </script>

@stop
 

@section('body-content-child')
@if(Session::has('booking'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('booking')}}.
    </div>
@endif
@if(Session::has('warning'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('warning')}}.
    </div>
@endif
{{-- {{Form::open(array('url'=>'admin/booking/detail/'.$transaction->id, 'method'=>'POST', 'files'=>true))}} --}}
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Customer</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <p>{{$customers->title}}. {{$customers->full_name}}</p>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <p>{{($customers->email)?$customers->email:'-'}}</p>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <p>{{$customers->phone}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ID Type</label>
                                <p>{{$customers->card_type}}</p>
                            </div>
                            <div class="form-group">
                                <label>ID Number</label>
                                <p>{{$customers->id_card_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <p>{{$customers->address}}</p>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Address</label>
                                <textarea class="form-control" rows="3" disabled="">{{$customers->address}}</textarea>
                            </div>
                        </div>
                    </div> --}}
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Transaction</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Transaction ID</label>
                                <p>{{$transaction->code}}</p>
                                {{-- <p>{{$orders->code_order}} {{Helper::CheckPayment($orders->id)}}</p> --}}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Transaction Date</label>
                                <p>{{date('d F Y',strtotime($transaction->created_at))}}</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Status Payment</label>
                                <p>{{($transaction->payment_status==1)?'<span class="label label-success">Paid</span>':'<span class="label label-danger">Unpaid</span>'}}</p>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Check in date</label>
                                <p>{{date('d F Y H:i',strtotime($transaction->checkin_date))}}</p>
                                {{-- <p>{{$orders->code_order}} {{Helper::CheckPayment($orders->id)}}</p> --}}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Check out date</label>
                                <p>{{date('d F Y H:i',strtotime($transaction->checkout_date))}} <b>({{Helper::countDay($transaction->checkin_date,$transaction->checkout_date)}} days)</b></p>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total</label>
                                <p>Rp <span class="total">{{number_format($transaction->total,0,",",".")}}</span></p>
                            </div>
                            @if(isset($cafeTransaction))
                            <div class="form-group">
                                <label for="exampleInputPassword1">Table Number</label>
                                <p>{{$cafeTransaction->table_number}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

       {{--  <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Note</label>
                        <textarea class="form-control" rows="3" name="note" placeholder="Note of operator"></textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div> --}}
    </div>
</div>
<div class="col-xs-6">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title pull-left">Rooms</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Room Number</th>
                        <th>Class</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($detailRoom)>0)
                        @foreach($detailRoom as $row)
                        <tr>
                            <td>{{$row->item}}</td>
                            <td>{{$row->class}}</td>
                            <td>Rp {{number_format($row->item_price,0,",",".")}}</span></td>
                        </tr>   
                        @endforeach
                    @else
                        <tr><td colspan="6"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<div class="col-xs-6">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title pull-left">Extra Charge</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($detailAdds)>0)
                        @foreach($detailAdds as $row)
                        <tr>
                            <td>{{$row->item}}</td>
                            <td>Rp {{number_format($row->item_price,0,",",".")}}</span></td>
                            <td>{{$row->qty}}</td>
                        </tr>   
                        @endforeach
                    @else
                        <tr><td colspan="6"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
@if(isset($detailCafe))
<div class="col-xs-12">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Cafe</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detailCafe as $rowCafe)
                        <tr>
                            <td>{{$rowCafe->item_name}}</td>
                            <td>Rp {{number_format($rowCafe->item_price,0,",",".")}}</span></td>
                            <td>{{$rowCafe->qty}}</td>
                            <td>{{($rowCafe->note)?$rowCafe->note:'-'}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Payment</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Payment Method</th>
                        <th>Amount</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($payments)>0)
                        @foreach($payments as $row)
                        <tr>
                            <td>{{date("d F Y H:i",strtotime($row->payment_date))}}</td>
                            <td>{{$row->method->name}}</td>
                            <td>Rp {{number_format($row->amount,0,",",".")}}</td>
                            <td>{{$row->note}}</td>
                        </tr> 
                        @endforeach
                    @else
                        <tr><td colspan="5"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <ul>
               <li>Payment Total : <b>Rp {{number_format($totalPayment,0,",",".")}}</b></li>
               <li>Give back : <b>Rp {{($totalPayment)?number_format($totalPayment-$transaction->total,0,",","."):0}}</b></li>
            </ul>
        </div>
    </div>
</div>
 
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to('admin/report')}}" class="btn btn-default">{{trans('button.bc')}}</a>
        {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
    </div>
</div>
{{-- {{Form::close()}} --}}
<div id="open-modal"></div>
@stop