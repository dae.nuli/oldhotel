@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to($url.'/print/'.$transaction->id)}}" target="_blank" class="btn btn-primary"><i class="fa fa-fw fa-print"></i> Print</a>
</div>
@stop

@section('end-script')
    @parent

    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>

    <link href="{{asset('assets/css/bootstrap-editable.css')}}" rel="stylesheet">
    <script src="{{asset('assets/js/bootstrap-editable.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script type="text/javascript">
    jQuery('.datetimepicker2').datetimepicker({
        closeOnDateSelect:true,
        format:'Y-m-d',
        timepicker:false,
        scrollInput:false
    });
    $(document).ready(function() {
         
    });
    </script>

@stop
 

@section('body-content-child')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('success')}}.
    </div>
@endif
@if(Session::has('alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('alert')}}.
    </div>
@endif
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Transaction Data</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Customer Name</label>
                                <p>{{$transaction->customer_name}}</p>
                            </div>
                            <div class="form-group">
                                <label>Transaction Date</label>
                                <p>{{date('d F Y',strtotime($transaction->created_at))}}</p>
                            </div>
                            <div class="form-group">
                                <label>Status Payment</label>
                                <p>{{($transaction->payment_status==1)?'<span class="label label-success">PAID</span>':'<span class="label label-danger">UNPAID</span>'}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Transaction Number</label>
                                <p>{{$transaction->transaction_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Table Number</label>
                                <p>{{$transaction->table_number}}</p>
                            </div>
                            <div class="form-group">
                                <label>Total</label>
                                <p>Rp <b>{{number_format($transaction->total,0,",",".")}}</b></p>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Item</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detailItem)>0)
                                @foreach($detailItem as $row)
                                <tr>
                                    <td>{{$row->item_name}}</td>
                                    <td>Rp {{number_format($row->item_price,0,",",".")}}</td>
                                    <td>{{$row->qty}}</td>
                                    <td>Rp {{number_format($row->qty*$row->item_price,0,",",".")}}</td>
                                </tr>
                                <?php $total[] = ($row->qty*$row->item_price); ?>
                                @endforeach
                            @else
                                <tr><td colspan="5"><center>Data is empty</center></td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-xs-12">
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Payment</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Payment Method</th>
                        <th>Amount</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($payments)>0)
                        @foreach($payments as $row)
                        <tr>
                            <td>{{date("d F Y H:i",strtotime($row->payment_date))}}</td>
                            <td>{{$row->method->name}}</td>
                            <td>Rp {{number_format($row->amount,0,",",".")}}</td>
                            <td>{{($row->note)?$row->note:'-'}}</td>
                        </tr> 
                        @endforeach
                    @else
                        <tr><td colspan="4"><center>Data is empty</center></td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <ul>
               <li>Payment Total : <b>Rp {{number_format($totalPayment,0,",",".")}}</b></li>
               <li>Cashback : <b>Rp {{($totalPayment)?number_format($totalPayment-$transaction->total,0,",","."):0}}</b></li>
            </ul>
        </div>
    </div>
</div>
 
<div class="col-xs-12">
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to($url)}}" class="btn btn-default">{{trans('button.bc')}}</a>
    </div>
</div>
@stop