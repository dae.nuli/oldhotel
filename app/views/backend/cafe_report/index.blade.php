@extends('backend.layouts.content')

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to($url)}}" class="btn btn-primary"><i class="fa fa-fw fa-bar-chart-o"></i> {{$chartText}} Chart</a>
</div>

<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('/admin/cafeReport/summary/')}}" target="_blank" class="btn btn-primary"><i class="fa fa-fw fa-print"></i> Print</a>
</div>

{{Form::open(array('url'=>$url.'/filter', 'method'=>'GET'))}}
<div class="pull-right" style="margin-left:5px">
    <button type="submit" class="btn btn-warning">Clear</button>
</div>
{{Form::close()}}

{{Form::open(array('url'=>$url.'/filter', 'method'=>'GET','class'=>'pull-right set-width'))}}
<div class="pull-right" style="margin-left:5px">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
<div class="input-group">
    <input type="text" name="Cafe_date_filter" class="form-control pull-right date-filter" value="{{Session::get('Cafe_date_filter')}}" style="width: 200px;" placeholder="yyyy-mm-dd / yyyy-mm-dd" autocomplete="off"/>
    <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
    </div>
</div>
{{Form::close()}}
<h5 class="pull-right">Grand Total : Rp {{number_format($grandTotal,0,',','.')}}</h5>
@stop

@section('body-content')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('success')}}.
    </div>
@endif
@if(!empty($is_chart))
<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Chart</h3>
                <h3 class="box-title pull-right">
                    {{Form::open(array('url' => 'admin/cafeReport', 'method' => 'post'))}}
                    <select class="form-control" name="sales" onchange="this.form.submit()">
                        @foreach($yearRange as $row)
                            <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                        @endforeach
                    </select>
                    {{Form::close()}}
                </h3>
            </div>
            <div class="box-body chart-responsive">
                <canvas id="myChart" style="width:100%; height:200px;" height="400"></canvas>
                {{-- <div class="chart" id="revenue-chart" style="height: 300px;"></div> --}}
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Transaction Number</th>
                <th>Customer Name</th>
                <th>Total</th>
                <th>Payment Status</th>
                <th>Order Status</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($index as $row)
            <tr>
                <td>{{$nomor++}}</td>
                <td>{{date('d F Y',strtotime($row->created_at))}}</td>
                <td><a href="{{URL::to($url.'/detail/'.$row->id)}}">{{$row->transaction_number}}</a></td>
                <td>{{$row->customer_name}}</td>
                <td>Rp {{number_format($row->total,0,",",".")}}</td>
                <td>
                    @if($row->payment_status==1)
                        <span class="label label-success">PAID</span>
                    @else
                        <span class="label label-danger">UNPAID</span>
                    @endif
                </td>
                <td>{{($row->order_status==1)?'<small class="label label-success">DELIVERED</small>':'<small class="label label-warning">PROCESSED</small>'}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$index->links()}}
    </div>
</div>
<div id="open-modal"></div>
@stop

@section('end-script')
    @parent
    <!-- InputMask -->
    <script src="{{asset('assets/js/Chart.js')}}"></script>
    <script src="{{asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker/daterangepicker-bs3.css')}}"/ >
    <script src="{{asset('assets/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript">
    // $(".date-filter").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd_yyyy-mm-dd"});
    $('.date-filter').daterangepicker({
        format: 'YYYY-MM-DD',
        separator:' / '
    });
    $(function () {
        var data = {
            labels: [{{$month}}],
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [{{$chart}}]
                }
            ]
        };
        // Get context with jQuery - using jQuery's .get() method.
        var ctx = $("#myChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var myNewChart = new Chart(ctx).Line(data);
    })
    </script>
@stop