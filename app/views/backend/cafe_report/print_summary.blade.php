<?php $about = Helper::about(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{$about->name}} | Report</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('assets/css/new/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('assets/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .paid{
      color: black;
    }
    .notpaid{
      color: red;
    }
    .upper{
      text-transform: uppercase;
    }
    </style>
  </head>
  <body onload="window.print();">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">{{$about->name}}.
              @if(!empty($start) && !empty($end))
              <small class="pull-right">From: {{date('d F Y',strtotime($start))}}, to {{date('d F Y',strtotime($end))}} </small>
              @else
              <small class="pull-right">Date: {{date('d F Y')}} </small>
              @endif
              <small class="pull-right" style="margin-right:20px"><b>Grand Total : Rp {{number_format($grandTotal,0,',','.')}}</b></small>
            </h2>
          </div><!-- /.col -->
        </div> 

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped" style="
    font-size: 12px; width:100%
">
               <thead>
                  <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Transaction ID</th>
                      <th>Customer Name</th>
                      <th>Total</th>
                      <th>Payment Status</th>
                      <th>Order Status</th>
                  </tr>
                  </thead>
                  <?php 
                  $nomor = 1;
                  ?>
                  <tbody>
                  @foreach($report as $row)
                  <tr>
                      <td>{{$nomor++}}</td>
                      <td>{{date('d M Y',strtotime($row->created_at))}}</td>
                      <td>{{$row->transaction_number}}</td>
                      <td>{{$row->customer_name}}</td>
                      <td>Rp {{number_format($row->total,0,",",".")}}</td>
                      <td class="upper">
                          @if($row->payment_status==1)
                              <b class="paid">PAID</b>
                          @else
                              <b class="notpaid">UNPAID</b>
                          @endif
                      </td>
                      <td class="upper">{{($row->order_status==1)?'<b>DELIVERED</b>':'<b>PROCESSED</b>'}}</td>
                  </tr>
                  @endforeach
                  </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->
 
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
  </body>
</html>