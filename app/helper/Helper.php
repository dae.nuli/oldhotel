<?php
class Helper {
	public static function about()
	{
		return AboutModel::find(1);
	}

	public static function expl($pel)
	{
		if(!empty($pel)){
			$aa = explode(',', $pel);
			foreach ($aa as $key => $value) {
				if($value!=''){
					$bb[] = $value;
				}
			}
			return $bb;
		}
	}

	public static function searchRoom($room,$status)
	{
		$tm = TransactionsModel::where('guest_status',$status)->get();
		foreach ($tm as $key => $value) {
			$idTrans[] = $value->id;
		}
		if(isset($idTrans)){
			$tdm = TransactionsDetailModel::whereIn('id_transactions',$idTrans)->where('item','LIKE','%'.$room.'%')->where('type',1)->get();
			foreach ($tdm as $i => $row) {
				$IdTDM[] = $row->id_transactions;
			}
			if(isset($IdTDM)){
				return $IdTDM;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	public static function roomDetail($id)
	{
		if(!empty($id)){
			$rm         = RoomModel::find($id);
			$data['name']  = $rm->name;
			$data['price'] = $rm->price;
			$data['class'] = $rm->classRoom->name;
			return $data;
		}
	}

	public static function setRoomStatus($id)
	{
		$rm = RoomModel::find($id);
		$rm->status = 2;
		$rm->save();
	}

	public static function setRoomStatusToEmpty($id)
	{
		$rm = RoomModel::find($id);
		$rm->status = 1;
		$rm->save();
	}

	public static function setRoomConditionToDirty($id)
	{
		$rm = RoomModel::find($id);
		$rm->kondisi_kamar = 2;
		$rm->save();
	}
	public static function addsDetail($id)
	{
		if(!empty($id)){
			$am            = ExtraChargeModel::find($id);
			$data['name']  = $am->name;
			$data['price'] = $am->price;
			return $data;
		}
	}

	public static function minusCafeItemStock($id,$qty) {
		if(!empty($id)){
			$var = CafeItemModel::find($id);
			$currentStock = ($var->stock - $qty);
			$var->stock = $currentStock;
			$var->save();
		}
	}

	public static function plusCafeItemStock($id,$qty) {
		if(!empty($id)){
			$var = CafeItemModel::find($id);
			$currentStock = ($var->stock + $qty);
			$var->stock = $currentStock;
			$var->save();
		}
	}

	public static function getCafeItemDetail($id) {
		if(!empty($id)){
			$var            = CafeItemModel::find($id);
			$data['name']  = $var->name;
			$data['price'] = $var->price;
			$data['stock'] = $var->stock;
			return $data;
		}
	}

	public static function findFacilityIcon($id)
	{
		$rm = RoomModel::find($id);
		if(!empty($rm->facility)){
			$exp = explode(',', $rm->facility);
			foreach ($exp as $row) {
				$ico = FacilityModel::find($row)->icon;
				$data[] = '<i class="fa-fw '.$ico.'"></i>';
			}
			return implode(' ', $data);
		}else{
			return '-';
		}
	}

	public static function findFacilityName($id)
	{
		$rm = RoomModel::find($id);
		if(!empty($rm->facility)){
			$exp = explode(',', $rm->facility);
			foreach ($exp as $row) {
				$data[] = FacilityModel::find($row)->name;
			}
			return implode(',<br/>', $data);
		}else{
			return '-';
		}
	}

	public static function countDay($a,$b)
	{
		$exp1 = explode(' ', $a);
		$exp2 = explode(' ', $b);

		$checkin       = explode('-', $exp1[0]);
		$date_checkin  = $checkin[2];
		$month_checkin = $checkin[1];
		$year_checkin  = $checkin[0];

		$checkout       = explode('-', $exp2[0]);
		$date_checkout  = $checkout[2];
		$month_checkout = $checkout[1];
		$year_checkout  = $checkout[0];

		$tgl1 = GregorianToJD($month_checkin, $date_checkin, $year_checkin);
		$tgl2 = GregorianToJD($month_checkout, $date_checkout, $year_checkout);
		
		return $day = $tgl2 - $tgl1;
	}



	// public static function ProductPicture($code)
	// {
	// 	return ProductsModel::where('code',$code)->first();
	// }
	public static function count_views($id,$type)
	{
		return VisitorModel::where('id_article',$id)->where('type',$type)->count();
	}

	// public static function total_subcategory($id)
	// {
	// 	return SubCategoryModel::where('id_category',$id)->count();
	// }

	// public static function ProductTotal($idSubCategory)
	// {
	// 	return ProductsModel::where('id_subcategory',$idSubCategory)->count();
	// }

	// public static function total_news($id)
	// {
	// 	return NewsModel::where('id_category',$id)->count();
	// }

	// public static function category_name($id)
	// {
	// 	$SCM = SubCategoryModel::find($id);
	// 	$CM  = CategoryModel::find($SCM->id_category);
	// 	return $CM->name;
	// }

	// public static function selected_category($id)
	// {
	// 	$SCM = SubCategoryModel::find($id);
	// 	$CM  = CategoryModel::find($SCM->id_category);
	// 	return $CM->id;
	// }

	// public static function SubPrefix($id)
	// {
	// 	$scm = SubCategoryModel::find($id);
	// 	return $scm->code_prefix;
	// }

	// public static function ProductPrice($code)
	// {
	// 	$pm = ProductsModel::where('code',$code)->first();
	// 	return $pm->price;
	// }

	// public static function findCodebyID($ID)
	// {
	// 	$pm = ProductsModel::find($ID);
	// 	return $pm->code;
	// }

	public static function TotalPayment($id)
	{
		$pay = TransactionsPaymentModel::where('id_transactions',$id)->count();
		if($pay){
			$OPM = TransactionsPaymentModel::where('id_transactions',$id)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$amount = array_sum($total);
		}else{
			$amount = 0; 
		}
		return $amount;
	}

	public static function CafeTotalPayment($id)
	{
		$pay = CafeTransactionPaymentModel::where('id_cafe_transaction',$id)->count();
		if($pay){
			$OPM = CafeTransactionPaymentModel::where('id_cafe_transaction',$id)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$amount = array_sum($total);
		}else{
			$amount = 0; 
		}
		return $amount;
	}

	public static function CheckOrderedItem($idTransaction,$idItem) {
		return CafeTransactionDetailModel::where('id_cafe_transaction',$idTransaction)->where('id_cafe_item',$idItem)->count();
	}
	// public static function CheckPayment($idOrder)
	// {
	// 	$om = OrdersModel::find($idOrder);
	// 	$pay = OrdersPaymentModel::where('id_order',$idOrder)->count();
	// 	if($pay){
	// 		$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
	// 		foreach ($OPM as $key => $value) {
	// 			$total[] = $value->amount;
	// 		}
	// 		$amount = array_sum($total);
	// 	}else{
	// 		$amount = 0; 
	// 	}

	// 	if(!empty($amount)){
	// 		if($om->total == $amount || $amount > $om->total ){
	// 			return '<span class="label label-success">Paid</span>';
	// 		}else{
	// 			return '<span class="label label-warning">Not Paid</span>';
	// 		}
	// 	}else{
	// 		return '<span class="label label-warning">Not Paid</span>';
	// 	}
	// }

	// public static function CheckPaymentStatus($idOrder)
	// {
	// 	$om = OrdersModel::find($idOrder);
	// 	$pay = OrdersPaymentModel::where('id_order',$idOrder)->count();
	// 	if($pay){
	// 		$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
	// 		foreach ($OPM as $key => $value) {
	// 			$total[] = $value->amount;
	// 		}
	// 		$amount = array_sum($total);
	// 	}else{
	// 		$amount = 0; 
	// 	}

	// 	if(!empty($amount)){
	// 		if($om->total == $amount || $amount > $om->total ){
	// 			return '<span class="label label-success">Lunas</span>';
	// 		}else{
	// 			return '<span class="label label-warning">Belum Lunas</span>';
	// 		}
	// 	}else{
	// 		return '<span class="label label-warning">Belum Lunas</span>';
	// 	}
	// }

	public static function CheckOrder($idOrder)
	{
		$om = OrdersModel::find($idOrder);
		$start_date  = strtotime($om->start_create);
		$finish_date = strtotime($om->finish_create);
		$now         = strtotime(date('Y-m-d'));
		if(!empty($start_date) || !empty($finish_date)){
			if($now >= $start_date && $now < $finish_date)
			{
				return '<span class="label label-info">On Process</span>';
			}else if($now >= $finish_date){
				return '<span class="label label-success">Finish</span>';
			}else{
				return '<span class="label label-danger">New</span>';
			}
		}else{
			return '<span class="label label-danger">New</span>';
		}
	}

	public static function TransactionTotal($customers)
	{
		return TransactionsModel::withTrashed()->where('id_customers',$customers)->count();
	}

	public static function GroupTotal($idGroup)
	{
		return UserModel::where('level',$idGroup)->count();
	}

	public static function CheckGroupName($idGroup)
	{
		return UsersGroupModel::find($idGroup)->group_name;
	}

	public static function FindCategory($CodeProduct)
	{
		$PM  = ProductsModel::withTrashed()->where('code',$CodeProduct)->first();
		$SCM = SubCategoryModel::find($PM->id_subcategory);
		$CM  = CategoryModel::find($SCM->id_category);
		return $CM->name;
	}

	public static function FindSubCategory($CodeProduct)
	{
		$PM  = ProductsModel::withTrashed()->where('code',$CodeProduct)->first();
		$SCM = SubCategoryModel::find($PM->id_subcategory);
		return $SCM->name;
	}

	public static function collectedPayment()
	{
		$PM = PaymentModel::all();
		$total[] = 0;
		foreach ($PM as $key => $row) {
			$total[] = $row->amount;
		}
		return array_sum($total);
	}

	public static function CafeCheckStatusPayment($transaction)
	{
		$TPM = CafeTransactionPaymentModel::where('id_cafe_transaction',$transaction)->get();
		foreach ($TPM as $key => $row) {
			$total[] = $row->amount;
		}
		$grandTotal = isset($total)?array_sum($total):0;
		$TM = CafeTransactionModel::find($transaction);
		
		if(!empty($TM)){
			if(($TM->total <= $grandTotal) && $grandTotal!=0){
				$TM->payment_status = 1;
			}else{
				$TM->payment_status = 0;
			}
			$TM->save();
		}
	}

	public static function CheckStatusPayment($transaction)
	{
		$TPM = TransactionsPaymentModel::where('id_transactions',$transaction)->get();
		foreach ($TPM as $key => $row) {
			$total[] = $row->amount;
		}
		$grandTotal = isset($total)?array_sum($total):0;
		$TM = TransactionsModel::find($transaction);
		
		if(!empty($TM)){
			if(($TM->total <= $grandTotal) && $grandTotal!=0){
				$TM->payment_status = 1;
			}else{
				$TM->payment_status = 0;
			}
			$TM->save();
		}

	}

	public static function ReportGrandTotal($from,$to)
	{
		$total[] =0;
		if(!empty($from) && !empty($to)){
		$OM = TransactionsModel::where('payment_status',1)
				->orderBy('created_at','asc')
				->whereBetween('created_at',array($from.' 00:00:00',$to.' 23:59:59'))->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}else{
		$OM = TransactionsModel::where('payment_status',1)
				->orderBy('created_at','asc')
				->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}
	}

	public static function CafeReportGrandTotal($from,$to)
	{
		$total[] =0;
		if(!empty($from) && !empty($to)){
		$OM = CafeTransactionModel::where('payment_status',1)
				->orderBy('created_at','asc')
				->whereBetween('created_at',array($from.' 00:00:00',$to.' 23:59:59'))->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}else{
		$OM = CafeTransactionModel::where('payment_status',1)
				->orderBy('created_at','asc')
				->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}
	}

	public static function afterDiscount($price,$discount)
	{
		return ($price-($price*($discount/100)));
	}

	// Front End


}