<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTambahan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('tambahan')){
			Schema::create('tambahan', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->string('name',100);
			    $table->string('price',20);
			    $table->integer('created_by');
			    $table->integer('updated_by');
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
