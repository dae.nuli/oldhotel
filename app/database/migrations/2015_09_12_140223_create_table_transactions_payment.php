<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionsPayment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('transactions_payment')){
			Schema::create('transactions_payment', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->integer('id_transactions')->unsigned();
			    $table->string('amount',50);
			    $table->dateTime('payment_date');
			    $table->integer('payment_method');
			    $table->string('note',255);
			    $table->integer('created_by');
			    $table->integer('updated_by');
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
