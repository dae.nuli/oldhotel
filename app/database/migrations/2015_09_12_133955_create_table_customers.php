<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('customers')){
			Schema::create('customers', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->string('title',10);
			    $table->string('full_name',100);
			    $table->string('email',100)->nullable();
			    $table->string('phone',50);
			    $table->string('id_card_number',100);
			    $table->string('card_type',20);
			    $table->string('address',100);
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
