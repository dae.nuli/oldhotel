<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionsDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('transactions_detail')){
			Schema::create('transactions_detail', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->integer('id_transactions')->unsigned();
			    $table->string('item',255);
			    $table->integer('qty')->unsigned();
			    $table->string('item_price',50);
			    $table->string('note',255);
			    $table->integer('created_by')->nullable()->default(null);
			    $table->integer('updated_by')->nullable()->default(null);
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
