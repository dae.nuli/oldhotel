<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('rooms')){
			Schema::create('rooms', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->integer('id_parent')->nullable()->default(null);
			    $table->string('name',100);
			    $table->string('price',20);
			    $table->enum('status',array(1,2,3))->default(1)->nullable();
			    $table->text('facility');
			    $table->integer('created_by');
			    $table->integer('updated_by');
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
