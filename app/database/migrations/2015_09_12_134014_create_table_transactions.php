<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('transactions')){
			Schema::create('transactions', function($table)
			{
			    $table->increments('id')->unsigned();
			    $table->string('code',100);
			    $table->integer('id_customers')->unsigned();
			    $table->dateTime('checkin_date');
			    $table->dateTime('checkout_date');
			    $table->string('total',50);
			    $table->string('note',255);
			    $table->enum('status',array('0','1'));
			    $table->enum('book_via',array('web','fo'));
			    $table->integer('created_by')->nullable()->default(null);
			    $table->integer('updated_by')->nullable()->default(null);
			    $table->softDeletes();
			    $table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
