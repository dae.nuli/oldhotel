<?php

class CafeTransactionModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'cafe_transaction';

	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}
}
