<?php

class CafeItemModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'cafe_item';
	public function author()
	{
		return $this->belongsTo('UserModel','created_by');
	}

}
