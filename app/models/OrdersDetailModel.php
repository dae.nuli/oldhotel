<?php

class OrdersDetailModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders_detail';

	public function client()
	{
		return $this->belongsTo('OrdersModel','id_order');
	}
}
