<?php

class TransactionsPaymentModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'transactions_payment';

	public function method()
	{
		return $this->belongsTo('PaymentMethodModel','payment_method');
	}
}
