<?php

class ProductsModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'products';

	public function categories()
	{
		return $this->belongsTo('SubCategoryModel','id_subcategory');
	}

	public function subcategory()
	{
		return $this->hasManyThrough('SubCategoryModel','CategoryModel','id_category','id_subcategory');
	}

	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

}
