<?php

class TransactionsModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'transactions';

	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

	public function customers()
	{
		return $this->belongsTo('CustomersModel','id_customers');
	}
}
