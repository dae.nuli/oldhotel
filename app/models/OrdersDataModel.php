<?php

class OrdersDataModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders_data';

	public function religion()
	{
		return $this->belongsTo('ReligionModel','id_religion');
	}

}
