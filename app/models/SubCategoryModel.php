<?php

class SubCategoryModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_subcategory';
	
	public function categories()
	{
		return $this->belongsTo('CategoryModel','id_category');
	}
	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}
}
