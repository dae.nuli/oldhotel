<?php

class GalleryModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gallery';
	
	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

}
