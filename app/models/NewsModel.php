<?php

class NewsModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news';
	
	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}
	
	public function category()
	{
		return $this->belongsTo('NewsCategoryModel','id_category');
	}
}
