<?php

class CafeTransactionPaymentModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'cafe_payment';

	public function method()
	{
		return $this->belongsTo('PaymentMethodModel','payment_method');
	}
}
