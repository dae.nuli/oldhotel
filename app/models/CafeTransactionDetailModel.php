<?php

class CafeTransactionDetailModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'cafe_transaction_detail';

	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

	public function tansaction()
	{
		return $this->belongsTo('CafeTransactionModel','id_cafe_transaction');
	}
}
