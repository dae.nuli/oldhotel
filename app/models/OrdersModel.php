<?php

class OrdersModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'orders';

	public function client()
	{
		return $this->belongsTo('ClientsModel','id_client');
	}
}
