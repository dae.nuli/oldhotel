<?php

class NewsCategoryModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news_category';
	
	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

}
