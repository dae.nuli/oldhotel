<?php

class TransactionsDetailModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'transactions_detail';

	public function author()
	{
		return $this->belongsTo('UserModel','id_user');
	}

	public function tansaction()
	{
		return $this->belongsTo('TransactionsModel','id_transactions');
	}
}
