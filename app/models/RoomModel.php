<?php

class RoomModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $softDelete = true;
	protected $table = 'rooms';
	public function author()
	{
		return $this->belongsTo('UserModel','created_by');
	}

	public function classRoom()
	{
		return $this->belongsTo('RoomModel','id_parent');
	}
}
