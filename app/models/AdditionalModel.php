<?php

class AdditionalModel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tambahan';
	
	public function author()
	{
		return $this->belongsTo('UserModel','created_by');
	}
}
