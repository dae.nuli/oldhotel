<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','HomeController@getIndex');
// Route::get('/', function() {
// 	return Redirect::to('/index.php');
// });
Route::controller('login','HomeController');
Route::group(array('before'=>'auth_admin'),function(){

	Route::group(array('prefix' => 'admin'), function(){

		
		Route::get('/home','Dashboard@index');
		Route::post('/home','Dashboard@postIndex');
		Route::controller('/profile','Profiles');
		//ACL
		Route::group(array('before' => 'acl'), function(){

			Route::controller('/checkIn','Booking');
			Route::controller('/checkOut','Checkout');
			Route::controller('/extracharge','ExtraCharge');
			// Route::controller('/products','Products');
			
			Route::controller('/roomClass','RoomClass');
			Route::controller('/room','RoomList');
			Route::controller('/facility','Facility');
			
			// Route::controller('/invoices','Invoices');
			Route::controller('/customers','Customers');
			// Route::controller('/bank','Bank');
			Route::controller('/about','About');
			// Route::controller('/testimony','Testimony');
			// Route::controller('/how','How');
			// Route::controller('/banner','Banner');
			Route::controller('/statistics','Statistics');
			Route::controller('/report','Report');
			Route::controller('/messages','Messages');
			
			
			Route::controller('/cafeReport','CafeReport');
			Route::controller('/cafeItem','CafeItem');
			Route::controller('/cafeTransaction','CafeTransaction');
		});
		Route::group(array('before' => 'acl_admin'), function(){

			Route::controller('/users','Users');
			Route::controller('/deleted-staff','DeletedUsers');
			Route::controller('/group','Groups');
			Route::controller('/permission','Permissions');

		});
		// Route::get('/',function(){
		// 	return Redirect::to('/admin/home');
		// });
	});
	Route::get('/logout','Dashboard@logout');
	
});
// App::error(function($exception, $code) { 
// 	if(Request::is('admin/*')){
// 		switch ($code) {
// 			case 404:
// 				$errors = 'Page not found';
// 				$note   = 'We could not find the page you were looking for';
// 				break;
// 			default:
// 				$errors = 'Something went wrong';
// 				$note   = 'We will work on fixing that right away';
// 				break;
// 		}
// 		View::share('title',$code);
// 		View::share('path',$errors);
// 		$data['code']  = $code;
// 		$data['error'] = $errors;
// 		$data['note']  = $note;
// 		return View::make('backend.errors',$data);
// 	}
// });

// App::error(function($exception, $code) { 
// 	// if(!Request::is('admin/*')){		
// 		return 'errors';
// 	// }
// });