<?php

class HotelController extends BaseController {
	public $limit = 9;
	public $ip;

	public function __construct()
	{
		 $this->ip = $_SERVER['REMOTE_ADDR'];
	}

	
	public function not()
	{
		return View::make('front.404');
	}
}