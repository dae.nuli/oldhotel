<?php

class ExtraCharge extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Extra Charge');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= ExtraChargeModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['extra'] = $qr->paginate($this->limit);
		return View::make('backend.extracharge.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Extra Charge');
		View::share('path','Create');
		return View::make('backend.extracharge.create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'     => 'required',
			'price'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/extracharge/create')->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');

				$cm             = new ExtraChargeModel;
				$cm->created_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang); 
				$cm->save();
				return Redirect::to('admin/extracharge')->with('extra','Data has been added');
			}else{
				return Redirect::to('admin/extracharge')->with('extra_alert','The price field is required.');
			}
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Extra Charge');
		View::share('path','Create');
		$data['extra'] = ExtraChargeModel::find($id);
		return View::make('backend.extracharge.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'  => 'required',
			'price' => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/extracharge/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');
				$cm             = ExtraChargeModel::find($id);
				$cm->updated_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang); 
				$cm->save();
				return Redirect::to('admin/extracharge')->with('extra','Data has been added');
			}else{
				return Redirect::to('admin/extracharge')->with('extra_alert','The price field is required.');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = ExtraChargeModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				$tdm = TransactionsDetailModel::withTrashed()->where('id_item',$id)->where('type',0)->count();
				if(!empty($tdm)){
					return Redirect::to('admin/extracharge')->with('extra_alert','The additional is used');
				}else{
					$am->delete();
					return Redirect::to('admin/extracharge')->with('extra','Data has been deleted');
				}
			}
		}
	}

}
