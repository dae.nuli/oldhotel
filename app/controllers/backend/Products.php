<?php

class Products extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Products');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$qr	= ProductsModel::orderBy('id','desc');
			if($cari){
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);		
		}else{
			$qr	= ProductsModel::where('id_user',$this->admin['id']);
			if($cari){
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
			$qr = $qr->orderBy('id','desc')->paginate($this->limit);
		}
		$data['product'] = $qr;
		return View::make('backend.product.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Products');
		View::share('path','Create');
		if($this->admin['level'] == 1){
			$category = CategoryModel::all();
		}else{
			$category = CategoryModel::where('id_user',$this->admin['id'])->first();
		} 
		$data['category'] = $category;
		return View::make('backend.product.create',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'category'     => 'required',
			'sub_category' => 'required',
			'price'        => 'required|numeric',
			'content'      => 'required',
			'picture'      => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/create')->withErrors($valid)->withInput();
		}else{
			$id_sub = Input::get('sub_category');

			$prefix  = Helper::SubPrefix($id_sub);
			$pm      = ProductsModel::where('id_subcategory',$id_sub)->orderBy('code','desc')->first();
			if(empty($pm)){
				$code_product = $prefix."0001";
			}else{
				$number = (int)substr($pm->code, strlen($prefix), 4);
				$number++;
				$code_product = $prefix.sprintf("%04d", $number);
			}
			$picture            = Input::get('picture');
			$cm                 = new ProductsModel;
			$cm->id_user        = $this->admin['id'];
			$cm->id_subcategory = $id_sub;
			$cm->code 			= $code_product;
			$cm->price          = Input::get('price');
			$cm->note           = Input::get('content');
			$cm->picture        = ($picture?$picture:'');
			$cm->save();
			return Redirect::to('admin/products')->with('products','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postSubcategory()
	{
		if(Request::ajax())
		{
			$id_category = Input::get('id_category');
			if(!empty($id_category)){
				if($this->admin['level'] == 1){
					$SCM = SubCategoryModel::where('id_category',$id_category)->get();
				}else{
					$SCM = SubCategoryModel::where('id_category',$id_category)->where('id_user',$this->admin['id'])->get();
				} 
				if(count($SCM) > 0){
					$html[] = '<option value="">- Select Sub Category -</option>';
					foreach ($SCM as $key => $value) {
						$html[] = "<option value='".$value->id."'>".$value->name."</option>";
					}
				}else{
					$html[] = '<option value="">- Sub Category is Empty -</option>';
				}
				return $html;
			}
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Products');
		View::share('path','Create');

		if($this->admin['level'] == 1)
		{
			$cm  = ProductsModel::find($id);
			$ct  = CategoryModel::all();
		}else{
			$cm  = ProductsModel::where('id',$id)
					->where('id_user',$this->admin['id'])
					->first();
			$ct  = CategoryModel::where('id_user',$this->admin['id'])->get();
		}
		$data['category']    = $ct;
		$data['selected_category']    = Helper::selected_category($cm->id_subcategory);
		$data['product']     = $cm;
		return View::make('backend.product.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'category'     => 'required',
			'sub_category' => 'required',
			'price'        => 'required|numeric',
			'content'      => 'required',
			'picture'      => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$picture            = Input::get('picture');
			$cm                 = ProductsModel::find($id);
			
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$sub_cat = Input::get('sub_category');
				if($cm->id_subcategory==$sub_cat){
					$cm->id_subcategory = $sub_cat;
					$cm->price          = Input::get('price');
					$cm->note           = Input::get('content');
					$cm->picture        = ($picture?$picture:'');
					$cm->save();
				}else{
					$ODM = OrdersDetailModel::where('code_product',$cm->code)->count();
					if($ODM > 0){
						return Redirect::to('admin/products')->with('products_cannot_change_subcategory','This product is used in order');
					}else{
						// $id_sub = Input::get('sub_category');

						$prefix  = Helper::SubPrefix($sub_cat);
						$pm      = ProductsModel::where('id_subcategory',$sub_cat)->orderBy('code','desc')->first();
						if(empty($pm)){
							$code_product = $prefix."0001";
						}else{
							$number = (int)substr($pm->code, strlen($prefix), 4);
							$number++;
							$code_product = $prefix.sprintf("%04d", $number);
						}
						$PMS = ProductsModel::find($id);
						$PMS->code           = $code_product;
						$PMS->id_subcategory = $sub_cat;
						$PMS->price          = Input::get('price');
						$PMS->note           = Input::get('content');
						$PMS->picture        = ($picture?$picture:'');
						$PMS->save();
					}
				}
				return Redirect::to('admin/products')->with('products','Data has been added');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = ProductsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/products')->with('products','Data has been deleted');
			}
		}
	}

}
