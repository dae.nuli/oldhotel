<?php

class Additional extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Additional');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= AdditionalModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['additional'] = $qr->paginate($this->limit);
		return View::make('backend.additional.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Additional');
		View::share('path','Create');
		return View::make('backend.additional.create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'     => 'required',
			'price'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/additional/create')->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');

				$cm             = new AdditionalModel;
				$cm->created_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang); 
				$cm->save();
				return Redirect::to('admin/additional')->with('additional','Data has been added');
			}else{
				return Redirect::to('admin/additional')->with('additional_alert','The price field is required.');
			}
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Additional');
		View::share('path','Create');
		$data['additional'] = AdditionalModel::find($id);
		return View::make('backend.additional.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'  => 'required',
			'price' => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/additional/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');
				$cm             = AdditionalModel::find($id);
				$cm->updated_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang); 
				$cm->save();
				return Redirect::to('admin/additional')->with('additional','Data has been added');
			}else{
				return Redirect::to('admin/additional')->with('additional_alert','The price field is required.');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = AdditionalModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				$tdm = TransactionsDetailModel::withTrashed()->where('id_item',$id)->where('type',0)->count();
				if(!empty($tdm)){
					return Redirect::to('admin/additional')->with('additional_alert','The additional is used');
				}else{
					$am->delete();
					return Redirect::to('admin/additional')->with('additional','Data has been deleted');
				}
			}
		}
	}

}
