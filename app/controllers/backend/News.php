<?php

class News extends BaseController {
	public $admin;
	public $limit = 10;
	public function __construct()
	{
		$this->admin = Session::get('admin');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		View::share('title','News');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$qr	= NewsModel::orderBy('id','desc');
			if($cari){
				$qr = $qr->where('title','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);		
		}else{
			$qr	= NewsModel::where('id_user',$this->admin['id']);
			if($cari){
				$qr = $qr->where('title','LIKE',"%$cari%");
			}
			$qr = $qr->orderBy('id','desc')->paginate($this->limit);
		}
		$data['news'] = $qr;
		return View::make('backend.news.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		View::share('title','News');
		View::share('path','Create');
		if($this->admin['level'] == 1){
			$data['category']	= NewsCategoryModel::all();
		}else{
			$data['category']	= NewsCategoryModel::where('id_user',$this->admin['id'])->get();
		}
		return View::make('backend.news.create',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'title'    => 'required',
			'category' => 'required',
			'content'  => 'required',
			'picture'  => 'required|max:2000|mimes:jpeg,png'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/news/create')
			->withErrors($valid)
			->withInput();
		}else{
			$title = Input::get('title');
			$label = Input::get('label');
			$cm              = new NewsModel;
			$cm->title       = $title;
			$cm->id_category = Input::get('category');
			$cm->content     = Input::get('content');
			$cm->id_user     = $this->admin['id'];
			if(Input::hasFile('picture'))
			{
				$gambar      =	Input::file('picture');
				$nama_gambar =	Str::random(10).'.'.$gambar->getClientOriginalExtension();
				$nm 	     =	NewsModel::where('picture',$nama_gambar)->count();
				while ($nm > 0) {
					$nama_gambar =	Str::random(10).'.'.$gambar->getClientOriginalExtension();
					$nm          =	NewsModel::where('picture',$nama_gambar)->count();
				}
				$size        = $_FILES['picture']['size'];
				$tmp_name    = $_FILES['picture']['tmp_name'];
				$type        = $_FILES['picture']['type'];
				ImageManipulation::uploadimage($nama_gambar,$size,$tmp_name,$type,'news');

				$cm->picture	=	$nama_gambar;
			}
			$cm->publish = Input::get('note');
			$cm->save();

			$up = NewsModel::find($cm->id);
			// Lib::insertLabel($label,$cm->id);
			$up->slug  = Str::slug($title).'-'.$cm->id;
			$up->save();

			return Redirect::to('admin/news')->with('news','Data has been created');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','News');
		View::share('path','Edit');

		if($this->admin['level'] == 1)
		{
			$data['category'] = NewsCategoryModel::all();
			$nm = NewsModel::find($id);
		}else{
			$data['category'] = NewsCategoryModel::where('id_user',$this->admin['id'])->get();
			$nm = NewsModel::where('id',$id)
			->where('id_user',$this->admin['id'])
			->first();
		}
		$data['news'] = $nm;
		return View::make('backend.news.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'title'    => 'required',
			'category' => 'required',
			'content'  => 'required'
			);
		if(Input::hasFile('picture')){
			$rules['picture'] = 'max:2000|mimes:jpeg,png';
		}
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/news/edit/'.$id)->withErrors($valid);
		}else{
			$title = Input::get('title');
			$label = Input::get('label');
			$cm              = NewsModel::find($id);
			if($cm->id_user == $this->admin['id'] || $this->admin['level'] == 1){
				$cm->slug        = Str::slug($title);
				$cm->title       = $title;
				$cm->id_category = Input::get('category');
				$cm->content     = Input::get('content');

				if(Input::hasFile('picture'))
				{
					
					$path       = public_path('assets/store/news/'.$cm->picture);
					// $path_thumb = public_path('assets/news/thumb_'.$cm->picture);
					if( is_file($path) ){
						unlink('assets/store/news/'.$cm->picture);
						// unlink('assets/news/thumb_'.$cm->picture);
					}
					$gambar      =	Input::file('picture');
					$nama_gambar =	Str::random(10).'.'.$gambar->getClientOriginalExtension();
					$nm 	     =	NewsModel::where('picture',$nama_gambar)->count();
					while ($nm > 0) {
						$nama_gambar =	Str::random(10).'.'.$gambar->getClientOriginalExtension();
						$nm          =	NewsModel::where('picture',$nama_gambar)->count();
					}
					$size        = $_FILES['picture']['size'];
					$tmp_name    = $_FILES['picture']['tmp_name'];
					$type        = $_FILES['picture']['type'];
					ImageManipulation::uploadimage($nama_gambar,$size,$tmp_name,$type,'news');
					
					$cm->picture	=	$nama_gambar;
				}
				$cm->publish = Input::get('note');
				$cm->save();

				$up = NewsModel::find($cm->id);
				$up->slug  = Str::slug($title).'-'.$cm->id;
				$up->save();
				return Redirect::to('admin/news')->with('news','Data has been updated');
			}
		}
	}

	public function getPublish($id)
	{
		$tm = NewsModel::find($id);
		$tm->publish = 1;
		$tm->save();
		return Redirect::to('admin/news')->with('news','Data has been published'); 
	}

	public function getDraft($id)
	{
		$tm = NewsModel::find($id);
		$tm->publish = 0;
		$tm->save();
		return Redirect::to('admin/news')->with('news','Data has been drafted'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = NewsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){

			$path       = public_path('assets/store/news/'.$am->picture);
			// $path_thumb = public_path('assets/store/news/thumb_'.$am->picture);

			if( is_file($path) ){
				unlink('assets/store/news/'.$am->picture);
				// unlink('assets/store/news/thumb_'.$am->picture);
			}
			
			$am->delete();
			return Redirect::to('admin/news')->with('news','Data has been deleted');
		}
	}

	// public function getDeletecomment($id)
	// {
	// 	$cm = CommentModel::find($id);
	// 	$am = NewsModel::find($cm->id_article);
	// 	if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
	// 		define('ID', $cm->id_article);
	// 		$cm->delete();
	// 		return Redirect::to('admin/news/comments/'.ID)->with('news','Data Comment has been deleted');
	// 	}
	// }

	// public function getViewcomment($id,$type=3)
	// {
	// 	View::share('title','News');
	// 	View::share('path','Comment');
	// 	$data['limit']    = $this->limit;
	// 	$am = NewsModel::find($id);
	// 	if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
	// 		$data['comments'] = CommentModel::where('id_article',$id)->where('type',$type)->paginate($this->limit);
	// 		return View::make('backend.news.comment',$data);
	// 	}
	// }

	// public function getViewvisitor($id,$type=3)
	// {
	// 	View::share('title','News');
	// 	View::share('path','Viewer');
	// 	$data['limit']    = $this->limit;
	// 	$am = NewsModel::find($id);
	// 	if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
	// 		$data['visitors'] = VisitorModel::where('id_article',$id)->where('type',$type)->paginate($this->limit);
	// 		return View::make('backend.news.visitor',$data);
	// 	}
	// }

	// public function getPublish($id)
	// {
	// 	$cm = CommentModel::find($id);
	// 	$am = NewsModel::find($cm->id_article);
	// 	if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
	// 		$cm->status = 1;
	// 		$cm->save();
	// 		return Redirect::to('admin/news/comments/'.$cm->id_article)->with('news','Data has been published');
	// 	}
	// }
	
	// public function getUnpublish($id)
	// {
	// 	$cm = CommentModel::find($id);
	// 	$am = NewsModel::find($cm->id_article);
	// 	if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
	// 		$cm->status = 0;
	// 		$cm->save();
	// 		return Redirect::to('admin/news/comments/'.$cm->id_article)->with('news','Data has been unpublished');
	// 	}
	// }


}
