<?php

class CafeTransaction extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit   = 10;
	private $title  = 'Cafe Transaction';
	private $folder = 'backend.cafe_transaction';
	private $uri    = 'admin/cafeTransaction';
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title',$this->title);
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= CafeTransactionModel::orderBy('id','desc')->where('type','0');
		if($cari){
			$qr = $qr->where('transaction_number','LIKE',"%$cari%");
		}
		$qr = $qr->paginate($this->limit);
		$data['index'] = $qr;
		$data['url']   = $this->uri;
		return View::make($this->folder.'.index',$data);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title',$this->title);
		View::share('path','Create');
		$data['item']	= CafeItemModel::orderBy('id','desc')->get();
		$data['action'] = $this->uri;
		$data['url']    = $this->uri;
		return View::make($this->folder.'.create',$data);
	}

	public function postIndex()
	{
		$rules = array(
			'number'	=> 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/create')->withErrors($valid)->withInput();
		}else{

			$codes =	rand(111111,999999);
			$nm    =	CafeTransactionModel::where('transaction_number',$codes)->count();
			while ($nm > 0) {
				$codes =	rand(111111,999999);
				$nm    =	CafeTransactionModel::where('transaction_number',$codes)->count();
			}

			$custName = Input::get('name');
			$tabNum   = Input::get('number');
			$note     = Input::get('note');
			$tm                     = new CafeTransactionModel;
			$tm->created_by         = $this->admin['id'];
			$tm->customer_name 		= ($custName)?$custName:NULL;
			$tm->transaction_number = $codes;
			$tm->table_number       = ($tabNum)?$tabNum:NULL;
			$tm->note               = ($note)?$note:NULL;
			$tm->order_status       = Input::get('status');
			$tm->save();

			$item     = Input::get('id');
			$item_qty = Input::get('qty');
			if(count($item) > 0){
				foreach ($item as $key => $value) {
					$detail = Helper::getCafeItemDetail($value);
					$tdm                      = new CafeTransactionDetailModel;
					$tdm->id_cafe_transaction = $tm->id;
					$tdm->id_cafe_item        = $value;
					$tdm->item_name           = $detail['name'];
					$tdm->qty                 = $item_qty[$key];
					$tdm->item_price          = $detail['price'];
					// $tdm->note                = Input::get('note')[$key];
					$tdm->created_by          = $this->admin['id'];
					$tdm->save();

					$total[] = $detail['price']*$item_qty[$key];
					Helper::minusCafeItemStock($value,$item_qty[$key]);
				}
			}

			$fixedPrice = array_sum($total);

			$TM        = CafeTransactionModel::find($tm->id);
			$TM->total = $fixedPrice;
			$TM->save();

			return Redirect::to($this->uri.'/detail/'.$tm->id)->with('success','Data has been added');
		}
	}

	public function getPrint($id)
	{
		$tm  = CafeTransactionModel::find($id);
		$data['transaction']     = $tm;
		$data['staff']           = UserModel::find($this->admin['id']);
		$data['detail']          = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$id)->get();
		$data['totalPayment']    = Helper::CafeTotalPayment($id);
		return View::make($this->folder.'.print',$data);
	}

	public function postInsertNewItem($transaction)
	{
		$rules = array(
			'id'  => 'required',
			'qty' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/detail/'.$transaction)->withErrors($valid)->withInput();
		}else{
			$tm            = CafeTransactionModel::find($transaction);
			$tdm           = CafeTransactionDetailModel::where('id_cafe_transaction',$transaction)->get();

			foreach ($tdm as $value) {
				$items[] = $value->item_price * $value->qty;
			}

			$itemId   = Input::get('id');
			$inputQty = Input::get('qty');
			$detail   = Helper::getCafeItemDetail($itemId);
			
			$tdm                      = new CafeTransactionDetailModel;
			$tdm->id_cafe_transaction = $tm->id;
			$tdm->id_cafe_item        = $itemId;
			$tdm->item_name           = $detail['name'];
			$tdm->qty                 = $inputQty;
			$tdm->item_price          = $detail['price'];
			$tdm->created_by          = $this->admin['id'];
			$tdm->save();

			Helper::minusCafeItemStock($itemId,$inputQty);

			$items[] = $detail['price']*$inputQty;
				
			$tm->total  = array_sum($items);
			$tm->save();
			Helper::CafeCheckStatusPayment($transaction);
			return Redirect::to($this->uri.'/detail/'.$transaction)->with('success','Item has been added');
		}
	}

	public function getModalEditTransaction($transaction)
	{
		$data['transaction'] = CafeTransactionModel::find($transaction);
		$data['url']         = $this->uri;
		return View::make($this->folder.'.modal_edit_transaction',$data);
	}

	public function postUpdateTransaction($transaction)
	{
		$rules = array(
			'name'         => 'required',
			'table_number' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/detail/'.$transaction)->withErrors($valid)->withInput();
		}else{
			
			$tm                = CafeTransactionModel::find($transaction);
			$tm->customer_name = Input::get('name');
			$tm->table_number  = Input::get('table_number');
			$tm->updated_by    = $this->admin['id'];
			$tm->save();

			return Redirect::to($this->uri.'/detail/'.$transaction)->with('success','Transaction has been updated');
		}
	}
	
	public function getDetail($id)
	{
		View::share('title',$this->title);
		View::share('path','Detail');
		if(!empty($id)){
			$tm = CafeTransactionModel::find($id);
			if(!empty($tm)){
				$data['url']          = $this->uri;
				$data['transaction']  = $tm;
				$data['item_cafe']    = CafeItemModel::orderBy('id','desc')->get();
				$data['item']         = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$id)->get();
				$data['payments']     = CafeTransactionPaymentModel::where('id_cafe_transaction',$id)->get();
				$data['totalPayment'] = Helper::CafeTotalPayment($id);
				return View::make($this->folder.'.detail',$data);
			}
		}

	}

	public function getModalAddPayment($id)
	{
		$data['transaction'] = CafeTransactionModel::find($id);
		$data['method']      = PaymentMethodModel::orderBy('id','desc')->get();
		$data['url']         = $this->uri;
		return View::make($this->folder.'.modal_add_payment',$data);
	}

	public function getModalEditPayment($transaction,$idpayment)
	{
		$data['method']      = PaymentMethodModel::all();
		$data['transaction'] = CafeTransactionModel::find($transaction);
		$data['payment']     = CafeTransactionPaymentModel::find($idpayment);
		$data['url']         = $this->uri;
		return View::make($this->folder.'.modal_edit_payment',$data);
	}

	public function postEditPayment($transaction,$idpayment)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required',
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/detail/'.$transaction)->with('alert','The payment fields are required');;
		}else{
			$uang = Input::get('amount');
			if(!empty($uang)){
				$note   = Input::get('note');
				$filter = array(',','.');
				$TPM                      = CafeTransactionPaymentModel::find($idpayment);
				$TPM->id_cafe_transaction = $transaction;
				$TPM->amount              = str_replace($filter, "", $uang);
				$TPM->payment_date        = Input::get('date');
				$TPM->payment_method      = Input::get('method');
				$TPM->note                = ($note)?$note:'';
				$TPM->updated_by          = $this->admin['id'];
				$TPM->save();
				Helper::CafeCheckStatusPayment($transaction);
				return Redirect::to($this->uri.'/detail/'.$transaction)->with('success','Payment has been updated');
			}else{
				return Redirect::to($this->uri.'/detail/'.$transaction)->with('alert','Amount field is required.');
			}
		}
	}

	public function postAddPayment($id)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/detail/'.$id)->with('alert','The payment fields are required');
		}else{
			$uang = Input::get('amount');
			if(!empty($uang)){
				$note   = Input::get('note');
				$filter = array(',','.');
				$PM                      = new CafeTransactionPaymentModel;
				$PM->id_cafe_transaction = $id;
				$PM->amount              = str_replace($filter, "", $uang); 
				$PM->payment_date        = Input::get('date');
				$PM->payment_method      = Input::get('method');
				$PM->note                = ($note)?$note:'';
				$PM->created_by          = $this->admin['id'];
				$PM->save();

				Helper::CafeCheckStatusPayment($id);
				return Redirect::to($this->uri.'/detail/'.$id)->with('success','Payment has been added');
			}else{
				return Redirect::to($this->uri.'/detail/'.$id)->with('alert','Amount field is required.');
			}
		}
	}

	public function getDeletePayment($idpayment,$transaction)
	{
		CafeTransactionPaymentModel::find($idpayment)->delete();
		Helper::CafeCheckStatusPayment($transaction);
		return Redirect::to($this->uri.'/detail/'.$transaction)->with('success','Payment has been deleted');
	}


	public function getDeleteItem($id)
	{
		$tdm      = CafeTransactionDetailModel::find($id);
		$tm       = CafeTransactionModel::find($tdm->id_cafe_transaction);

		Helper::plusCafeItemStock($tdm->id_cafe_item, $tdm->qty);
		$tdm->delete(); // Remove Items

		$cafeItem = CafeTransactionDetailModel::where('id_cafe_transaction',$tm->id)->get();

		foreach ($cafeItem as $value) {
			$items[] = $value->item_price * $value->qty;
		}


		$tm->total = isset($items)?array_sum($items):NULL;
		$tm->save();

		Helper::CafeCheckStatusPayment($tm->id);
		return Redirect::to($this->uri.'/detail/'.$tm->id)->with('success','Ordered item has been deleted');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDone($id) {
		if(!empty($id)) {
			$tm  = CafeTransactionModel::find($id);
			$tm->order_status = 1;
			$tm->save();
			return Redirect::to($this->uri)->with('success','Order has been delivered');
		}
	}

	public function getDelete($id)
	{
		$tm = CafeTransactionModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($tm)){
				$tdm = CafeTransactionDetailModel::where('id_cafe_transaction',$tm->id)->get();
				CafeTransactionDetailModel::where('id_cafe_transaction',$tm->id)->delete();
				CafeTransactionPaymentModel::where('id_cafe_transaction',$tm->id)->delete();
				$tm->delete();
				return Redirect::to($this->uri)->with('success','Data has been deleted');
			}
		}
	}

}
