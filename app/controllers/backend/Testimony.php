<?php

class Testimony extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Testimony');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$data['testimony']  = TestimonyModel::orderBy('id','desc')->paginate($this->limit);
		return View::make('backend.testimony.index',$data);
	}

	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Testimony');
		return View::make('backend.testimony.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/testimony/create')->withErrors($valid)->withInput();
		}else{
			$bm          = new TestimonyModel;
			$bm->picture = Input::get('picture');
			$bm->save();
			return Redirect::to('admin/testimony')->with('testimony','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Testimony');

		$data['testimony'] = TestimonyModel::find($id);
		return View::make('backend.testimony.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/testimony/edit/'.$id)->withErrors($valid);
		}else{
			$bm          = TestimonyModel::find($id);
			$bm->picture = Input::get('picture');
			$bm->save();
			return Redirect::to('admin/testimony')->with('testimony','Data has been updated'); 
		}
	}

	public function getActive($id)
	{
		$tm = TestimonyModel::find($id);
		$tm->status = 1;
		$tm->save();
		return Redirect::to('admin/testimony')->with('testimony','Data has been activated'); 
	}

	public function getNoactive($id)
	{
		$tm = TestimonyModel::find($id);
		$tm->status = 0;
		$tm->save();
		return Redirect::to('admin/testimony')->with('testimony','Data has been disactivated'); 
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$bm = TestimonyModel::find($id);
		$bm->delete();
		return Redirect::to('admin/testimony')->with('testimony','Data has been deleted');
	}
}
