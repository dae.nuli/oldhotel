<?php

class Customers extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Customers');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$qr	= CustomersModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('full_name','LIKE',"%$cari%");
		}
		$qr = $qr->paginate($this->limit);
		
		$data['customers'] = $qr;
		return View::make('backend.customers.index',$data);
	}

	public function getModalDetailCustomers($id)
	{
		View::share('title','Customers');
		View::share('path','Detail');

		$data['customers'] = CustomersModel::find($id);
		return View::make('backend.customers.modal_detail_customers',$data);
	}
	
	// public function getCreate()
	// {
	// 	View::share('path','Create');
	// 	View::share('title','Customers');
	// 	return View::make('backend.customers.create');
	// }

	// public function postInsert()
	// {
	// 	$rules = array(
	// 		'name'         => 'required',
	// 		'email'        => 'required|unique:customers,email',
	// 		'phone_number' => 'required',
	// 		'address'      => 'required'
	// 		);
	// 	$valid = Validator::make(Input::all(),$rules);
	// 	if($valid->fails())
	// 	{
	// 		return Redirect::to('admin/customers/create')->withErrors($valid)->withInput();
	// 	}else{
	// 		$cm          = new CustomersModel;
	// 		$cm->name    = Input::get('name');
	// 		$cm->email   = Input::get('email');
	// 		$cm->phone   = Input::get('phone_number');
	// 		$cm->address = Input::get('address');
	// 		$cm->save();
	// 		return Redirect::to('admin/customers')->with('customers','Data has been added');
	// 	}
	// }

	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Customers');

		$data['customers'] = CustomersModel::find($id);
		return View::make('backend.customers.edit',$data);
	}

	public function postUpdate($id)
	{
		$rules = array(
			'title'     => 'required',
			'name'      => 'required',
			'email'     => 'unique:customers,email,'.$id,
			'phone'     => 'required',
			'id_card'   => 'required',
			'card_type' => 'required',
			'address'   => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/customers/edit/'.$id)->withErrors($valid);
		}else{
			$cm                 = CustomersModel::find($id);
			$cm->title          = Input::get('title');
			$cm->full_name      = Input::get('name');
			$cm->email          = Input::get('email');
			$cm->phone          = Input::get('phone');
			$cm->id_card_number = Input::get('id_card');
			$cm->card_type      = Input::get('card_type');
			$cm->address        = Input::get('address');
			$cm->save();
			return Redirect::to('admin/customers')->with('customers','Data has been updated'); 
		}
	}

	public function getDelete($id)
	{
		$cm = CustomersModel::find($id);
		$tm = TransactionsModel::where('id_customers',$id)->count();
		if($tm > 0){
			return Redirect::to('admin/customers')->with('customers_alert','Data is used');
		}else{
			$cm->delete();
			return Redirect::to('admin/customers')->with('customers','Data has been deleted');
		}
	}
}