<?php

class Report extends BaseController {
	public $limit  = 10;
	public $folder = 'backend.report';
	public $uri    = 'admin/report';
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Report');
		View::share('path','Index');
		$data['limit']  = $this->limit;
		$OM = TransactionsModel::where('payment_status',1)->orderBy('created_at','asc');
		if(Session::has('R_from_date') && Session::has('R_to_date')){
			$OM = $OM->whereBetween('created_at',array(Session::get('R_from_date').' 00:00:00',Session::get('R_to_date').' 23:59:59'));
		}
		$data['report']     = $OM->paginate($this->limit);
		$data['grandTotal'] = Helper::ReportGrandTotal(Session::get('R_from_date'),Session::get('R_to_date'));

		
		$SessionYear       = Session::get('hotel_year') ? Session::get('hotel_year') : date('Y');
		$month             = ($SessionYear==date('Y'))?self::MonthName(date('n')):array('January','February','March','April','May','June','July','August','September','October','November','December');
		$data['month']     = '"'. implode('","', $month) .'"';
		$data['year']      = ($SessionYear) ? $SessionYear : date('Y');
		
		$data['chart']     = ($SessionYear==date('Y'))?implode(",", self::ChartItemSalesOfNow($data['year'])):implode(",", self::ChartItemSales($data['year']));
		$data['yearRange'] = range(2014,date('Y'));
		
		$data['url']       = (Session::get('show_chart'))?'admin/report/hide':'admin/report/display';
		$data['is_chart']  = (Session::get('show_chart'))?Session::get('show_chart'):NULL;
		$data['chartText'] = (Session::get('show_chart'))?'Hide':'Show';
		return View::make($this->folder.'.index',$data);
	}

	public function getDisplay()
	{
		Session::put('show_chart',true);
		return Redirect::to($this->uri);
	}

	public function getHide()
	{
		Session::forget('show_chart');
		return Redirect::to($this->uri);
	}
	
	public function postIndex()
	{
		$sales = Input::get('sales');
		if($sales){
			Session::put('hotel_year',$sales);
		}
		return Redirect::to($this->uri);
	}
	public function getDetail($id)
	{
		View::share('title','Report');
		View::share('path','Detail');
		if(!empty($id)){
			$tm  = TransactionsModel::find($id);
			$ctm = CafeTransactionModel::where('transaction_id',$id)->first();
			if(!empty($tm)){
				$data['transaction']  = $tm;
				$data['customers']    = CustomersModel::find($tm->id_customers);
				$data['detailRoom']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',1)->get();
				$data['detailAdds']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',0)->get();
				if($ctm){
					$data['cafeTransaction'] = CafeTransactionModel::find($ctm->id);
					$data['detailCafe']      = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
				}
				$data['payments']     = PaymentModel::where('id_transactions',$id)->get();
				$data['totalPayment'] = Helper::TotalPayment($id);
				return View::make('backend.report.detail',$data);
			}
		}
	}

	public function getPrint($id)
	{
		$tm  = TransactionsModel::find($id);
		$ctm = CafeTransactionModel::where('transaction_id',$id)->first();
		$data['transaction']  = $tm;
		$data['customers']    = CustomersModel::find($tm->id_customers);
		$data['staff']        = UserModel::find($this->admin['id']);
		$data['detail']       = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->get();
		if($ctm){
			$data['cafeTransaction'] = CafeTransactionModel::find($ctm->id);
			$data['detailCafe']      = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
		}
		$data['totalPayment'] = Helper::TotalPayment($id);
		return View::make('backend.report.print',$data);
	}

	public function getSummary()
	{
		// View::share('title','Report');
		// View::share('path','Index');
		// $OM = TransactionsModel::where('payment_status',1)->orderBy('created_at','asc');
		// if(Session::has('R_from_date') && Session::has('R_to_date')){
		// 	$OM = $OM->whereBetween('created_at',array(Session::get('R_from_date').' 00:00:00',Session::get('R_to_date').' 23:59:59'));
		// }
		// $data['start'] = Session::get('R_from_date');
		// $data['end'] = Session::get('R_to_date');
		// $data['report']     = $OM->get();
		// $data['grandTotal'] = Helper::ReportGrandTotal(Session::get('R_from_date'),Session::get('R_to_date'));
		// // return View::make('backend.report.print_summary',$data);
		// $print = View::make('backend.report.print_summary',$data);
	 //    return PDF::load($print, 'A4', 'portrait')->show();

		View::share('title','Report');
		View::share('path','Index');
		$OM = TransactionsModel::where('payment_status',1)->orderBy('created_at','asc');
		if(Session::has('R_from_date') && Session::has('R_to_date')){
			$OM = $OM->whereBetween('created_at',array(Session::get('R_from_date').' 00:00:00',Session::get('R_to_date').' 23:59:59'));
		}
		$data['start'] = Session::get('R_from_date');
		$data['end'] = Session::get('R_to_date');
		$data['report']     = $OM->get();
		$data['grandTotal'] = Helper::ReportGrandTotal(Session::get('R_from_date'),Session::get('R_to_date'));
		return View::make('backend.report.print_summary',$data);
	}

	public function getFilter()
	{
		$R_date_filter = Input::get('R_date_filter');
		if(!empty($R_date_filter)){
			$dates       = explode('/', $R_date_filter);
			$from        = date('Y-m-d',strtotime($dates[0]));
			$to          = date('Y-m-d',strtotime($dates[1]));
			$date1       = abs(strtotime($from));
			$date2       = abs(strtotime($to));
			
			if($date1 != '25200' && $date2 != '25200'){
				Session::put('R_from_date',$from);
				Session::put('R_to_date',$to);
				Session::put('R_date_filter',$R_date_filter);
			}else{
				Session::forget('R_from_date');
				Session::forget('R_to_date');
				Session::forget('R_date_filter');
			}
		}else{
			Session::forget('R_from_date');
			Session::forget('R_to_date');
			Session::forget('R_date_filter');
		}
		return Redirect::to('admin/report');
	}

	public function ChartItemSales($year)
	{
		$vm = DB::table('transactions')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('year(created_at) = '.$year)
             ->get();
             	if(count($vm) > 0){
			        $mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;

				    foreach ($vm as $row) {
				    	if($row->month == 1){
					        $mon['jan']++;
				    	}elseif($row->month == 2){
					        $mon['feb']++;
				    	}elseif($row->month == 3){
					        $mon['mar']++;
				    	}elseif($row->month == 4){
					        $mon['apr']++;
				    	}elseif($row->month == 5){
					        $mon['mei']++;
				    	}elseif($row->month == 6){
					        $mon['jun']++;
				    	}elseif($row->month == 7){
					        $mon['jul']++;
				    	}elseif($row->month == 8){
					        $mon['agu']++;
				    	}elseif($row->month == 9){
					        $mon['sep']++;
				    	}elseif($row->month == 10){
					        $mon['oct']++;
				    	}elseif($row->month == 11){
					        $mon['nov']++;
				    	}elseif($row->month == 12){
					        $mon['dec']++;
				    	}
				    }
				    return $mon;
				}else{
					$mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;
			        return $mon;
				}
	}

	public function MonthName($month)
	{
		for ($i=1; $i <=$month ; $i++) { 
			switch ($i) {
				case 1:
					$Mth[] = 'January';
				break;
				
				case 2:
					$Mth[] = 'February';
				break;
				
				case 3:
					$Mth[] = 'March';
				break;
				
				case 4:
					$Mth[] = 'April';
				break;
				
				case 5:
					$Mth[] = 'May';
				break;
				
				case 6:
					$Mth[] = 'June';
				break;
				
				case 7:
					$Mth[] = 'July';
				break;
				
				case 8:
					$Mth[] = 'August';
				break;
				
				case 9:
					$Mth[] = 'September';
				break;
				
				case 10:
					$Mth[] = 'October';
				break;
				
				case 11:
					$Mth[] = 'November';
				break;
				
				case 12:
					$Mth[] = 'December';
				break;
			}
		}
		return $Mth;
	}

	public function ChartItemSalesOfNow($year)
	{
		$months = date('n');
		$vm = DB::table('transactions')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('month(created_at) <= '.$months)
             ->whereRaw('year(created_at) = '.$year)
             ->get();
         		for ($i=1; $i <= $months; $i++) { 
         			$mon[$i] = 0;
         		}
             	if(count($vm) > 0){
				    foreach ($vm as $key => $row) {
				    	
				    	if($row->month == 1){
					        $mon[1]++;
				    	}elseif($row->month == 2){
					        $mon[2]++;
				    	}elseif($row->month == 3){
					        $mon[3]++;
				    	}elseif($row->month == 4){
					        $mon[4]++;
				    	}elseif($row->month == 5){
					        $mon[5]++;
				    	}elseif($row->month == 6){
					        $mon[6]++;
				    	}elseif($row->month == 7){
					        $mon[7]++;
				    	}elseif($row->month == 8){
					        $mon[8]++;
				    	}elseif($row->month == 9){
					        $mon[9]++;
				    	}elseif($row->month == 10){
					        $mon[10]++;
				    	}elseif($row->month == 11){
					        $mon[11]++;
				    	}elseif($row->month == 12){
					        $mon[12]++;
				    	}
				    }
				}
			    return $mon;
	}
}