<?php

class RoomClass extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Class');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= RoomModel::orderBy('id','desc')->where('id_parent',null);
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['room'] = $qr->paginate($this->limit);
		return View::make('backend.room.class_index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Class');
		View::share('path','Create');
		$data['facility'] = FacilityModel::all();
		return View::make('backend.room.class_create',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'     => 'required',
			'price'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/roomClass/create')->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');
				$facil = Input::get('facility');

				$cm                = new RoomModel;
				$cm->created_by    = $this->admin['id'];
				$cm->name          = Input::get('name');
				$cm->price         = str_replace($filter, "", $uang);
				$cm->facility      = (!empty($facil)?implode(',', $facil):'');
				$cm->status        = null;
				$cm->kondisi_kamar = null;
				$cm->save();
				return Redirect::to('admin/roomClass')->with('room','Data has been added');
			}else{
				return Redirect::to('admin/roomClass')->with('room_alert','The price field is required.');
			}
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Class');
		View::share('path','Create');
		$data['room']     = RoomModel::find($id);
		$data['facility'] = FacilityModel::all();
		return View::make('backend.room.class_edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'  => 'required',
			'price' => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/roomClass/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');
				$facil = Input::get('facility');
				$cm             = RoomModel::find($id);
				$cm->updated_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang);
				$cm->facility   = (!empty($facil)?implode(',', $facil):'');
				// $cm->facility   = (!empty($facil)?json_encode($facil,JSON_FORCE_OBJECT):'');
				$cm->save();
				return Redirect::to('admin/roomClass')->with('room','Data has been added');
			}else{
				return Redirect::to('admin/roomClass')->with('room_alert','The price field is required.');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = RoomModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				$rm = RoomModel::withTrashed()->where('id_parent',$id)->count();
				if(!empty($rm)){
					return Redirect::to('admin/roomClass')->with('room_alert','The class is used');
				}else{
					$am->delete();
					return Redirect::to('admin/roomClass')->with('room','Data has been deleted');
				}
			}
		}
	}

}
