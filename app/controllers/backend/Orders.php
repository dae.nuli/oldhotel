<?php

class Orders extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Orders');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$data['orders'] = OrdersModel::orderBy('id','desc')->paginate($this->limit);
		return View::make('backend.orders.index',$data);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDetail($id)
	{
		View::share('title','Orders');
		View::share('path','Detail');

		$om  = OrdersModel::find($id);
		$data['orders'] = $om;
		$data['client'] = ClientsModel::find($om->id_client);
		$data['detail'] = OrdersDetailModel::orderBy('id','asc')->where('id_order',$id)->get();
		$data['products'] = ProductsModel::all();
		$data['payments'] = PaymentModel::where('id_order',$id)->get();
		$data['totalPayment'] = Helper::TotalPayment($id);
		return View::make('backend.orders.detail',$data);
	}

	public function postItem($id=null)
	{
		// $ODM = OrdersDetailModel::where('id_order',$id)->get();
		// 	foreach ($ODM as $key => $value) {
		// 		$total[] = ($value->qty*$value->item_price);
		// 	}
		// 	return array_sum($total);
		$inputs = Input::all();
		if($inputs['name']=='code_product'){
			$PM = ProductsModel::where('code',$inputs['value'])->first();

			$om                  = OrdersDetailModel::find($inputs['pk']);
			$om->code_product 	 = $inputs['value'];
			$om->item_price      = $PM->price;
			$om->save();

			$subtotal = ($om->qty*$PM->price);
			$ODM = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($ODM as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();
			$data = array('total'=>$OM->total,'item_price'=>$PM->price,'subtotal'=>$subtotal,'id'=>$om->id);
			return Response::json(array(
				'error' =>false,
				'data' => $data),
				200
			);

		}elseif($inputs['name']=='qty'){
			$om      = OrdersDetailModel::find($inputs['pk']);
			$om->qty = $inputs['value'];
			$om->save();

			$subtotal = ($om->qty*$om->item_price);

			$ODM = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($ODM as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();

			$data = array('total'=>$OM->total,'subtotal'=>$subtotal,'id'=>$om->id);
			return Response::json(array(
				'error' =>false,
				'data' => $data),
				200
			);
		}elseif($inputs['name']=='item_price'){
			$om      = OrdersDetailModel::find($inputs['pk']);
			$om->item_price = $inputs['value'];
			$om->save();

			$subtotal = ($om->qty*$om->item_price);

			$ODM = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($ODM as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();

			$data = array('total'=>$OM->total,'item_price' => $om->item_price,'subtotal'=>$subtotal,'id'=>$om->id);
			return Response::json(array(
				'error' =>false,
				'data' => $data),
				200
			);
		}
	}

	public function getData($id)
	{
		View::share('title','Orders');
		View::share('path','Data');

		$data['data'] = OrdersDataModel::where('orders_detail_id',$id)->first();
		$data['detail'] = OrdersDetailModel::find($id);
		$data['religion'] = ReligionModel::all();
		return View::make('backend.orders.data',$data);
	}

	public function postFindPriceByProduct()
	{
		$code = Input::get('id_product');
		if(!empty($code)){
			$pm = ProductsModel::find($code);
			return $pm->price;
		}
	}

	public function postAddItem($id)
	{
		$rules = array(
			'product' => 'required',
			'price'   => 'required',
			'qty'     => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$id)->with('warning','The item fields are required');
		}else{
			$ODM = new OrdersDetailModel;
			$ODM->id_order     = $id;
			$ODM->code_product = Helper::findCodebyID(Input::get('product'));
			$ODM->qty          = Input::get('qty');
			$ODM->item_price   = Input::get('price');
			$ODM->save();

			$OD = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($OD as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();
			return Redirect::to('admin/orders/detail/'.$id)->with('success','Item has been added');
		}
	}
	
	public function getModalAddPayment($id)
	{
		$data['products'] = ProductsModel::all();
		$data['orders']   = $id;
		$data['method']   = PaymentMethodModel::all();
		$total_order = OrdersModel::find($id)->total;
		$pay = OrdersPaymentModel::where('id_order',$id)->count();
		if($pay){
			$OPM = OrdersPaymentModel::where('id_order',$id)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$data['amount'] = $total_order - array_sum($total);
		}else{
			$data['amount'] = $total_order; 
		}
		return View::make('backend.orders.modal_add_payment',$data);
	}

	public function postAddPayment($id)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$id)->with('warning','The payment fields are required');
		}else{
			$note   = Input::get('note');
			$filter = array(',','.');
			$PM = new OrdersPaymentModel;
			$PM->id_order       = $id;
			$PM->amount         = str_replace($filter, "", Input::get('amount')); 
			$PM->payment_date   = Input::get('date');
			$PM->payment_method = Input::get('method');
			$PM->note           = ($note)?$note:'';
			$PM->save();

			Helper::CheckStatusPayment($id);
			return Redirect::to('admin/orders/detail/'.$id)->with('success','Payment has been added');
		}
	}

	public function postExistEmail()
	{
		if(Request::ajax()){
			$email = Input::get('email');
			$cm = ClientsModel::where('email',$email)->count();
			if($cm > 0){
				return 1;
			}else{
				return 0;
			}
		}
	}

	public function postAddOrder()
	{
		$id_client = Input::get('id_client');

		if(!empty($id_client)){
			$CM = ClientsModel::find($id_client);
			if(!empty($CM)){
				$om     = OrdersModel::orderBy('code_order','desc')->first();
				$prefix = "AG";
				if(empty($om)){
					$OrderCode = $prefix."00001";
				}else{
					$number = (int)substr($om->code_order, strlen($prefix), 5);
					$number++;
					$OrderCode = $prefix.sprintf("%05d", $number);
				}
				
				$om = new OrdersModel;
				$om->code_order = $OrderCode;
				$om->id_client  = $CM->id;
				$om->total      = 0;
				$om->save();
				return Redirect::to('admin/orders')->with('success','Order has been added');
			}else{
				return Redirect::to('admin/orders')->with('warning','Order fail to add');
			}
		}else{
			$rules = array(
				'name'    => 'required',
				'email'   => 'required|unique:clients,email',
				'phone'   => 'required',
				'address' => 'required'
				);
			$valid = Validator::make(Input::all(),$rules);
			if($valid->fails())
			{
				return Redirect::to('admin/orders')->with('warning','The fields are required');
			}else{
				$cm = new ClientsModel;
				$cm->name    = Input::get('name');
				$cm->email   = Input::get('email');
				$cm->address = Input::get('address');
				$cm->phone   = Input::get('phone');
				$cm->save();

				$om     = OrdersModel::orderBy('code_order','desc')->first();
				$prefix = "AG";
				if(empty($om)){
					$OrderCode = $prefix."00001";
				}else{
					$number = (int)substr($om->code_order, strlen($prefix), 5);
					$number++;
					$OrderCode = $prefix.sprintf("%05d", $number);
				}
				
				$om = new OrdersModel;
				$om->code_order = $OrderCode;
				$om->id_client  = $cm->id;
				$om->total      = 0;
				$om->save();
				return Redirect::to('admin/orders')->with('success','Order has been added');
			}
		}
	}
	
	public function getModalAddOrders()
	{
		$data['names'] = ClientsModel::all();
		return View::make('backend.orders.modal_add_orders',$data);
	}

	public function getModalAddItem($id)
	{
		$data['products'] = ProductsModel::all();
		$data['orders'] = $id;
		return View::make('backend.orders.modal_add_item',$data);
	}

	public function getModalClientInfo($id)
	{
		$data['client'] = ClientsModel::find($id);
		return View::make('backend.orders.modal_client_info',$data);
	}

	public function getModalEditPayment($idorder,$idpayment)
	{
		$data['method'] = PaymentMethodModel::all();
		$data['orders'] = $idorder;
		$data['payment'] = OrdersPaymentModel::find($idpayment);
		return View::make('backend.orders.modal_edit_payment',$data);
	}

	public function postEditPayment($idorder,$idpayment)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$idorder);
		}else{
			$note   = Input::get('note');
			$filter = array(',','.');
			$ODM    = OrdersPaymentModel::find($idpayment);
			$ODM->id_order       = $idorder;
			$ODM->amount         = str_replace($filter, "", Input::get('amount'));
			$ODM->payment_date   = Input::get('date');
			$ODM->payment_method = Input::get('method');
			$ODM->note           = ($note)?$note:'';
			$ODM->save();
			Helper::CheckStatusPayment($idorder);

			return Redirect::to('admin/orders/detail/'.$idorder)->with('success','Payment has been updated');
		}
	}
	public function getDeletePayment($idpayment,$idorder)
	{
		OrdersPaymentModel::find($idpayment)->delete();
		Helper::CheckStatusPayment($idorder);
		return Redirect::to('admin/orders/detail/'.$idorder)->with('success','Payment has been deleted');
	}
	public function postData($id=null)
	{
		$rules = array(
			'nama_mempelai_wanita'      => 'required',
			'nama_ayah_mempelai_wanita' => 'required',
			'nama_ibu_mempelai_wanita'  => 'required',
			'alamat_mempelai_wanita'    => 'required',
			'nama_mempelai_pria'        => 'required',
			'nama_ayah_mempelai_pria'   => 'required',
			'nama_ibu_mempelai_pria'    => 'required',
			'alamat_mempelai_pria'      => 'required',
			'akad_tanggal'              => 'required',
			'akad_jam'                  => 'required',
			'akad_lokasi'               => 'required',
			'resepsi_tanggal'           => 'required',
			'resepsi_jam'               => 'required',
			'resepsi_lokasi'            => 'required',
			'panggilan_wanita'          => 'required',
			'panggilan_pria'            => 'required',
			'agama'                     => 'required',
			'turut_mengundang'          => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		$ID_ORDER = Input::get('id_orders');
		$ID_ORDER_DETAIL = Input::get('orders_detail_id');
		if($valid->fails())
		{
			return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('errors','The fields are required');
		}else{
			if(!empty($id)){
				$ODM = OrdersDataModel::find($id);
				$ODM->woman_name           = Input::get('nama_mempelai_wanita');
				$ODM->woman_father         = Input::get('nama_ayah_mempelai_wanita');
				$ODM->woman_mother         = Input::get('nama_ibu_mempelai_wanita');
				$ODM->woman_address        = Input::get('alamat_mempelai_wanita');
				$ODM->man_name             = Input::get('nama_mempelai_pria');
				$ODM->man_father           = Input::get('nama_ayah_mempelai_pria');
				$ODM->man_mother           = Input::get('nama_ibu_mempelai_pria');
				$ODM->man_address          = Input::get('alamat_mempelai_pria');
				$ODM->akad_date            = Input::get('akad_tanggal');
				$ODM->akad_time            = Input::get('akad_jam');
				$ODM->akad_location        = Input::get('akad_lokasi');
				$ODM->resepsi_date         = Input::get('resepsi_tanggal');
				$ODM->resepsi_time         = Input::get('resepsi_jam');
				$ODM->resepsi_location     = Input::get('resepsi_lokasi');
				$ODM->nickname_woman       = Input::get('panggilan_wanita');
				$ODM->nickname_man         = Input::get('panggilan_pria');
				$ODM->id_religion          = Input::get('agama');
				$ODM->important_invitation = Input::get('turut_mengundang');
				$ODM->save();
				return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('success','Data has been updated');
			}else{
				$ODM = new OrdersDataModel;
				$ODM->orders_detail_id     = Input::get('orders_detail_id');
				$ODM->woman_name           = Input::get('nama_mempelai_wanita');
				$ODM->woman_father         = Input::get('nama_ayah_mempelai_wanita');
				$ODM->woman_mother         = Input::get('nama_ibu_mempelai_wanita');
				$ODM->woman_address        = Input::get('alamat_mempelai_wanita');
				$ODM->man_name             = Input::get('nama_mempelai_pria');
				$ODM->man_father           = Input::get('nama_ayah_mempelai_pria');
				$ODM->man_mother           = Input::get('nama_ibu_mempelai_pria');
				$ODM->man_address          = Input::get('alamat_mempelai_pria');
				$ODM->akad_date            = Input::get('akad_tanggal');
				$ODM->akad_time            = Input::get('akad_jam');
				$ODM->akad_location        = Input::get('akad_lokasi');
				$ODM->resepsi_date         = Input::get('resepsi_tanggal');
				$ODM->resepsi_time         = Input::get('resepsi_jam');
				$ODM->resepsi_location     = Input::get('resepsi_lokasi');
				$ODM->nickname_woman       = Input::get('panggilan_wanita');
				$ODM->nickname_man         = Input::get('panggilan_pria');
				$ODM->id_religion          = Input::get('agama');
				$ODM->important_invitation = Input::get('turut_mengundang');
				$ODM->save();
				return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('success','Data has been updated');
			}
		}
	}

	public function postDetail($id)
	{
		$rules = array(
			'start_date'  => 'required',
			'finish_date' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$id)->withErrors($valid);
		}else{
			$note              = Input::get('note');
			$om                = OrdersModel::find($id);
			// $om->order_status  = 1;
			$om->start_create  = Input::get('start_date');
			$om->finish_create = Input::get('finish_date');
			$om->note          = ($note)?$note:'';
			$om->save();
			return Redirect::to('admin/orders/detail/'.$id)->with('success','Data has been updated');
		}
	}

	public function getEdit($id)
	{
		View::share('title','Orders');
		View::share('path','Edit');

		$om  = OrdersModel::where('id',$id)->first();
		$cm  = ClientsModel::find($om->id_client);
		$odm = OrdersDetailModel::where('id_order',$id)->get();
		$data['orders'] = $om;
		$data['client'] = $cm;
		$data['detail'] = $odm;
		return View::make('backend.orders.edit',$data);
	}

	public function getPrint($IdOrder)
	{
		$om  = OrdersModel::find($IdOrder);
		$data['orders'] = $om;
		$data['client'] = ClientsModel::find($om->id_client);
		$data['detail'] = OrdersDetailModel::orderBy('id','asc')->where('id_order',$IdOrder)->get();
		$data['products'] = ProductsModel::all();
		$data['payments'] = PaymentModel::where('id_order',$IdOrder)->get();
		$data['totalPayment'] = Helper::TotalPayment($IdOrder);
		$print = View::make('backend.orders.print',$data);
	    return PDF::load($print, 'A4', 'portrait')->show();
	}

	public function getDataPrint($id)
	{
		$data['mempelai'] = OrdersDataModel::find($id);
		$ODM              = OrdersDetailModel::find($data['mempelai']->orders_detail_id);
		$om               = OrdersModel::find($ODM->id_order);
		$data['orders']   = $om;
		$data['client']   = ClientsModel::find($om->id_client);
		$print     = View::make('backend.orders.print_data',$data);
	    return PDF::load($print, 'A4', 'portrait')->show();
	}

	public function getDeleteItem($id)
	{
		$ODM = OrdersDetailModel::find($id);
		$IDO = $ODM->id_order;
		$ODM->delete();
		
		$ModData = OrdersDataModel::where('orders_detail_id',$id)->delete();
		$ODML = OrdersDetailModel::where('id_order',$IDO)->get();
		foreach ($ODML as $key => $value) {
			$total[] = ($value->qty*$value->item_price);
		}
		if(!empty($total)){
			$tot = array_sum($total);
		}else{
			$tot = 0;
		}
		$OM = OrdersModel::find($IDO);
		$OM->total = $tot;
		$OM->save();

		return Redirect::to('admin/orders/detail/'.$IDO);
	}

	public function getDeleteOrder($id)
	{
		OrdersModel::find($id)->delete();
		$ODM = OrdersDetailModel::where('id_order',$id)->get();
		if(count($ODM) > 0){
			foreach ($ODM as $key => $value) {
				OrdersDataModel::where('orders_detail_id',$value->id)->delete();
			}
			OrdersDetailModel::where('id_order',$id)->delete();
		}
		OrdersPaymentModel::where('id_order',$id)->delete();
		return Redirect::to('admin/orders')->with('success','Data has been deleted');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'category'     => 'required',
			'sub_category' => 'required',
			'price'        => 'required|numeric',
			'content'      => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$picture            = Input::get('picture');
			$cm                 = ProductsModel::find($id);
			
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->id_subcategory = Input::get('sub_category');
				$cm->code = 1;
				$cm->price          = Input::get('price');
				$cm->note           = Input::get('content');
				$cm->picture        = ($picture?$picture:'');
				$cm->save();
				return Redirect::to('admin/products')->with('products','Data has been added');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = ProductsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/products')->with('products','Data has been deleted');
			}
		}
	}

}
