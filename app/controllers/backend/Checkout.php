<?php

class Checkout extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Check Out');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= TransactionsModel::orderBy('id','desc')->where('guest_status',2);
		// $qr	= TransactionsModel::orderBy('id','desc')->where('book_via','web');
		if($cari){
			$sr = Helper::searchRoom($cari,2);
			if($sr){
				$qr = $qr->where('code','LIKE',"%$cari%")->orWhereIn('id',$sr);
			}else{
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
		}
		$qr = $qr->paginate($this->limit);
		$data['booking'] = $qr;
		return View::make('backend.checkout.index',$data);
	}
 
	public function getPrint($id)
	{
		$tm  = TransactionsModel::find($id);
		$ctm = CafeTransactionModel::where('transaction_id',$id)->first();
		$data['transaction']  = $tm;
		$data['customers']    = CustomersModel::find($tm->id_customers);
		$data['staff']        = UserModel::find($this->admin['id']);
		$data['detail']       = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->get();
		// $data['detailCafe']   = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
		$data['totalPayment'] = Helper::TotalPayment($id);
		return View::make('backend.checkout.print',$data);
	} 

	public function getDetail($id)
	{
		View::share('title','Check Out');
		View::share('path','Detail');
		if(!empty($id)){
			$tm = TransactionsModel::find($id);
			// $ctm = CafeTransactionModel::where('transaction_id',$id)->first();
			if(!empty($tm)){
				$data['transaction']  = $tm;
				$data['customers']    = CustomersModel::find($tm->id_customers);
				$data['detailRoom']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',1)->get();
				$data['detailAdds']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',0)->get();
				// if($ctm){
				// 	$data['cafeTransaction'] = CafeTransactionModel::find($ctm->id);
				// 	$data['detailCafe']      = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
				// }
				$data['payments']     = PaymentModel::where('id_transactions',$id)->get();
				$data['totalPayment'] = Helper::TotalPayment($id);
				return View::make('backend.checkout.detail',$data);
			}
		}
	}

}
