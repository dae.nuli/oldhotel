<?php

class Banner extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Banner');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$qr	= BannerModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('title','LIKE',"%$cari%");
		}
		$qr = $qr->paginate($this->limit);

		$data['banner'] = $qr;
		return View::make('backend.banner.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Banner');
		View::share('path','Create');
		return View::make('backend.banner.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'title'   => 'required',
			'picture' => 'required',
			'content' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/banner/create')->withErrors($valid)->withInput();
		}else{
			$cm          = new BannerModel;
			$cm->title   = Input::get('title');
			$cm->content = Input::get('content');
			$cm->picture = Input::get('picture');
			$cm->save();
			return Redirect::to('admin/banner')->with('banner','Data has been added');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Banner');
		View::share('path','Edit');

		$data['banner'] = BannerModel::find($id);
		return View::make('backend.banner.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'title'   => 'required',
			'picture' => 'required',
			'content' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/banner/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$bm          = BannerModel::find($id);
			$bm->title   = Input::get('title');
			$bm->content = Input::get('content');
			$bm->picture = Input::get('picture');
			$bm->save();
			return Redirect::to('admin/banner')->with('banner','Data has been added');
		}
	}

	public function getActive($id)
	{
		$bm = BannerModel::find($id);
		$bm->status = 1;
		$bm->save();
		return Redirect::to('admin/banner')->with('banner','Data has been activated'); 
	}

	public function getNoactive($id)
	{
		$bm = BannerModel::find($id);
		$bm->status = 0;
		$bm->save();
		return Redirect::to('admin/banner')->with('banner','Data has been disactivated'); 
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$bm = BannerModel::find($id);
		if(!empty($bm)){
			$bm->delete();
			return Redirect::to('admin/banner')->with('banner','Data has been deleted');
		}
	}
}
