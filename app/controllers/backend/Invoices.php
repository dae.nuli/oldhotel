<?php

class Invoices extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Invoices');
		View::share('path','Index');
		$data['limit']    = $this->limit;
		$data['invoices'] = OrdersModel::orderBy('id','desc')->where('order_status','!=',0)->paginate($this->limit);
		return View::make('backend.invoices.index',$data);
	}

	public function getCreate()
	{
		View::share('title','Create');
		View::share('path','Index');
		$data['limit']    = $this->limit;
		$data['invoices'] = OrdersModel::orderBy('id','desc')->where('order_status','!=',0)->paginate($this->limit);
		return View::make('backend.invoices.create',$data);	
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDetail($id)
	{
		View::share('title','Invoices');
		View::share('path','Detail');

		$om  = OrdersModel::where('id',$id)->where('order_status','!=',0)->first();
		$data['orders'] = $om;
		$data['client'] = ClientsModel::find($om->id_client);
		$data['detail'] = OrdersDetailModel::where('id_order',$id)->get();
		$data['products'] = ProductsModel::all();
		return View::make('backend.invoices.detail',$data);
	}

	public function postItem($id=null)
	{
		// $ODM = OrdersDetailModel::where('id_order',$id)->get();
		// 	foreach ($ODM as $key => $value) {
		// 		$total[] = ($value->qty*$value->item_price);
		// 	}
		// 	return array_sum($total);
		$inputs = Input::all();
		if($inputs['name']=='code_product'){
			$PM = ProductsModel::where('code',$inputs['value'])->first();

			$om                  = OrdersDetailModel::find($inputs['pk']);
			$om->code_product 	 = $inputs['value'];
			$om->item_price      = $PM->price;
			$om->save();

			$subtotal = ($om->qty*$PM->price);
			$ODM = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($ODM as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();
			$data = array('total'=>$OM->total,'item_price'=>$PM->price,'subtotal'=>$subtotal,'id'=>$om->id);
			return Response::json(array(
				'error' =>false,
				'data' => $data),
				200
			);

		}elseif($inputs['name']=='qty'){
			$om      = OrdersDetailModel::find($inputs['pk']);
			$om->qty = $inputs['value'];
			$om->save();

			$subtotal = ($om->qty*$om->item_price);

			$ODM = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($ODM as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();

			$data = array('total'=>$OM->total,'subtotal'=>$subtotal,'id'=>$om->id);
			return Response::json(array(
				'error' =>false,
				'data' => $data),
				200
			);
		}
	}

	public function getData($id)
	{
		View::share('title','Orders');
		View::share('path','Data');

		$data['data'] = OrdersDataModel::where('orders_detail_id',$id)->first();
		$data['detail'] = OrdersDetailModel::find($id);
		$data['religion'] = ReligionModel::all();
		return View::make('backend.orders.data',$data);
	}

	public function postFindPriceByProduct()
	{
		$code = Input::get('id_product');
		if(!empty($code)){
			$pm = ProductsModel::find($code);
			return $pm->price;
		}
	}

	public function postAddItem($id)
	{
		$rules = array(
			'product' => 'required',
			'price'   => 'required',
			'qty'     => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$id);
		}else{
			$ODM = new OrdersDetailModel;
			$ODM->id_order     = $id;
			$ODM->code_product = Helper::findCodebyID(Input::get('product'));
			$ODM->qty          = Input::get('qty');
			$ODM->item_price   = Input::get('price');
			$ODM->save();

			$OD = OrdersDetailModel::where('id_order',$id)->get();
			foreach ($OD as $key => $value) {
				$total[] = ($value->qty*$value->item_price);
			}
			$OM = OrdersModel::find($id);
			$OM->total = array_sum($total);
			$OM->save();
			return Redirect::to('admin/orders/detail/'.$id)->with('success','Item has been added');
		}
	}

	public function getModalAddItem($id)
	{
		$data['products'] = ProductsModel::all();
		$data['orders'] = $id;
		return View::make('backend.orders.modal_add_item',$data);
	}

	public function postData($id=null)
	{
		$rules = array(
			'nama_mempelai_wanita'      => 'required',
			'nama_ayah_mempelai_wanita' => 'required',
			'nama_ibu_mempelai_wanita'  => 'required',
			'alamat_mempelai_wanita'    => 'required',
			'nama_mempelai_pria'        => 'required',
			'nama_ayah_mempelai_pria'   => 'required',
			'nama_ibu_mempelai_pria'    => 'required',
			'alamat_mempelai_pria'      => 'required',
			'akad_tanggal'              => 'required',
			'akad_jam'                  => 'required',
			'akad_lokasi'               => 'required',
			'resepsi_tanggal'           => 'required',
			'resepsi_jam'               => 'required',
			'resepsi_lokasi'            => 'required',
			'panggilan_wanita'          => 'required',
			'panggilan_pria'            => 'required',
			'agama'                     => 'required',
			'turut_mengundang'          => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		$ID_ORDER = Input::get('id_orders');
		$ID_ORDER_DETAIL = Input::get('orders_detail_id');
		if($valid->fails())
		{
			return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('errors','The fields are required');
		}else{
			if(!empty($id)){
				$ODM = OrdersDataModel::find($id);
				$ODM->woman_name           = Input::get('nama_mempelai_wanita');
				$ODM->woman_father         = Input::get('nama_ayah_mempelai_wanita');
				$ODM->woman_mother         = Input::get('nama_ibu_mempelai_wanita');
				$ODM->woman_address        = Input::get('alamat_mempelai_wanita');
				$ODM->man_name             = Input::get('nama_mempelai_pria');
				$ODM->man_father           = Input::get('nama_ayah_mempelai_pria');
				$ODM->man_mother           = Input::get('nama_ibu_mempelai_pria');
				$ODM->man_address          = Input::get('alamat_mempelai_pria');
				$ODM->akad_date            = Input::get('akad_tanggal');
				$ODM->akad_time            = Input::get('akad_jam');
				$ODM->akad_location        = Input::get('akad_lokasi');
				$ODM->resepsi_date         = Input::get('resepsi_tanggal');
				$ODM->resepsi_time         = Input::get('resepsi_jam');
				$ODM->resepsi_location     = Input::get('resepsi_lokasi');
				$ODM->nickname_woman       = Input::get('panggilan_wanita');
				$ODM->nickname_man         = Input::get('panggilan_pria');
				$ODM->id_religion          = Input::get('agama');
				$ODM->important_invitation = Input::get('turut_mengundang');
				$ODM->save();
				return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('success','Data has been updated');
			}else{
				$ODM = new OrdersDataModel;
				$ODM->orders_detail_id     = Input::get('orders_detail_id');
				$ODM->woman_name           = Input::get('nama_mempelai_wanita');
				$ODM->woman_father         = Input::get('nama_ayah_mempelai_wanita');
				$ODM->woman_mother         = Input::get('nama_ibu_mempelai_wanita');
				$ODM->woman_address        = Input::get('alamat_mempelai_wanita');
				$ODM->man_name             = Input::get('nama_mempelai_pria');
				$ODM->man_father           = Input::get('nama_ayah_mempelai_pria');
				$ODM->man_mother           = Input::get('nama_ibu_mempelai_pria');
				$ODM->man_address          = Input::get('alamat_mempelai_pria');
				$ODM->akad_date            = Input::get('akad_tanggal');
				$ODM->akad_time            = Input::get('akad_jam');
				$ODM->akad_location        = Input::get('akad_lokasi');
				$ODM->resepsi_date         = Input::get('resepsi_tanggal');
				$ODM->resepsi_time         = Input::get('resepsi_jam');
				$ODM->resepsi_location     = Input::get('resepsi_lokasi');
				$ODM->nickname_woman       = Input::get('panggilan_wanita');
				$ODM->nickname_man         = Input::get('panggilan_pria');
				$ODM->id_religion          = Input::get('agama');
				$ODM->important_invitation = Input::get('turut_mengundang');
				$ODM->save();
				return Redirect::to('admin/orders/data/'.$ID_ORDER_DETAIL)->with('success','Data has been updated');
			}
		}
	}

	public function postDetail($id)
	{
		$rules = array(
			'start_date'  => 'required',
			'finish_date' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/orders/detail/'.$id)->withErrors($valid);
		}else{
			$note              = Input::get('note');
			$om                = OrdersModel::find($id);
			$om->order_status  = 1;
			$om->start_create  = Input::get('start_date');
			$om->finish_create = Input::get('finish_date');
			$om->note          = ($note)?$note:'';
			$om->save();
			return Redirect::to('admin/orders')->with('orders','Data has been processed, please check invoice to see processed order data');
		}
	}

	public function getEdit($id)
	{
		View::share('title','Orders');
		View::share('path','Edit');

		$om  = OrdersModel::where('id',$id)->where('order_status','0')->first();
		$cm  = ClientsModel::find($om->id_client);
		$odm = OrdersDetailModel::where('id_order',$id)->get();
		$data['orders'] = $om;
		$data['client'] = $cm;
		$data['detail'] = $odm;
		return View::make('backend.orders.edit',$data);
	}

	public function getDeleteItem($id)
	{
		$ODM = OrdersDetailModel::find($id);
		$IDO = $ODM->id_order;
		$ODM->delete();
		
		$ModData = OrdersDataModel::where('orders_detail_id',$id)->delete();
		$ODML = OrdersDetailModel::where('id_order',$IDO)->get();
		foreach ($ODML as $key => $value) {
			$total[] = ($value->qty*$value->item_price);
		}
		if(!empty($total)){
			$tot = array_sum($total);
		}else{
			$tot = 0;
		}
		$OM = OrdersModel::find($IDO);
		$OM->total = $tot;
		$OM->save();

		return Redirect::to('admin/orders/detail/'.$IDO);
	}

	public function getDeleteOrder($id)
	{
		OrdersModel::find($id)->delete();
		$ODM = OrdersDetailModel::where('id_order',$id)->get();
		if(!empty($ODM)){
			foreach ($ODM as $key => $value) {
				OrdersDataModel::where('orders_detail_id',$value->id)->delete();
			}
			OrdersDetailModel::where('id_order',$id)->delete();
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'category'     => 'required',
			'sub_category' => 'required',
			'price'        => 'required|numeric',
			'content'      => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$picture            = Input::get('picture');
			$cm                 = ProductsModel::find($id);
			
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->id_subcategory = Input::get('sub_category');
				$cm->code = 1;
				$cm->price          = Input::get('price');
				$cm->note           = Input::get('content');
				$cm->picture        = ($picture?$picture:'');
				$cm->save();
				return Redirect::to('admin/products')->with('products','Data has been added');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = ProductsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/products')->with('products','Data has been deleted');
			}
		}
	}

}
