<?php

class Booking extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit  = 10;
	public $folder = 'backend.booking';
	public $title  = 'Check In';
	public $uri    = 'admin/checkIn';
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title',$this->title);
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= TransactionsModel::orderBy('id','desc')->where('guest_status',1);
		// $qr	= TransactionsModel::orderBy('id','desc')->where('book_via','web');
		if($cari){
			$sr = Helper::searchRoom($cari,1);
			if($sr){
				$qr = $qr->where('code','LIKE',"%$cari%")->orWhereIn('id',$sr);
			}else{
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
		}
		$qr = $qr->paginate($this->limit);
		$data['booking'] = $qr;
		return View::make('backend.booking.index',$data);
	}

	public function getGoout($id)
	{
		if(!empty($id)){
			$tdm = TransactionsDetailModel::where('id_transactions',$id)->where('type',1)->get();
			foreach ($tdm as $key => $val) {
				Helper::setRoomStatusToEmpty($val->id_item);
				Helper::setRoomConditionToDirty($val->id_item);
			}
			$tm = TransactionsModel::find($id);
			$tm->guest_status = 2;
			$tm->save();
			return Redirect::to('admin/checkIn')->with('booking','Transaction number '.$tm->code.' has been moved to check out menu');
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Check In');
		View::share('path','Create');
		return View::make('backend.booking.create');
	}

	public function getModalChooseRoom()
	{
		$data['rooms'] = RoomModel::orderBy('id','asc')
						->where('id_parent','!=','null')
						->where('status',1)
						->where('kondisi_kamar',1)->get();
		return View::make('backend.booking.modal_choose_room',$data);
	}

	public function getModalChooseTambahan()
	{
		$data['tambahan'] = ExtraChargeModel::orderBy('id','desc')->get();
		return View::make('backend.booking.modal_choose_tambahan',$data);
	}

	public function postIndex()
	{
		$rules = array(
			'name'        => 'required',
			'title'       => 'required',
			'card_type'   => 'required',
			'id_card'     => 'required|numeric',
			'address'     => 'required',
			'phone'       => 'required',
			'email'       => 'email',
			'room'        => 'required',
			// 'additional'  => 'required',
			'checkin'     => 'required',
			'checkout'    => 'required'
			// 'max_payment' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/create')->withErrors($valid)->withInput();
		}else{

			$codes =	rand(111111,999999);
			// $codes =	Str::random(7);
			$nm    =	TransactionsModel::where('code',$codes)->count();
			while ($nm > 0) {
				$codes =	rand(111111,999999);
				// $codes =	Str::random(7);
				$nm    =	TransactionsModel::where('code',$codes)->count();
			}
// return Input::get('max_payment');
			$email   = Input::get('email');
			$address = Input::get('address');
			$cm                 = new CustomersModel;
			$cm->title          = Input::get('title');
			$cm->full_name      = Input::get('name');
			$cm->email          = (isset($email)?$email:'');
			$cm->phone          = Input::get('phone');
			$cm->id_card_number = Input::get('id_card');
			$cm->card_type      = Input::get('card_type');
			$cm->address        = (isset($address)?$address:'');
			$cm->created_by     = $this->admin['id'];
			$cm->save();
			
			$tm                 = new TransactionsModel;
			$tm->created_by     = $this->admin['id'];
			$tm->code           = $codes;
			$tm->id_customers   = $cm->id;
			$tm->checkin_date   = Input::get('checkin');
			$tm->checkout_date  = Input::get('checkout');
			$tm->end_payment    = (Input::get('max_payment'))?Input::get('max_payment'):NULL;
			$tm->payment_status = 0;
			$tm->book_via       = 2;
			// $tm->discount       = (Input::get('discount'))?Input::get('discount'):'0';
			// $tm->type          = 1;
			$tm->save();

			// $days = 
			// $exp1 = explode(' ', Input::get('checkin'));
			// $exp2 = explode(' ', Input::get('checkout'));

			// $checkin       = explode('-', $exp1[0]);
			// $date_checkin  = $checkin[2];
			// $month_checkin = $checkin[1];
			// $year_checkin  = $checkin[0];

			// $checkout       = explode('-', $exp2[0]);
			// $date_checkout  = $checkout[2];
			// $month_checkout = $checkout[1];
			// $year_checkout  = $checkout[0];

			// $tgl1 = GregorianToJD($month_checkin, $date_checkin, $year_checkin);
			// $tgl2 = GregorianToJD($month_checkout, $date_checkout, $year_checkout);
			
			$day = Helper::countDay(Input::get('checkin'),Input::get('checkout'));

			$rooms    = Helper::expl(Input::get('room'));
			$adds     = Helper::expl(Input::get('additional'));
			$adds_qty = Helper::expl(Input::get('additional_qty'));

			if(!empty($rooms)){
				foreach ($rooms as $key => $value) {
					$roomDet = Helper::roomDetail($value);
					$tdm                  = new TransactionsDetailModel;
					$tdm->id_transactions = $tm->id;
					$tdm->id_item         = $value;
					$tdm->item            = $roomDet['name'];
					$tdm->class           = $roomDet['class'];
					$tdm->qty             = 1;
					$tdm->item_price      = $roomDet['price'];
					$tdm->note            = '';
					$tdm->type            = 1;
					$tdm->created_by      = $this->admin['id'];
					$tdm->save();

					Helper::setRoomStatus($value);

					$total[] = $roomDet['price']*$day;
				}
			}

			if(!empty($adds)){
				foreach ($adds as $key => $value) {
					$addsDet = Helper::addsDetail($value);
					$tdm                  = new TransactionsDetailModel;
					$tdm->id_transactions = $tm->id;
					$tdm->id_item         = $value;
					$tdm->item            = $addsDet['name'];
					$tdm->qty             = $adds_qty[$key];
					$tdm->item_price      = $addsDet['price'];
					$tdm->note            = '';
					$tdm->type            = 0;
					$tdm->created_by      = $this->admin['id'];
					$tdm->save();
					$total[] = $addsDet['price']*$adds_qty[$key];
				}
			}

			$fixedPrice = array_sum($total);

			$TM = TransactionsModel::find($tm->id);
			$TM->total                = $fixedPrice;
			// $TM->total_after_discount = ($tm->discount)?Helper::afterDiscount($fixedPrice,$tm->discount):$fixedPrice;
			$TM->save();

			return Redirect::to('admin/checkIn/detail/'.$tm->id)->with('booking','Data has been added');
		}
	}

	public function getPrint($id)
	{
		$tm  = TransactionsModel::find($id);
		$ctm = CafeTransactionModel::where('transaction_id',$id)->first();
		$data['transaction']     = $tm;
		$data['customers']       = CustomersModel::find($tm->id_customers);
		$data['staff']           = UserModel::find($this->admin['id']);
		$data['detail']          = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->get();
		// // $data['products']  = ProductsModel::all();
		// $data['payments']     = TransactionsPaymentModel::where('id_transactions',$id)->get();
		$data['totalPayment']    = Helper::TotalPayment($id);
		if($ctm){
			$data['detailCafe']      = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
		}
		// $data['totalPayment'] = Helper::TotalPayment($id);
		return View::make('backend.booking.print',$data);
	}
	public function getModalAddAdds($transaction)
	{
		$data['transaction'] = TransactionsModel::find($transaction);
		$data['tambahan']    = ExtraChargeModel::orderBy('id','desc')->get();
		return View::make('backend.booking.modal_add_additional',$data);
	}

	public function postAddAdds($transaction)
	{
		$rules = array(
			'additional'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/detail/'.$transaction)->withErrors($valid)->withInput();
		}else{

			$tm            = TransactionsModel::find($transaction);
			
			$day           = Helper::countDay($tm->checkin_date,$tm->checkout_date);
			
			$additional    = Input::get('additional');
			$additionalQty = Input::get('additional_qty');
			
			$tdm           = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',1)->get();
			$tdm_adds      = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',0)->get();
			foreach ($tdm as $value) {
				$rooms[] = $value->item_price * $day;
			}
			if(count($tdm_adds)>0){
				foreach ($tdm_adds as $row) {
					$rooms[] = $row->item_price * $row->qty;
				}
			}
// print_r($additional);
// print_r($additionalQty);
// return;
			if(!empty($additional)){
				foreach ($additional as $key => $val) {
					$addsDet = Helper::addsDetail($val);
					$tdm                  = new TransactionsDetailModel;
					$tdm->id_transactions = $tm->id;
					$tdm->id_item         = $val;
					$tdm->item            = $addsDet['name'];
					$tdm->qty             = $additionalQty[$key];
					$tdm->item_price      = $addsDet['price'];
					$tdm->note            = '';
					$tdm->type            = 0;
					$tdm->created_by      = $this->admin['id'];
					$tdm->save();
					$rooms[] = $addsDet['price']*$additionalQty[$key];
				}
			}
			$fixedPrice = array_sum($rooms);
			$tm->total  = $fixedPrice;
			// $tm->total_after_discount = ($tm->discount)?Helper::afterDiscount($fixedPrice,$tm->discount):$fixedPrice;
			$tm->save();
			Helper::CheckStatusPayment($transaction);
			return Redirect::to('admin/checkIn/detail/'.$transaction)->with('booking','Additional has been added');
		}
	}
	public function getModalAddRoom($transaction)
	{
		$data['transaction'] = TransactionsModel::find($transaction);
		$data['rooms']       = RoomModel::orderBy('id','asc')
								->where('id_parent','!=','null')
								->where('status',1)
								->where('kondisi_kamar',1)->get();
		return View::make($this->folder.'.modal_add_room',$data);
	}
	
	public function postAddRoom($transaction)
	{
		$rules = array(
			'room'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/detail/'.$transaction)->withErrors($valid)->withInput();
		}else{

			$tm    = TransactionsModel::find($transaction);
			
			$day   = Helper::countDay($tm->checkin_date,$tm->checkout_date);
			
			$room = Input::get('room');

			$tdm      = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',1)->get();
			$tdm_adds = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',0)->get();
			foreach ($tdm as $value) {
				$rooms[] = $value->item_price * $day;
			}
			if(count($tdm_adds)>0){
				foreach ($tdm_adds as $row) {
					$rooms[] = $row->item_price * $row->qty;
				}
			}


			if(!empty($room)){
				foreach ($room as $key => $val) {
					$roomDet              = Helper::roomDetail($val);
					$ntdm                  = new TransactionsDetailModel;
					$ntdm->id_transactions = $tm->id;
					$ntdm->id_item         = $val;
					$ntdm->item            = $roomDet['name'];
					$ntdm->class           = $roomDet['class'];
					$ntdm->qty             = 1;
					$ntdm->item_price      = $roomDet['price'];
					$ntdm->note            = '';
					$ntdm->type            = 1;
					$ntdm->created_by      = $this->admin['id'];
					$ntdm->save();

					Helper::setRoomStatus($val);
					
					$rooms[] = $roomDet['price'] * $day;
				}
			}

			$fixedPrice = array_sum($rooms);
			$tm->total = $fixedPrice;
			// $tm->total_after_discount = ($tm->discount)?Helper::afterDiscount($fixedPrice,$tm->discount):$fixedPrice;
			$tm->save();
			Helper::CheckStatusPayment($transaction);
			return Redirect::to('admin/checkIn/detail/'.$transaction)->with('booking','Room has been added');
		}
	}

	public function getModalEditTransaction($transaction)
	{
		$data['transaction'] = TransactionsModel::find($transaction);
		return View::make('backend.booking.modal_edit_transaction',$data);
	}

	public function postUpdateTransaction($transaction)
	{
		$rules = array(
			'checkin'     => 'required',
			'checkout'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/detail/'.$transaction)->withErrors($valid)->withInput();
		}else{
			$day      = Helper::countDay(Input::get('checkin'),Input::get('checkout'));
			$tdm      = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',1)->get();
			$tdm_adds = TransactionsDetailModel::where('id_transactions',$transaction)->where('type',0)->get();
			foreach ($tdm as $value) {
				$rooms[] = $value->item_price * $day;
			}
			if(count($tdm_adds)>0){
				foreach ($tdm_adds as $row) {
					$rooms[] = $row->item_price * $row->qty;
				}
			}
			$fixedPrice = array_sum($rooms);
			$tm                = TransactionsModel::find($transaction);
			$tm->checkin_date  = Input::get('checkin');
			$tm->checkout_date = Input::get('checkout');
			$tm->updated_by    = $this->admin['id'];
			$tm->total         = $fixedPrice;
			// $tm->total_after_discount = ($tm->discount)?Helper::afterDiscount($fixedPrice,$tm->discount):$fixedPrice;
			$tm->save();
			Helper::CheckStatusPayment($transaction);

			return Redirect::to('admin/checkIn/detail/'.$transaction)->with('booking','Payment has been updated');
		}
	}
	
	public function getModalEditCustomers($id,$tr)
	{
		$data['user'] = CustomersModel::find($id);
		$data['tr'] = $tr;
		return View::make('backend.booking.modal_edit_customers',$data);
	}

	public function postUpdateCustomers($id,$tr)
	{
		$rules = array(
			'name'        => 'required',
			'title'       => 'required',
			'card_type'   => 'required',
			'id_card'     => 'required|numeric',
			'address'     => 'required',
			'phone'       => 'required',
			'email'       => 'email'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/detail/'.$tr)->withErrors($valid)->withInput();
		}else{
			$email   = Input::get('email');
			$address = Input::get('address');

			$cm                 = CustomersModel::find($id);
			$cm->title          = Input::get('title');
			$cm->full_name      = Input::get('name');
			$cm->email          = (isset($email)?$email:'');
			$cm->phone          = Input::get('phone');
			$cm->id_card_number = Input::get('id_card');
			$cm->card_type      = Input::get('card_type');
			$cm->address        = (isset($address)?$address:'');
			$cm->updated_by     = $this->admin['id'];
			$cm->save();

			return Redirect::to('admin/checkIn/detail/'.$tr)->with('booking','Customer has been updated');
		}
	}

	public function getDetail($id)
	{
		View::share('title','Check In');
		View::share('path','Detail');
		if(!empty($id)){
			$tm  = TransactionsModel::find($id);
			$ctm = CafeTransactionModel::where('transaction_id',$id)->first();
			if(!empty($tm)){
				$data['transaction']  = $tm;
				$data['customers']    = CustomersModel::find($tm->id_customers);
				$data['detailRoom']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',1)->get();
				$data['detailAdds']   = TransactionsDetailModel::orderBy('id','asc')->where('id_transactions',$id)->where('type',0)->get();
				if($ctm){
					$data['cafeTransaction'] = CafeTransactionModel::find($ctm->id);
					$data['detailCafe']      = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$ctm->id)->get();
				}
				$data['payments']     = TransactionsPaymentModel::where('id_transactions',$id)->get();
				$data['totalPayment'] = Helper::TotalPayment($id);
				return View::make($this->folder.'.detail',$data);
			}
		}

	}

	public function getDeleteCafeItem($id,$transaction)
	{
		// $tdm = TransactionsDetailModel::find($id);

		$tm       = TransactionsModel::find($transaction);
		
		$ctdm     = CafeTransactionDetailModel::find($id);
		$ctm      = CafeTransactionModel::find($ctdm->id_cafe_transaction);
		
		$day      = Helper::countDay($tm->checkin_date,$tm->checkout_date);
		
		$tdm_adds = TransactionsDetailModel::where('id','!=',$id)->where('id_transactions',$tm->id)->where('type',0)->get();
		$tdm_room = TransactionsDetailModel::where('id_transactions',$tm->id)->where('type',1)->get();

		foreach ($tdm_room as $value) {
			$rooms[] = $value->item_price * $day;
		}

		if(count($tdm_adds)>0){
			foreach ($tdm_adds as $row) {
				$rooms[] = $row->item_price * $row->qty;
			}
		}

		Helper::plusCafeItemStock($ctdm->id_cafe_item, $ctdm->qty);
		$ctdm->delete(); // Remove Items

		$cafeItem = CafeTransactionDetailModel::where('id_cafe_transaction',$ctm->id)->get();

		foreach ($cafeItem as $value) {
			$items[] = $value->item_price * $value->qty;
			$rooms[] = $value->item_price * $value->qty;
		}

		$ctm->total = isset($items)?array_sum($items):NULL;
		$ctm->save();

		$tm->total = array_sum($rooms);
		$tm->save();

		Helper::CheckStatusPayment($tm->id);

		return Redirect::to($this->uri.'/detail/'.$tm->id)->with('success','Cafe item has been deleted');
	}

	public function getModalAddCafe($transaction)
	{
		$data['cafe']            = CafeItemModel::orderBy('id','asc')->get();
		$data['action']          = $this->uri.'/modal-add-cafe/'.$transaction;
		$data['cafeTransaction'] = CafeTransactionModel::where('transaction_id',$transaction)->first();
		return View::make($this->folder.'.modal_add_cafe',$data);
	}

	public function postModalAddCafe($transaction)
	{
		$rules = array(
			'number'	=> 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if(!$valid->fails())
		{
			$tabNum   = Input::get('number');

			$tm = CafeTransactionModel::where('transaction_id',$transaction)->first();
			if(!$tm) {
				$tm                 = new CafeTransactionModel;
				$tm->created_by     = $this->admin['id'];
				$tm->transaction_id = $transaction;
				$tm->type           = 1;
				$tm->table_number   = ($tabNum)?$tabNum:NULL;
				$tm->save();
			}

			$item      = Input::get('id');
			$item_qty  = Input::get('qty');
			$item_note = Input::get('note');
			if(count($item) > 0){
				foreach ($item as $key => $value) {
					$detail = Helper::getCafeItemDetail($value);
					$tdm                      = new CafeTransactionDetailModel;
					$tdm->id_cafe_transaction = $tm->id;
					$tdm->id_cafe_item        = $value;
					$tdm->item_name           = $detail['name'];
					$tdm->qty                 = $item_qty[$key];
					$tdm->item_price          = $detail['price'];
					$tdm->note                = Input::get('note')[$key];
					$tdm->created_by          = $this->admin['id'];
					$tdm->save();

					$total[] = $detail['price']*$item_qty[$key];
					Helper::minusCafeItemStock($value,$item_qty[$key]);
				}
			}

			$fixedPrice = array_sum($total);

			$TM           = TransactionsModel::find($transaction);
			$currentPrice = $TM->total + $fixedPrice;
			$TM->total    = $currentPrice;
			$TM->save();


			$tm->table_number = ($tabNum)?$tabNum:NULL;
			$tm->total        = $fixedPrice;
			$tm->save();

			return Redirect::to($this->uri.'/detail/'.$transaction)->with('success','Data has been added');
		}
	}

	public function getModalAddPayment($id)
	{
		$data['transaction'] = TransactionsModel::find($id);
		$data['method']      = PaymentMethodModel::orderBy('id','desc')->get();
		return View::make($this->folder.'.modal_add_payment',$data);
	}

	public function getModalEditPayment($transaction,$idpayment)
	{
		$data['method']      = PaymentMethodModel::all();
		$data['transaction'] = TransactionsModel::find($transaction);
		$data['payment']     = TransactionsPaymentModel::find($idpayment);
		return View::make($this->folder.'.modal_edit_payment',$data);
	}

	public function postEditPayment($transaction,$idpayment)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required',
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::back()->with('warning','The payment fields are required');;
			// return Redirect::to('admin/checkIn/detail/'.$transaction)->with('warning','The payment fields are required');;
		}else{
			$uang = Input::get('amount');
			if(!empty($uang)){
				$note   = Input::get('note');
				$filter = array(',','.');
				$TPM                  = TransactionsPaymentModel::find($idpayment);
				$TPM->id_transactions = $transaction;
				$TPM->amount          = str_replace($filter, "", $uang);
				$TPM->payment_date    = Input::get('date');
				$TPM->payment_method  = Input::get('method');
				$TPM->note            = ($note)?$note:'';
				$TPM->updated_by      = $this->admin['id'];
				$TPM->save();
				Helper::CheckStatusPayment($transaction);
				return Redirect::back()->with('booking','Payment has been updated');
				// return Redirect::to('admin/checkIn/detail/'.$transaction)->with('booking','Payment has been updated');
			}else{
				return Redirect::back()->with('warning','Amount field is required.');
				// return Redirect::to('admin/checkIn/detail/'.$transaction)->with('warning','Amount field is required.');
			}
		}
	}

	public function postAddPayment($id)
	{
		$rules = array(
			'method' => 'required',
			'amount' => 'required',
			'date'   => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/checkIn/detail/'.$id)->with('warning','The payment fields are required');
		}else{
			$uang = Input::get('amount');
			if(!empty($uang)){
				$note   = Input::get('note');
				$filter = array(',','.');
				$PM                  = new TransactionsPaymentModel;
				$PM->id_transactions = $id;
				$PM->amount          = str_replace($filter, "", $uang); 
				$PM->payment_date    = Input::get('date');
				$PM->payment_method  = Input::get('method');
				$PM->note            = ($note)?$note:'';
				$PM->created_by      = $this->admin['id'];
				$PM->save();

				Helper::CheckStatusPayment($id);
				return Redirect::back()->with('booking','Payment has been added');
				// return Redirect::to('admin/checkIn/detail/'.$id)->with('booking','Payment has been added');
			}else{
				return Redirect::back()->with('warning','Amount field is required.');
				// return Redirect::to('admin/checkIn/detail/'.$id)->with('warning','Amount field is required.');
			}
		}
	}

	public function getDeletePayment($idpayment,$transaction)
	{
		TransactionsPaymentModel::find($idpayment)->delete();
		Helper::CheckStatusPayment($transaction);
		return Redirect::back()->with('booking','Payment has been deleted');
		// return Redirect::to('admin/checkIn/detail/'.$transaction)->with('booking','Payment has been deleted');
	}

	public function getDeleteRoom($id)
	{
		$tdm      = TransactionsDetailModel::find($id);
		$tm       = TransactionsModel::find($tdm->id_transactions);
		
		Helper::setRoomStatusToEmpty($tdm->id_item);
		
		$day      = Helper::countDay($tm->checkin_date,$tm->checkout_date);
		
		$tdm_room = TransactionsDetailModel::where('id','!=',$id)->where('id_transactions',$tm->id)->where('type',1)->get();
		if(count($tdm_room)<1){
			return Redirect::to('admin/checkIn/detail/'.$tm->id)->with('warning','Room can not be deleted');
		}
		$tdm_adds = TransactionsDetailModel::where('id_transactions',$tm->id)->where('type',0)->get();

		foreach ($tdm_room as $value) {
			$rooms[] = $value->item_price * $day;
		}
		if(count($tdm_adds)>0){
			foreach ($tdm_adds as $row) {
				$rooms[] = $row->item_price * $row->qty;
			}
		}

		$tm->total = array_sum($rooms);
		$tm->save();

		$tdm->delete();
		Helper::CheckStatusPayment($tm->id);
		return Redirect::to('admin/checkIn/detail/'.$tm->id)->with('booking','Room has been deleted');
	}

	public function getDeleteItem($id)
	{
		$tdm      = TransactionsDetailModel::find($id);
		$tm       = TransactionsModel::find($tdm->id_transactions);
		
		$day      = Helper::countDay($tm->checkin_date,$tm->checkout_date);
		
		$tdm_adds = TransactionsDetailModel::where('id','!=',$id)->where('id_transactions',$tm->id)->where('type',0)->get();
		$tdm_room = TransactionsDetailModel::where('id_transactions',$tm->id)->where('type',1)->get();

		foreach ($tdm_room as $value) {
			$rooms[] = $value->item_price * $day;
		}
		if(count($tdm_adds)>0){
			foreach ($tdm_adds as $row) {
				$rooms[] = $row->item_price * $row->qty;
			}
		}

		$tm->total = array_sum($rooms);
		$tm->save();

		$tdm->delete();
		Helper::CheckStatusPayment($tm->id);
		return Redirect::to('admin/checkIn/detail/'.$tm->id)->with('booking','Additional has been deleted');
	}

	public function getDelete($id)
	{
		$tm = TransactionsModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($tm)){
				$tdm = TransactionsDetailModel::where('type',1)->where('id_transactions',$tm->id)->get();
				foreach ($tdm as $key => $value) {
					Helper::setRoomStatusToEmpty($value->id_item);
				}
				TransactionsDetailModel::where('id_transactions',$tm->id)->delete();
				TransactionsPaymentModel::where('id_transactions',$tm->id)->delete();
				$tm->delete();
				return Redirect::to('admin/checkIn')->with('booking','Data has been deleted');
			}
		}
	}

}
