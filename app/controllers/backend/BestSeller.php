<?php

class BestSeller extends BaseController {

	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Best Seller');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');

		$qr	= OrdersDetailModel::orderBy('id','desc')->groupBy('code_product');
		if($cari){
			$qr = $qr->where('code_product','LIKE',"%$cari%");
		}
		if(Session::has('BS_from_date') && Session::has('BS_to_date')){
			$qr = $qr->whereBetween('created_at',array(Session::get('BS_from_date').' 00:00:00',Session::get('BS_to_date').' 23:59:59'));
		}
		$qr = $qr->paginate($this->limit);

		$data['best'] = $qr;
		return View::make('backend.best.index',$data);
	}

	public function getFilter()
	{
		$BS_date_filter = Input::get('BS_date_filter');
		if(!empty($BS_date_filter)){
			$dates       = explode('/', $BS_date_filter);
			$from        = date('Y-m-d',strtotime($dates[0]));
			$to          = date('Y-m-d',strtotime($dates[1]));
			$date1       = abs(strtotime($from));
			$date2       = abs(strtotime($to));
			
			if($date1 != '25200' && $date2 != '25200'){
				Session::put('BS_from_date',$from);
				Session::put('BS_to_date',$to);
				Session::put('BS_date_filter',$BS_date_filter);
			}else{
				Session::forget('BS_from_date');
				Session::forget('BS_to_date');
				Session::forget('BS_date_filter');
			}
		}else{
			Session::forget('BS_from_date');
			Session::forget('BS_to_date');
			Session::forget('BS_date_filter');
		}
		return Redirect::to('admin/best');
	}
}
