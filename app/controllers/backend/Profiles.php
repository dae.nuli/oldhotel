<?php

class Profiles extends BaseController {
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Profile');
		View::share('path','Edit');
		$data['profile'] = UserModel::find($this->admin['id']);
		return View::make('backend.profile.index',$data);
	}

	public function postIndex()
	{
		$pass = Input::get('password');
		$rules['name']         = 'required';
		$rules['phone_number'] = 'required|numeric';
		$rules['address']      = 'required';
		if(!empty($pass)){
			$rules['password']        = 'required|min:8';
			$rules['retype_password'] = 'required|same:password';
		}
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/profile')->withErrors($valid);
		}else{
			$cm            = UserModel::find($this->admin['id']);
			$cm->name      = Input::get('name');
			$cm->bbm       = Input::get('bbm');
			$cm->address   = Input::get('address');
			$cm->phone     = Input::get('phone_number');
			$cm->facebook  = Input::get('facebook');
			$cm->twitter   = Input::get('twitter');
			$cm->instagram = Input::get('instagram');
			if(!empty($pass)){
				$cm->password     = sha1($pass.'sha1');
			}
			$cm->save();
			return Redirect::to('admin/profile')->with('users','Data has been updated'); 
		}
	}
}