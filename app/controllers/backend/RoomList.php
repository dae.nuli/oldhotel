<?php

class RoomList extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Room');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= RoomModel::orderBy('status','desc')->orderBy('kondisi_kamar','desc')->where('id_parent','!=','null');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['room'] = $qr->paginate($this->limit);
		return View::make('backend.room.index',$data);
	}

	public function getDirty()
	{
		$rm = RoomModel::where('kondisi_kamar',2)->count();
		if(!empty($rm)){
			return Redirect::to('admin/room')->with('room_alert','Please clean the dirty room');
		}else{
			return Redirect::to('admin/room')->with('room','Dirty room is empty');
		}
	}

	public function getCleaning($id)
	{
		if(!empty($id)){
			$rm = RoomModel::find($id);
			$rm->kondisi_kamar = 1;
			$rm->save();
			return Redirect::to('admin/room')->with('room','Room has been cleaned');
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Room');
		View::share('path','Create');
		$data['class'] = RoomModel::where('id_parent',null)->get();
		$data['facility'] = FacilityModel::all();
		return View::make('backend.room.create',$data);
	}

	public function postFindClass()
	{
		$idClass = Input::get('ClassId');
		if(!empty($idClass)){
			$row = RoomModel::find($idClass);
			$data['price']    = $row->price;
			$data['facility'] = $row->facility;
			return Response::json($data,200);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		// $rules = array(
		// 	'class' => 'required|numeric',
		// 	'name'  => 'required',
		// 	'price' => 'required'
		// 	);
		// $valid = Validator::make(Input::all(),$rules);
		// if($valid->fails())
		// {
		// 	return Redirect::to('admin/room/create')->withErrors($valid)->withInput();
		// }else{
			$price    = Input::get('price');
			$facility = Input::get('facility');
			// echo "<pre>";
			// var_dump($facility);
			// echo "</pre>";
			// return ;
			$filter   = array(',','.');
			$name     = Input::get('name');
			foreach ($name as $key => $value) {
				$cm             = new RoomModel;
				$cm->created_by = $this->admin['id'];
				$cm->id_parent  = Input::get('class')[$key];
				$cm->name       = $value;
				$cm->price      = str_replace($filter, "", $price[$key]); 
				$cm->facility   = (isset($facility)?implode(',', $facility):'');
				// $cm->facility   = (!empty($facil)?json_encode($facil,JSON_FORCE_OBJECT):'');
				$cm->save();
			}
			return Redirect::to('admin/room')->with('room','Data has been added');
		// }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Room');
		View::share('path','Create');
		$data['room']     = RoomModel::find($id);
		$data['facility'] = FacilityModel::all();
		$data['class'] = RoomModel::where('id_parent',null)->get();
		return View::make('backend.room.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'  => 'required',
			'price' => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/room/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$uang = Input::get('price');
			if(!empty($uang)){
				$filter = array(',','.');
				$facil = Input::get('facility');
				$cm             = RoomModel::find($id);
				$cm->updated_by = $this->admin['id'];
				$cm->name       = Input::get('name');
				$cm->price      = str_replace($filter, "", $uang); 
				$cm->facility   = (!empty($facil)?implode(',', $facil):'');
				// $cm->facility   = (!empty($facil)?json_encode($facil,JSON_FORCE_OBJECT):'');
				$cm->save();
				return Redirect::to('admin/room')->with('room','Data has been updated');
			}else{
				return Redirect::to('admin/room')->with('room_alert','The price field is required.');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = RoomModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				$tdm = TransactionsDetailModel::withTrashed()->where('id_item',$id)->where('type',1)->count();
				if(!empty($tdm)){
					return Redirect::to('admin/room')->with('room_alert','The room is used');
				}else{
					$am->delete();
					return Redirect::to('admin/room')->with('room','Data has been deleted');
				}
			}
		}
	}

}
