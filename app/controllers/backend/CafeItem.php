<?php

class CafeItem extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit  = 10;
	public $title  = 'Cafe Item';
	public $folder = 'backend.cafe_item';
	public $uri    = 'admin/cafeItem';
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title',$this->title);
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= CafeItemModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['url'] = $this->uri;
		$data['index'] = $qr->paginate($this->limit);
		return View::make($this->folder.'.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title',$this->title);
		View::share('path','Create');
		$data['url']    = $this->uri;
		$data['action'] = $this->uri;
		return View::make($this->folder.'.create',$data);
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'  => 'required',
			'stock' => 'required',
			'price' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/create')->withErrors($valid)->withInput();
		}else{
			$price  = Input::get('price');
			$filter = array(',','.');
			$name = Input::get('name');
			foreach ($name as $key => $value) {
				$cm             = new CafeItemModel;
				$cm->created_by = $this->admin['id'];
				$cm->name       = $value;
				$cm->stock      = Input::get('stock')[$key];
				$cm->price      = str_replace($filter, "", $price[$key]); 
				$cm->save();
			}
			return Redirect::to($this->uri)->with('success','Data has been added');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title',$this->title);
		View::share('path','Edit');
		$data['url']    = $this->uri;
		$data['action'] = $this->uri.'/edit/'.$id;
		$data['index']  = CafeItemModel::find($id);
		return View::make($this->folder.'.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'  => 'required',
			'stock' => 'required',
			'price' => 'required'
		);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to($this->uri.'/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$price  = Input::get('price');
			$filter = array(',','.');
			$cm             = CafeItemModel::find($id);
			$cm->updated_by = $this->admin['id'];
			$cm->name       = Input::get('name');
			$cm->stock      = Input::get('stock');
			$cm->price      = str_replace($filter, "", $price); 
			$cm->save();
			return Redirect::to($this->uri)->with('success','Data has been updated');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = CafeItemModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				$tdm = CafeTransactionDetailModel::withTrashed()->where('id_cafe_item',$id)->count();
				if(!empty($tdm)){
					return Redirect::to($this->uri)->with('alert','The item is used');
				}else{
					$am->delete();
					return Redirect::to($this->uri)->with('success','Data has been deleted');
				}
			}
		}
	}

}
