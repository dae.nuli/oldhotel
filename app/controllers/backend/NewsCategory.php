<?php
class NewsCategory extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}
	
	public function getIndex()
	{
		View::share('path','Index');
		View::share('title','Category');
		$data['limit'] = $this->limit;
		if($this->admin['level'] == 1)
		{
			$qr	= NewsCategoryModel::orderBy('id','desc')
						->paginate($this->limit);		
		}else{
			$qr	= NewsCategoryModel::where('id_user',$this->admin['id'])
						->orderBy('id','desc')
						->paginate($this->limit);
		}
		$data['category'] = $qr;
		return View::make('backend.news_category.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Category');
		return View::make('backend.news_category.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array('name' => 'required');
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/news_category/create')->withErrors($valid);
		}else{
			$cm          = new NewsCategoryModel;
			$cm->id_user = $this->admin['id'];
			$cm->name    = Input::get('name');
			$cm->save();
			return Redirect::to('admin/news_category')->with('news_category','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Category');

		if($this->admin['level'] == 1)
		{
			$cm = NewsCategoryModel::find($id);
		}else{
			$cm = NewsCategoryModel::where('id',$id)
			->where('id_user',$this->admin['id'])
			->first();
		}
		$data['category'] = $cm;
		return View::make('backend.news_category.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array('name' => 'required');
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/news_category/edit/'.$id)->withErrors($valid);
		}else{
			$cm       = NewsCategoryModel::find($id);
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->name = Input::get('name');
				$cm->save();
				return Redirect::to('admin/news_category')->with('news_category','Data has been updated'); 
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		View::share('path','Delete');
		View::share('title','Category');

		$am = NewsCategoryModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			$pm = ProductsModel::where('id_category',$id)->count();
			if($pm > 0){
				return Redirect::to('admin/news_category')->with('news_category_alert','Data is used');
			}else{
				$am->delete();
				return Redirect::to('admin/news_category')->with('news_category','Data has been deleted');
			}
		}
	}


}
