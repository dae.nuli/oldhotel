<?php

class ProductsSubCategory extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}
	
	public function getIndex()
	{
		// return Input::get('search');
		View::share('path','Index');
		View::share('title','Sub Category');
		$data['limit'] = $this->limit;
		$category      = (Session::get('category')?Session::get('category'):"all");
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$data['category'] = CategoryModel::all();
			$qr               = SubCategoryModel::orderBy('id','desc');
			if($category!="all"){
				$qr = $qr->where('id_category',$category);
			}
			if($cari){
				$qr = $qr->where('name','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);		
		}else{
			$data['category'] = CategoryModel::where('id_user',$this->admin['id'])->get();
			$qr               = SubCategoryModel::where('id_user',$this->admin['id'])->orderBy('id','desc');
			if($category!="all"){
				$qr = $qr->where('id_category',$category);
			}
			if($cari){
				$qr = $qr->where('name','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);
		}
		$data['session']     = $category;
		$data['subcategory'] = $qr;
		return View::make('backend.subcategory.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Sub Category');
		if($this->admin['level'] == 1){
			$data['category']	= CategoryModel::all();
		}else{
			$data['category']	= CategoryModel::where('id_user',$this->admin['id'])->get();
		}
		return View::make('backend.subcategory.create',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'        => 'required',
			'category'    => 'required',
			'code_prefix' => 'required|unique:product_subcategory,code_prefix',
			'picture'     => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/product_subcategory/create')->withErrors($valid)->withInput();
		}else{
			$names = Input::get('name');
			$picture         = Input::get('picture');
			$cm              = new SubCategoryModel;
			$cm->id_user     = $this->admin['id'];
			$cm->id_category = Input::get('category');
			$cm->name        = $names;
			$cm->slug        = Str::slug($names);
			$cm->code_prefix = Input::get('code_prefix');
			$cm->picture     = ($picture?$picture:'');
			$cm->save();
			return Redirect::to('admin/product_subcategory')->with('product_subcategory','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Sub Category');
		if($this->admin['level'] == 1)
		{
			$data['category']	= CategoryModel::all();
			$cm = SubCategoryModel::find($id);
		}else{
			$data['category']	= CategoryModel::where('id_user',$this->admin['id'])->get();
			$cm = SubCategoryModel::where('id',$id)
			->where('id_user',$this->admin['id'])
			->first();
		}
		$data['subcategory'] = $cm;
		return View::make('backend.subcategory.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'        => 'required',
			'category'    => 'required',
			'code_prefix' => 'required|unique:product_subcategory,code_prefix,'.$id,
			'picture'     => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/product_subcategory/edit/'.$id)->withErrors($valid);
		}else{
			$cm              = SubCategoryModel::find($id);
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$names = Input::get('name');
				$picture         = Input::get('picture');
				$cm->id_category = Input::get('category');
				$cm->name        = $names;
				$cm->slug        = Str::slug($names);
				$cm->code_prefix = Input::get('code_prefix');
				$cm->picture     = ($picture?$picture:'');
				$cm->save();
				return Redirect::to('admin/product_subcategory')->with('product_subcategory','Data has been updated'); 
			}
		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = SubCategoryModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			$pm = ProductsModel::where('id_subcategory',$id)->count();
			if($pm > 0){
				return Redirect::to('admin/product_subcategory')->with('product_subcategory_alert','Data is used');
			}else{
				$am->delete();
				return Redirect::to('admin/product_subcategory')->with('product_subcategory','Data has been deleted');
			}
		}
	}

	public function postFilter()
	{
		$submit = Input::get('submit');
		if($submit)
		{
			Session::put('category',Input::get('category'));
		}
		return Redirect::to('admin/product_subcategory');
	}



	public function getDetail($id=null)
	{
		$psc = SubCategoryModel::find($id);
		View::share('title','Sub Category');
		View::share('path',$psc->name.'<i class="fa fa-fw fa-angle-right"></i> Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$qr	= ProductsModel::orderBy('id','desc');
			if($cari){
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
			$qr = $qr->where('id_subcategory',$id)->paginate($this->limit);
		}else{
			$qr	= ProductsModel::where('id_user',$this->admin['id']);
			if($cari){
				$qr = $qr->where('code','LIKE',"%$cari%");
			}
			$qr = $qr->where('id_subcategory',$id)->orderBy('id','desc')->paginate($this->limit);
		}
		$data['sub']     = $id;
		$data['product'] = $qr;
		return View::make('backend.subcategory.detail',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreateProduct()
	{
		$id = Input::get('sub');
		View::share('title','Sub Category');
		if($this->admin['level'] == 1){
			$subcategory = SubCategoryModel::find($id);
		}else{
			$subcategory = SubCategoryModel::where('id_user',$this->admin['id'])->where('id',$id)->first();
		}
		View::share('path',$subcategory->name.'<i class="fa fa-fw fa-angle-right"></i> Create');
		$data['subcategory'] = $subcategory;
		return View::make('backend.subcategory.create_product',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreateProduct()
	{
		$rules = array(
			'price'   => 'required|numeric',
			'content' => 'required',
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		$id_sub = Input::get('subcategory');
		if($valid->fails())
		{
			return Redirect::to('admin/product_subcategory/create-product?sub='.$id_sub)->withErrors($valid)->withInput();
		}else{
			$prefix  = Helper::SubPrefix($id_sub);
			$pm      = ProductsModel::where('id_subcategory',$id_sub)->orderBy('code','desc')->first();
			if(empty($pm)){
				$code_product = $prefix."0001";
			}else{
				$number = (int)substr($pm->code, strlen($prefix), 4);
				$number++;
				$code_product = $prefix.sprintf("%04d", $number);
			}
			$picture = Input::get('picture');
			$cm                 = new ProductsModel;
			$cm->id_user        = $this->admin['id'];
			$cm->id_subcategory = $id_sub;
			$cm->code 			= $code_product;
			$cm->price          = Input::get('price');
			$cm->note           = Input::get('content');
			$cm->picture        = ($picture?$picture:'');
			$cm->save();
			return Redirect::to('admin/product_subcategory/detail/'.$id_sub)->with('product_subcategory','Data has been added');
		}
	}

	public function getEditProduct($sub,$id)
	{
		View::share('title','Sub Category');

		if($this->admin['level'] == 1)
		{
			$cm = ProductsModel::where('id',$id)
			->where('id_subcategory',$sub)
			->first();
		}else{
			$cm = ProductsModel::where('id',$id)
			->where('id_subcategory',$sub)
			->where('id_user',$this->admin['id'])
			->first();
		}
		$subcategory = SubCategoryModel::find($sub);
		View::share('path',$subcategory->name.'<i class="fa fa-fw fa-angle-right"></i> Edit');
		$data['subcategory'] = $subcategory;
		$data['product']     = $cm;
		return View::make('backend.subcategory.edit_product',$data);
	}

	public function postEditProduct($sub,$id)
	{
		$rules = array(
			'price'   => 'required|numeric',
			'content' => 'required',
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/product_subcategory/edit-product/'.$sub.'/'.$id)->withErrors($valid);
		}else{
			$picture= Input::get('picture');
			$cm 	= ProductsModel::find($id);
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->price          = Input::get('price');
				$cm->note           = Input::get('content');
				$cm->picture        = ($picture?$picture:'');
				$cm->save();
				return Redirect::to('admin/product_subcategory/detail/'.$sub)->with('product_subcategory','Data has been updated'); 
			}
		}
	}

	public function getDeleteProduct($sub,$id)
	{
		$am = ProductsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/product_subcategory/detail/'.$sub)->with('product_subcategory','Data has been deleted');
			}
		}
	}
}
