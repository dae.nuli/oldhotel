<?php

class Gallery extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Gallery');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$qr	= GalleryModel::orderBy('id','desc');
			if($cari){
				$qr = $qr->where('title','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);		
		}else{
			$qr	= GalleryModel::where('id_user',$this->admin['id']);
			if($cari){
				$qr = $qr->where('title','LIKE',"%$cari%");
			}
			$qr = $qr->orderBy('id','desc')->paginate($this->limit);
		}
		$data['gallery'] = $qr;
		return View::make('backend.gallery.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Gallery');
		View::share('path','Create');
		return View::make('backend.gallery.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'title'   => 'required',
			'picture' => 'required',
			'content' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/gallery/create')->withErrors($valid)->withInput();
		}else{
			$picture     = Input::get('picture');
			$cm          = new GalleryModel;
			$cm->id_user = $this->admin['id'];
			$cm->title   = Input::get('title');
			$cm->content = Input::get('content');
			$cm->picture = ($picture?$picture:'');
			$cm->save();
			return Redirect::to('admin/gallery')->with('gallery','Data has been added');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Gallery');
		View::share('path','Edit');

		if($this->admin['level'] == 1)
		{
			$cm  = GalleryModel::find($id);
		}else{
			$cm  = GalleryModel::where('id',$id)
					->where('id_user',$this->admin['id'])
					->first();
		}
		$data['gallery']     = $cm;
		return View::make('backend.gallery.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'title'   => 'required',
			'picture' => 'required',
			'content' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/gallery/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$picture            = Input::get('picture');
			$cm                 = GalleryModel::find($id);
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->title   = Input::get('title');
				$cm->content = Input::get('content');
				$cm->picture = ($picture?$picture:'');
				$cm->save();
				return Redirect::to('admin/gallery')->with('gallery','Data has been added');
			}
		}
	}

	public function getActive($id)
	{
		$tm = GalleryModel::find($id);
		$tm->status = 1;
		$tm->save();
		return Redirect::to('admin/gallery')->with('gallery','Data has been activated'); 
	}

	public function getNoactive($id)
	{
		$tm = GalleryModel::find($id);
		$tm->status = 0;
		$tm->save();
		return Redirect::to('admin/gallery')->with('gallery','Data has been disactivated'); 
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = GalleryModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/gallery')->with('gallery','Data has been deleted');
			}
		}
	}
}
