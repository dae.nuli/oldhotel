<?php

class CafeReport extends BaseController {
	public $limit = 10;
	public $title  = 'Cafe Report';
	public $folder = 'backend.cafe_report';
	public $uri    = 'admin/cafeReport';
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title',$this->title);
		View::share('path','Index');
		$data['limit']  = $this->limit;
		$OM = CafeTransactionModel::where('type','0')->where('payment_status',1)->orderBy('created_at','asc');
		if(Session::has('Cafe_from_date') && Session::has('Cafe_to_date')){
			$OM = $OM->whereBetween('created_at',array(Session::get('Cafe_from_date').' 00:00:00',Session::get('Cafe_to_date').' 23:59:59'));
		}
		$data['index']     = $OM->paginate($this->limit);
		$data['grandTotal'] = Helper::CafeReportGrandTotal(Session::get('Cafe_from_date'),Session::get('Cafe_to_date'));
		$data['url']        = $this->uri;

		$SessionYear          = Session::get('cafe_year') ? Session::get('cafe_year') : date('Y');
		$month                = ($SessionYear==date('Y'))?self::MonthName(date('n')):array('January','February','March','April','May','June','July','August','September','October','November','December');
		$data['month']        = '"'. implode('","', $month) .'"';
		$data['year']         = ($SessionYear) ? $SessionYear : date('Y');

		$data['chart']        = ($SessionYear==date('Y'))?implode(",", self::ChartItemSalesOfNow($data['year'])):implode(",", self::ChartItemSales($data['year']));
		$data['yearRange']    = range(2014,date('Y'));

		$data['url']       = (Session::get('show_cafe_chart'))?'admin/cafeReport/hide':'admin/cafeReport/display';
		$data['is_chart']  = (Session::get('show_cafe_chart'))?Session::get('show_cafe_chart'):NULL;
		$data['chartText'] = (Session::get('show_cafe_chart'))?'Hide':'Show';

		return View::make($this->folder.'.index',$data);
	}
		
	public function postIndex()
	{
		$sales = Input::get('sales');
		if($sales){
			Session::put('cafe_year',$sales);
		}
		return Redirect::to($this->uri);
	}
	public function getDisplay()
	{
		Session::put('show_cafe_chart',true);
		return Redirect::to($this->uri);
	}

	public function getHide()
	{
		Session::forget('show_cafe_chart');
		return Redirect::to($this->uri);
	}

	public function getDetail($id)
	{
		View::share('title',$this->title);
		View::share('path','Detail');
		if(!empty($id)){
			$tm = CafeTransactionModel::find($id);
			if(!empty($tm)){
				$data['transaction']  = $tm;
				$data['detailItem']   = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$id)->get();
				$data['payments']     = CafeTransactionPaymentModel::where('id_cafe_transaction',$id)->get();
				$data['totalPayment'] = Helper::CafeTotalPayment($id);
				$data['url']          = $this->uri;
				return View::make($this->folder.'.detail',$data);
			}
		}
	}

	public function getPrint($id)
	{
		$tm  = CafeTransactionModel::find($id);
		$data['transaction']  = $tm;
		$data['staff']        = UserModel::find($this->admin['id']);
		$data['detail']       = CafeTransactionDetailModel::orderBy('id','asc')->where('id_cafe_transaction',$id)->get();
		$data['totalPayment'] = Helper::CafeTotalPayment($id);
		$data['url']          = $this->uri;
		return View::make($this->folder.'.print',$data);
	}

	public function getSummary()
	{ 
		View::share('title',$this->title);
		View::share('path','Index');
		$OM = CafeTransactionModel::where('payment_status',1)->orderBy('created_at','asc');
		if(Session::has('Cafe_from_date') && Session::has('Cafe_to_date')){
			$OM = $OM->whereBetween('created_at',array(Session::get('Cafe_from_date').' 00:00:00',Session::get('Cafe_to_date').' 23:59:59'));
		}
		$data['start']      = Session::get('Cafe_from_date');
		$data['end']        = Session::get('Cafe_to_date');
		$data['report']     = $OM->get();
		$data['grandTotal'] = Helper::CafeReportGrandTotal(Session::get('Cafe_from_date'),Session::get('Cafe_to_date'));
		return View::make($this->folder.'.print_summary',$data);
	}

	public function getFilter()
	{
		$Cafe_date_filter = Input::get('Cafe_date_filter');
		if(!empty($Cafe_date_filter)){
			$dates       = explode('/', $Cafe_date_filter);
			$from        = date('Y-m-d',strtotime($dates[0]));
			$to          = date('Y-m-d',strtotime($dates[1]));
			$date1       = abs(strtotime($from));
			$date2       = abs(strtotime($to));
			
			if($date1 != '25200' && $date2 != '25200'){
				Session::put('Cafe_from_date',$from);
				Session::put('Cafe_to_date',$to);
				Session::put('Cafe_date_filter',$Cafe_date_filter);
			}else{
				Session::forget('Cafe_from_date');
				Session::forget('Cafe_to_date');
				Session::forget('Cafe_date_filter');
			}
		}else{
			Session::forget('Cafe_from_date');
			Session::forget('Cafe_to_date');
			Session::forget('Cafe_date_filter');
		}
		return Redirect::to($this->uri);
	}

	public function ChartItemSales($year)
	{
		$vm = DB::table('cafe_transaction')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->where('type','0')
             ->whereRaw('year(created_at) = '.$year)
             ->get();
             	if(count($vm) > 0){
			        $mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;

				    foreach ($vm as $row) {
				    	if($row->month == 1){
					        $mon['jan']++;
				    	}elseif($row->month == 2){
					        $mon['feb']++;
				    	}elseif($row->month == 3){
					        $mon['mar']++;
				    	}elseif($row->month == 4){
					        $mon['apr']++;
				    	}elseif($row->month == 5){
					        $mon['mei']++;
				    	}elseif($row->month == 6){
					        $mon['jun']++;
				    	}elseif($row->month == 7){
					        $mon['jul']++;
				    	}elseif($row->month == 8){
					        $mon['agu']++;
				    	}elseif($row->month == 9){
					        $mon['sep']++;
				    	}elseif($row->month == 10){
					        $mon['oct']++;
				    	}elseif($row->month == 11){
					        $mon['nov']++;
				    	}elseif($row->month == 12){
					        $mon['dec']++;
				    	}
				    }
				    return $mon;
				}else{
					$mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;
			        return $mon;
				}
	}

	public function MonthName($month)
	{
		for ($i=1; $i <=$month ; $i++) { 
			switch ($i) {
				case 1:
					$Mth[] = 'January';
				break;
				
				case 2:
					$Mth[] = 'February';
				break;
				
				case 3:
					$Mth[] = 'March';
				break;
				
				case 4:
					$Mth[] = 'April';
				break;
				
				case 5:
					$Mth[] = 'May';
				break;
				
				case 6:
					$Mth[] = 'June';
				break;
				
				case 7:
					$Mth[] = 'July';
				break;
				
				case 8:
					$Mth[] = 'August';
				break;
				
				case 9:
					$Mth[] = 'September';
				break;
				
				case 10:
					$Mth[] = 'October';
				break;
				
				case 11:
					$Mth[] = 'November';
				break;
				
				case 12:
					$Mth[] = 'December';
				break;
			}
		}
		return $Mth;
	}

	public function ChartItemSalesOfNow($year)
	{
		$months = date('n');
		$vm = DB::table('cafe_transaction')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->where('type','0')
             ->whereRaw('month(created_at) <= '.$months)
             ->whereRaw('year(created_at) = '.$year)
             ->get();
         		for ($i=1; $i <= $months; $i++) { 
         			$mon[$i] = 0;
         		}
             	if(count($vm) > 0){
				    foreach ($vm as $key => $row) {
				    	
				    	if($row->month == 1){
					        $mon[1]++;
				    	}elseif($row->month == 2){
					        $mon[2]++;
				    	}elseif($row->month == 3){
					        $mon[3]++;
				    	}elseif($row->month == 4){
					        $mon[4]++;
				    	}elseif($row->month == 5){
					        $mon[5]++;
				    	}elseif($row->month == 6){
					        $mon[6]++;
				    	}elseif($row->month == 7){
					        $mon[7]++;
				    	}elseif($row->month == 8){
					        $mon[8]++;
				    	}elseif($row->month == 9){
					        $mon[9]++;
				    	}elseif($row->month == 10){
					        $mon[10]++;
				    	}elseif($row->month == 11){
					        $mon[11]++;
				    	}elseif($row->month == 12){
					        $mon[12]++;
				    	}
				    }
				}
			    return $mon;
	}
}