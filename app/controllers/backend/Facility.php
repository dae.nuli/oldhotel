<?php

class Facility extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Facility');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		$qr	= FacilityModel::orderBy('id','desc');
		if($cari){
			$qr = $qr->where('name','LIKE',"%$cari%");
		}
		$data['facility'] = $qr->paginate($this->limit);
		return View::make('backend.room.facility_index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Facility');
		View::share('path','Create');
		return View::make('backend.room.facility_create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'     => 'required',
			'icon'     => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/facility/create')->withErrors($valid)->withInput();
		}else{
			$cm       = new FacilityModel;
			$cm->name = Input::get('name');
			$cm->icon = Input::get('icon');
			$cm->save();
			return Redirect::to('admin/facility')->with('facility','Data has been added');
		}
	}

	public function getIcon()
	{
		return View::make('backend.room.modal_icon');
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Facility');
		View::share('path','Create');
		$data['facility']     = FacilityModel::find($id);
		return View::make('backend.room.facility_edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name'     => 'required',
			'icon'     => 'required',
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/facility/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$cm       = FacilityModel::find($id);
			$cm->name = Input::get('name');
			$cm->icon = Input::get('icon');
			$cm->save();
			return Redirect::to('admin/facility')->with('facility','Data has been added');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = FacilityModel::find($id);
		if($this->admin['level'] == 1){
			if(!empty($am)){
				// $dataFacility[] = '';
				$rm = RoomModel::withTrashed()->get();
				foreach ($rm as $key => $value) {
					$fasil = $value->facility;
					$data = explode(',', $fasil);
					foreach ($data as $i => $val) {
						if($val==$id){
							$dataFacility[] = $val;
						}
					}
				}
				// return count($dataFacility);
				if(isset($dataFacility)){
					return Redirect::to('admin/facility')->with('facility_alert','The facility is used');
				}else{
					$am->delete();
					return Redirect::to('admin/facility')->with('facility','Data has been deleted');
				}
			}
		}
	}

}
